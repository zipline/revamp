<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}

$rr = $db->query("SELECT * FROM admins ORDER BY username ASC");

include 'common/header.php' ;
?>
<div class="controls">
	<a href="admins_.php" class="btn btn-success">Add Admin</a>
</div>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
	<table class="table" style="width: auto;">
		<?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			?><tr class="<?= $class ?>">
				<td><?= $r['username'] ?></td>
				<td><a href="admins_permissions.php?id=<?= $r['id'] ?>" class="btn btn-default btn-xs">Permissions</a></td>
				<td><? edit_button("admins_.php?id={$r['id']}") ; ?></td>
				<td><? delete_button('admins',$r['id']) ; ?></td>
			</tr><?
		}
	?></table><?
}

include 'common/footer.php' ;

