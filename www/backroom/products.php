<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
	exit('Permission denied');
}

$rr = $db->query("
	SELECT p.*, c.title AS category_title
	FROM products AS p LEFT OUTER JOIN product_categories AS c
		ON p.id_categories = c.id
	ORDER BY p.priority ASC");

include 'common/header.php';
?>
<div class="controls"><? button('add', 'products_.php', 'Add Product'); ?></div>

<?
if (!$rr) {
	show_nothing();
} else {
	?>
	<ol class="sortable list" cmsTable="products" cmsHierarchyMode="flat">
		<?
		foreach ($rr as $r) {
			?>
			<li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
				<div class="row">
					<div class="cell dead"><?= htmlspecialchars($r['category_title']) ?></div>
					<div class="cell"><?= htmlspecialchars($r['title']) ?></div>
					<div class="buttons">
						<div class="cell">$<?= number_format($r['price'], 2) ?></div>
						<div class="cell"><? edit_button("products_.php?id={$r['id']}") ?></div>
						<div class="cell"><? delete_button('products', $r['id']) ?></div>
					</div>
				</div>
			</li>
			<?
		}
		?>
	</ol>
	<?
}

include 'common/footer.php';

