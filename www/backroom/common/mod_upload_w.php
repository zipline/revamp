<?

/* 	if( $mod_upload_allow_delete ){
		// If deleting file, unlink it
		if ($_POST["mod_upload_deleting"]) {
			$f = $_POST["mod_upload_path"] . "/" . $_POST["mod_upload_filename"];
			if (file_exists($f)) unlink($f);
		}
	}//if
 */
if($_POST['deleting']){
	$f = $_POST["mod_upload_path"] . "/" . $_POST["deleteimagename"];
	if (file_exists($f)) unlink($f);
}

function safename( $str ){
	return strtolower(str_replace( array('%20','&','`',' ','/','\\',"'",'-'), '_', $str )) ;
}

// rename the file, replacing bad characters with "_" ,
// and adding a unique timestamp right before the extension
function timename( $str ){
	preg_match('/\.[^.]+$/',$str,$matches) ;
	$ext= safename($matches[0]) ;
	$start = safename( substr( $str, 0, strlen($str)-strlen($ext) ) ) ;
	return $start .'-'. time() . $ext ;
}

function relocateFile($src_path, $options_array){
	// options are: dir , name , new_path
	if( is_file($src_path) ){
		//make the name safe
		foreach( $options_array as $key=>$options ){
			extract($options) ;
			$dir = rtrim($dir,'/') ; //just in case
			//make folder if it doesn't exist
			if(!is_dir($dir) ){
				mkdir($dir,0775,true) ;
			}
			$new_path = "$dir/$name" ;
			copy($src_path,$new_path) ;
			$return_names[$key] = $name ;
		}
	}
}

function relocateImage( $src_path , $options_array ){
	/*
		move and resize file, making as many copies as requested
		second argument is an array of arrays, each subarray holding options for an additional resizing operation
		options=>( dir, name, width, height, wm_source )
			dir: directory to put it
			name: new name for the file; if none given, use name of uploaded file
			width,height: height for new file; if either is left out, the other will be figured out
				proportionally.  if larger than original size, no resizing will occur
			wm_source: location of the watermark in .png format to apply.  will be located in the bottom with a margin of 5px

		sample usage:
			if( is_uploaded_file($_FILES["mod_upload_userfile"]["tmp_name"]) ){
				$basepath = '../upload/works' ;
				$name = safename($_FILES['mod_upload_userfile']['name']) ;
				$image_options = array(
						0=>array('name'=>$name,'dir'=>"$basepath/", 'width'=>600),
						1=>array('name'=>$name,'dir'=>"$basepath/thumb/", 'width'=>150)
					) ;
				$image_names = relocateImage($_FILES['mod_upload_userfile']['tmp_name'], $image_options) ; //this function in mod_upload_w
			}//if
	*/
	d($resize_function) ;
	if( is_file($src_path) ){
		//make the name safe
		foreach( $options_array as $key=>$o ){
			// options are: name , dir , width , height , wm_source
			#extract($options) ;
			$resize_function = $o['function'] ;
			if( ! in_array($resize_function,array('resizethumb','resizeprop')) ){
				$resize_function = 'resizeImage' ;
			}
			$existing_name = $o['name'] ;
			//make the file name safe
			$name = $o['name'] ? $o['name'] : time().'.jpg' ;
			$dir = rtrim($o['dir'],'/') ; //just in case
			//make folder if it doesn't exist
			d("dir:$dir;") ;
			if(!is_dir($dir) ){
				mkdir($dir,0775,true) ;
			}

			$new_path = "$dir/$name" ;
			d("new_path:$new_path;") ;
			list($old_w,$old_h) = getimagesize($src_path) ;
			//never size an image up
			if( ($o['width'] and $old_w<$o['width']) or ($o['height'] and $old_h<$o['height']) ){
				copy($src_path,$new_path) ;
			}else if( $o['width'] or $o['height'] ){
				$resize_function($src_path,$new_path,$o['width'],$o['height'],$existing_name) ;
			}else{
				copy($src_path,$new_path) ;
			}
			@chmod($new_path,0666) ;

			//do watermark if requested
			if( $o['wm_source'] ){
				watermark( $new_path,$o['wm_source'] ) ;
			}
			$return_names[$key] = $name ;
		}
	}
	return $return_names ;
}




function resizeImage($srcfile,$dstfile,$newwidth,$newheight,$orig_filename='') {
	//obtain file extension
	if (!$orig_filename) {
		$orig_filename = basename($srcfile);
	}
	$tmp = explode('.',$orig_filename);
	$ext = strtolower(end($tmp)) ;

	if( !$newheight ) $newheight = 'proportional' ;
	if( !$newwidth  ) $newwidth  = 'proportional' ;
	if( !$newheight and !$newwidth ){
		copy($srcfile,$dstfile) ;
		return ;
	}

	list($oldwidth,$oldheight) = getimagesize($srcfile) ;
	//if one of the dimensions is the string 'proportional', resize it to be
	//proportional to the other dimension
	if( $newheight == 'proportional' )
		$newheight = $oldheight * $newwidth/$oldwidth ;
	else if( $newwidth == 'proportional' )
		$newwidth = $oldwidth * $newheight/$oldheight ;
	d('Extension: '.$ext) ;
	// Get photo from temp file
	if( $ext=='png' )
		$src = ImageCreateFromPng($srcfile);
	elseif( $ext=='gif' )
		$src = ImageCreateFromGif($srcfile);
	else
		$src = ImageCreateFromJpeg($srcfile);
	$src_width = ImageSX($src);
	$src_height = ImageSY($src);
	
	if (($src_width/$src_height) > ($newwidth/$newheight)) $ratio = $src_width/$newwidth;
	else $ratio = $src_height/$newheight;
	
	$final_width = $src_width/$ratio;
	$final_height = $src_height/$ratio;
	
	$x = ($newwidth - $final_width)/2;
	$y = ($newheight - $final_height)/2;
	
	// Create resized photo
	@$dst = ImageCreateTrueColor($newwidth,$newheight);
	@$color = ImageColorAllocate($dst,255,255,255);
	@ImageFill($dst,0,0,$color);

	if( $ext=='png' ){
		//need to set these options for png transparency to be preserved
		imageAlphaBlending($dst,false) ;
		imageSaveAlpha($dst,true) ;
	}

	@ImageCopyResampled($dst,$src,$x,$y,0,0,$final_width,$final_height,$src_width,$src_height);

	if( 'png'==$ext )
		@ImagePng($dst,$dstfile,9);
	elseif( 'gif'==$ext ) 
		@ImageGif($dst,$dstfile,90);
	else
		@ImageJpeg($dst,$dstfile,90);

	@ImageDestroy($dst);
	@ImageDestroy($src);
}




//watermark an image the easy way!
//thanks to this cool guy: http://php.net/manual/en/function.imagecopymerge.php#48169
//note: transparency doesn't work.
//to use translucent watermark, save it as a png with alpha channel (png24)
function waterMark($fileInHD, $wmFile, $transparency = 50, $jpegQuality = 90, $margin = 0) {
	
	$jpegImg = imageCreateFromJPEG($fileInHD) ;
	$wmFileTmp = $wmFile.'_temp.png' ;//the name for the resized watermark
	//resize watermark to have same width as source image
	resizeImage( $wmFile , $wmFileTmp , imageSX($jpegImg) , 'proportional' ) ;
	$wmImg  = imageCreateFromPNG($wmFileTmp) ; //read in the new, resized watermark

	// set up watermark position - 5 pixels from the bottom-right corner
	$wmX = imageSX($jpegImg)-imageSX($wmImg) - $margin ;
	$wmY = imageSY($jpegImg)-imageSY($wmImg) - $margin ;
	
	// Water mark process
	imageCopy($jpegImg, $wmImg, $wmX, $wmY, 0, 0, imageSX($wmImg), imageSY($wmImg)) ;
	
	// Overwriting image
	ImageJPEG($jpegImg, $fileInHD, $jpegQuality) ;
	
}//waterMark

	
	




function write_uplbox($name,$path,$cur) {
	?><select name="<?=$name; ?>" class="selectbox">
	<option value=""></option><?
		$dir = opendir($path);
		$items = array();
		while($filename = readdir($dir)) if (is_file($path."/".$filename)) array_push($items,$filename);
		closedir($dir);
		asort($items);
		foreach($items as $filename) {
			if ($filename == $cur) $s = " selected"; else $s = "";
			echo("  <option value=\"$filename\"$s>$filename</option>\n");
		}
	?></select><?	
}


 
 
 
 


function write_form($action,$note,$path) {
	$path = rtrim($path,'/') ;
	?>
<script type="text/javascript">
function image_preview(imagename) {
	if (imagename)
		document.images["preview"].src="../upload/image/thumbs/" + imagename;
	else
		document.images["preview"].src="images/quickpreview.gif";
}
</script>
	<table style="margin: 0px auto; border:1px solid grey; border-collapse:collapse; border-spacing:0px; background-color:lightgrey;" >
		<tr>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="3" height="120">
					<form method="POST" enctype="multipart/form-data">
						<tr><td colspan="3" style="background-color:white; font-weight:bold;"><?= $note ?></td></tr>
						<tr class="dark">
							<td>
								<input type="hidden" name="uploading" value="true"><b>Upload File:</b>
							</td>
							<td><input type="file" name="mod_upload_userfile" class="inputbox"></td>
							<td>&nbsp;&nbsp;<input type="submit" value="upload" class="inputbutton" style="width: 55px;"></td>
						</tr>
					</form>
					<form method="POST" enctype="multipart/form-data">
					<tr class="light">
						<td><input type="hidden" name="deleting" value="true"><b>Delete File:</b></td>
						<td><input type="hidden" name="mod_upload_path" value="<?=$path ?>">
							<select name="deleteimagename" class="selectbox" onchange="image_preview(this[this.selectedIndex].value);">
								<option value=''></option><?
								$dir = opendir($path) ;
								while($imagename = readdir($dir)) {
									if( is_file("$path/$imagename") ) echo("<option value=\"$imagename\">$imagename</option>\n") ;
								}
								closedir($dir) ;
							?></select>
						</td>
						<td>&nbsp;&nbsp;<input type="submit" value="delete" class="inputbutton" style="width: 55px;"></td>
					</tr>

						<tr class="dark">
							<td><b>Quick Preview:</b></td>

							<td colspan="2"  >
							<select name="preview" class="selectbox" onchange="image_preview(this[this.selectedIndex].value);">
								<option value=""></option>
								<?

								$dir = opendir($path);
								while($imagename = readdir($dir)) {
									if( is_file("$path/$imagename") ) echo("<option value=\"$imagename\">$imagename</option>\n");
								}
								closedir($dir);

								?>
							</select>
							</td>
						</tr>
					</form>

					<tr class="light">
						<td><b>Rename:</b></td>
						<td colspan="2">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<form action="">

								<tr>
									<td>
										from 
										<select name="rename_from" class="selectbox" onchange="image_preview(this[this.selectedIndex].value);">
											<option value=""></option>
											<?

											$dir = opendir($path);
											while($imagename = readdir($dir)) {
												if( is_file("$path/$imagename") ) echo("<option value=\"$imagename\">$imagename</option>\n");
											}
											closedir($dir);

											?>
										</select>
									
									</td>
								</tr>
	
								<tr>
									<td nowrap>
										&nbsp; &nbsp; &nbsp;to
										<input type="text" name="rename_to" maxlength="20" class="inputbox">
										<input type="submit" name="rename" value="rename" class="inputbutton" />
									</td>
								</tr>

							</form>
						</table>
						</td>
					</tr>
				</table>
			</td>
			<td style="width:150px; text-align:center; vertical-align:top;" ><img src="images/quickpreview.gif" border="0" alt="" name="preview"></td>
		</tr>
	</table><?
}
	
function write_file_form($action,$note,$path) {
	$path = rtrim($path,'/') ;
	?><table style=" margin: 0px auto; border:1px solid grey; border-collapse:collapse; border-spacing:0px; background-color:lightgrey;" >
		<tr>
			<td valign="top">

				<table border="0" cellspacing="0" cellpadding="3" height="120">
					<form method="POST" enctype="multipart/form-data">
						<tr><td colspan="3" style="background-color:white; font-weight:bold;"><?= $note ?></td></tr>
						<tr class="dark">
							<td><input type="hidden" name="uploading" value="true"><b>Upload File:</b></td>
							<td><input type="file" name="mod_upload_userfile" class="inputbox"></td>
							<td>&nbsp;&nbsp;<input type="submit" value="upload" class="inputbutton" style="width: 55px;"></td>
						</tr>
					</form>

					<form method="POST" enctype="multipart/form-data">
					<tr class="light">
						<td><input type="hidden" name="deleting" value="true"><b>Delete File:</b></td>
						<td><input type="hidden" name="mod_upload_path" value="<?=$path ?>">
							<select name="deleteimagename" class="selectbox" onchange="image_preview(this[this.selectedIndex].value);">
								<option value=''></option><?
								$dir = opendir($path) ;
								while($imagename = readdir($dir)) {
									if( is_file("$path/$imagename") ) echo("<option value=\"$imagename\">$imagename</option>\n") ;
								}
								closedir($dir) ;
							?></select>
						</td>
						<td>&nbsp;&nbsp;<input type="submit" value="delete" class="inputbutton" style="width: 55px;"></td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table><?
}
// create image based on input image type
// accepts jpeg, gif, and png
function createimage($srcfile){
	$arr = getimagesize($srcfile) ;
	$mime = $arr['mime'] ;
	$ext = strtolower(end(explode('/',$mime))) ;
	switch($ext){
		case 'jpeg' :
			$src = imagecreatefromjpeg($srcfile);
			break ;
		case 'gif' :
			$src = imagecreatefromgif($srcfile);
			break ;
		case 'png' :
			$src = imagecreatefrompng($srcfile);
			break ;
	}
	return $src ;
}

// resize by shrinking image until the narrower dimension fits inside the box, then cut off excess
function resizethumb( $srcfile,$dstfile,$newwidth,$newheight,$orig_filename='' ){
	$ratio = $newwidth/$newheight;
	$ext = end(explode('.',$orig_filename)) ;
	$src = createimage($srcfile) ;
	$srcwidth = imagesx($src);
	$srcheight = imagesy($src);
	$cropx1 = 0;
	$cropy1 = 0;
	$cropx2 = $srcwidth;
	$cropy2 = $srcheight;
	if ($srcwidth/$srcheight<$ratio) {
		$newy = $srcwidth * $newheight / $newwidth;
		$cropy1 = ($srcheight-$newy)/2;
		$cropy2 = $srcheight-$cropy1;
	}
	else {
		$newx = $srcheight * $newwidth / $newheight;
		$cropx1 = ($srcwidth - $newx)/2;
		$cropx2 = $srcwidth - $cropx1;
	}
	$dst = imagecreatetruecolor($newwidth,$newheight);
	$color = imagecolorallocate($dst,255,255,255);
	imagefill($dst,0,0,$color);
	if( $ext=='png' ){
		//need to set these options for png transparency to be preserved
		imageAlphaBlending($dst,false) ;
		imageSaveAlpha($dst,true) ;
	}
	imagecopyresampled($dst,$src,0,0,$cropx1,$cropy1,$newwidth,$newheight,$cropx2-$cropx1,$cropy2-$cropy1);
	if( 'png'==$ext )
		ImagePng($dst,$dstfile,9);
	elseif( 'gif'==$ext ) 
		ImageGif($dst,$dstfile,90);
	else
		ImageJpeg($dst,$dstfile,90);
	imagedestroy($dst);
	imagedestroy($src);
}
// resize by shrinking image until it entire image fits in target rectangle
function resizeprop( $srcfile,$dstfile,$newwidth,$newheight,$orig_filename='' ){
	#$ratio = $newwidth/$newheight;
	$ext = end(explode('.',$orig_filename)) ;
	$src = createimage($srcfile) ;
	$srcwidth = imagesx($src);
	$srcheight = imagesy($src);
	$ratio = $srcwidth/$srcheight ;
	$lowerbound = $newwidth/$newheight ;
	if( $srcwidth < $newwidth and $srcheight < $newheight ){
		//nothing
		$finalh = $srcheight ;
		$finalw = $srcwidth ;
	}else{
		if( $ratio < $lowerbound ){
			// vertical constrain
			$finalh = $newheight ;
			$finalw = $ratio * $finalh ;
		}
		else{
			// horizontal constrain
			$finalw = $newwidth ;
			$finalh = 1/$ratio * $finalw ;
		}
	}
	d("width:$finalw") ;
	d("height:$finalh") ;

	$dst = imagecreatetruecolor($finalw,$finalh);
	$color = imagecolorallocate($dst,255,255,255);
	imagefill($dst,0,0,$color);
	if( $ext=='png' ){
		//need to set these options for png transparency to be preserved
		imageAlphaBlending($dst,false) ;
		imageSaveAlpha($dst,true) ;
	}
	imagecopyresampled($dst,$src,0,0,0,0, $finalw,$finalh, $srcwidth,$srcheight );
	if( 'png'==$ext )
		ImagePng($dst,$dstfile,9);
	elseif( 'gif'==$ext ) 
		ImageGif($dst,$dstfile,90);
	else
		ImageJpeg($dst,$dstfile,90);
	imagedestroy($dst);
	imagedestroy($src);
}


function resizelarge( $srcfile,$dstfile,$newwidth,$newheight ){
	$src = createimage($srcfile);
	$src_width = imagesx($src);
	$src_height = imagesy($src);
	if (($src_width/$src_height) > ($newwidth/$newheight)) $ratio = $src_width/$newwidth; else $ratio = $src_height/$newheight;
	$final_width = $src_width/$ratio;
	$final_height = $src_height/$ratio;
	$x = ($newwidth - $final_width)/2;
	$y = ($newheight - $final_height)/2;
	$dst = imagecreatetruecolor($newwidth,$newheight);
	$color = imagecolorallocate($dst,255,255,255);
	imagefill($dst,0,0,$color);
	imagecopyresampled($dst,$src,$x,$y,0,0,$final_width,$final_height,$src_width,$src_height);
	imagejpeg($dst,$dstfile);
	imagedestroy($dst);
	imagedestroy($src);
}


