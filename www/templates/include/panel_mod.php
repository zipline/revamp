<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

ob_start();
write_select(array(
    'rows' => $db->query("SELECT * FROM options WHERE id_categories = 1 AND display ORDER BY priority ASC"),
    'value' => 'id',
    'label' => 'title',
    'name' => 'pattern[]',
    'current' => '',
    'default' => '',
    'display_length' => 35,
));
$patterns = ob_get_clean();
ob_start();
write_select(array(
    'rows' => $db->query("SELECT * FROM options WHERE id_categories = 4 AND display ORDER BY priority ASC"),
    'value' => 'id',
    'label' => 'title',
    'name' => 'material[]',
    'current' => '',
    'default' => '',
    'display_length' => 35,
));
$material = ob_get_clean();

ob_start();
write_select(array(
    'rows' => $db->query("SELECT * FROM options WHERE id_categories = 2 AND display ORDER BY priority ASC"),
    'value' => 'id',
    'label' => 'title',
    'name' => 'color[]',
    'current' => '',
    'default' => '',
    'display_length' => 35,
));
$color = ob_get_clean();


$query = $db->prepare("SELECT * FROM options WHERE id_categories IN('1','2','4') AND display ORDER BY priority ASC");
$query->execute();
$oo = $query->fetchall();

$available_options = array();
foreach ($oo as $o) {
    $available_options[] = $o['id'];
}

//get list of options that exclude other options
$exclude_options = array();
foreach ($available_options AS $option) {
    $query = $db->prepare("SELECT hide_oid FROM option_hide WHERE oid = ?");
    $query->execute(array($option));
    $hidelist = $query->fetchAll();
    if ($hidelist) {
        $list = array();
        foreach ($hidelist as $hl) {
            $list[] = $hl['hide_oid'];
        }

        $exclude_options[$option] = $list;
    }
}

$content = $twigpanel->render('panel_mod.twig', array(
    'form' => $formdata,
    'patterns' => $patterns,
    'material' => $material,
    'finish' => $finish,
    'color' => $color,
    'exclude_options' => $exclude_options,
));


$page['page_vars']['content'] .= $content;

if (isset($_POST['formsubmit'])) {

    if (!$_POST['bi_email']) {
        e('An email address is required to request a quote');
    } else {
        //submit form
        ob_start();
        ?>

        <div class="section receipt">
            <h2>Panel Modification</h2>

            <h3>Contact Information</h3>
            <b>Name</b><br>
            <?= $_POST['name'] ?>
            <br><br>
            <b>Company</b><br>
            <?= $_POST['company'] ?>
            <br><br>
            <b>Email</b><br>
            <?= $_POST['bi_email'] ?>
            <br><br>
            <b>Phone</b><br>
            <?= $_POST['bi_phone'] ?>
            <br><br>
            <b>Preferred Contact Method</b><br>
            <?
            foreach ($_POST['contactpref'] as $cp) {
                echo $cp . ' <br>';
            }
            ?>
            <br><br><br><b>If we need to contact you by phone, what would be the best time?
                Revamp Panels business hours are 8am to 5pm Pacific Time, Monday through Friday.
                Please check next to day and time. You may check multiple boxes.</b>
            <br><br>
            <b>Day(s) of the Week</b><br>
            <?
            foreach ($_POST['contactdate'] as $cd) {
                echo $cd . ' <br>';
            }
            ?>
            <br>
            <b>Time(s)</b><br>
            <?
            foreach ($_POST['contacttime'] as $ct) {
                echo $ct . ' <br>';
            }
            ?>
            <br><br>

            <h3>Panel Modification Information</h3>

            <ol>
                <?
                $count = count($_POST['description']);
                for ($i = 0; $i < $count; $i++) {
                    ?>
                    <li>
                        <b>Please provide a short description of modifications you are requesting.</b><br>
                        <?= $_POST['description'][$i] ?>
                        <br><br>
                        <b>Quantity of panels for this modification to be ordered?</b><br>
                        <?= $_POST['quantity'][$i] ?>
                        <br><br>
                        <b>Please provide the panel measurements.</b><br>
                        <?= $_POST['xdim'][$i] ?> :X<br>
                        <?= $_POST['ydim'][$i] ?> :Y
                        <br><br>
                        <b>Pattern</b><br>
                        <?
                        if ($_POST['pattern'][$i]) {
                            $r = sql_fetch_by_key($db, 'options', 'id', $_POST['pattern'][$i]);
                            echo $r['title'];
                        }
                        ?>
                        <br><br>
                        <b>Material</b><br>
                        <?
                        if ($_POST['material'][$i]) {
                            $r = sql_fetch_by_key($db, 'options', 'id', $_POST['material'][$i]);
                            echo $r['title'];
                        }
                        ?>
                        <br><br>
                        <b>Color</b><br>
                        <?
                        if ($_POST['color'][$i]) {
                            $r = sql_fetch_by_key($db, 'options', 'id', $_POST['color'][$i]);
                            echo $r['title'];
                        }
                        ?>
                        <br><br><br>
                    </li>
                <? } ?>
            </ol>
        </div>
        <?

        $receipt = ob_get_clean();

        //$config['receipt_contact'] = 'brandon@designspike.com';
        smtp_mail($_POST['bi_email'], $config['receipt_contact'], "Panel Modification Request", $receipt);

        $page['page_vars']['content'] = "Thank you for your request. A representative will get in touch with you as soon as possible.";
    }
}



