<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}
?>
<div class="formfield">
    <b>Page Heading</b><br/>
    <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
</div>
<div class="formfield">
    <b>Page Content</b><br/>
    <textarea name="content" class="rich_editor"
              style="width: 100% ; height: 400px ;"><?= $page_vars['content'] ?></textarea>
</div>
<div class="formfield">
    <b>Path of file to include</b> <span class="note">(code is stored in /templates/include)</span><br/>
    <input type="text" name="file_path" value="<?= htmlspecialchars($page_vars['file_path']) ?>"
           style="width: 100%;"/>
</div>

