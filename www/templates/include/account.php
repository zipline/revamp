<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);



$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;



$content = $twigpanel->render('account.twig', array(
    'f' => $account,
    'session' => $_SESSION,
    'user' => $_SESSION['log'],
    'post' => $_POST,
    'config' => $config
));

$page['page_vars']['content'] .= $content;