<?

include 'common/config.php';

switch ($_GET['a']) {
    case 'couponcode':

        $discount = $db->prepare("SELECT * FROM discounts WHERE code=?");
        $discount->execute(array($_POST['code']));
        $discount = $discount->fetch();
        if($discount){
            if($discount['expires']>(date('Y-m-d').' 00:00:00')){
                unset($_SESSION['discounts']);//only one code at a time
                $_SESSION['discounts'][] = $discount['id'];
            }else{
                e('Coupon Code "'.$_POST['code'].'" Has Expired');
            }
        }else{
            e('Coupon Code "'.$_POST['code'].'" Not Found');
        }

        hle('/cart');
        break;
	case 'cart-invoice-add':	
		if ( ! $_SESSION['cart']) {
			$_SESSION['cart'] = array();
		}
		$_POST['invoice_amount'] = $_POST['invoice_amount'] + ($_POST['invoice_amount']*.029);
		$_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], '999999', array(), $_POST['invoice_amount'], 'Invoice #'.$_POST['invoice_number']);
		hle('/cart');
		break;
	case 'cart-add':

		if ( ! $_SESSION['cart']) {
			$_SESSION['cart'] = array();
		}
		$options = array();
		$i = 0;


        $_REQUEST['qty'] = intval($_REQUEST['qty']);

        if($_REQUEST['qty'] > 50){
            $_REQUEST['qty'] = 50;
        }elseif($_REQUEST['qty'] < 1){
            $_REQUEST['qty'] = 1;
        }

		foreach($_REQUEST['opt'] as $o){
			$i++;
			$option_value = $db->prepare("SELECT o.title, c.title AS key_title FROM options o, option_categories c WHERE o.id_categories=c.id AND o.id=?");
			$option_value->execute(array($o));
			$option_value = $option_value->fetch();
			$options["option_$i"] = $o.'|'.$option_value['title'].'|'.$option_value['key_title'];
		}
		$_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], $_REQUEST['id'], $options, $_REQUEST['qty']);
		hle('/cart');
		break;
	
	case 'cart-add-with-arbitrary-price':
		if ( ! $_SESSION['cart']) {
			$_SESSION['cart'] = array();
		}
		$_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], $_POST['id'], array(), $_POST['amount']);
		hle('/cart');
		break;
	
	case 'cart-add_multiple_products':
		$product_ids = explode(',', $_GET['products']);
		// check product_ids
		if (!$product_ids or !is_array($product_ids)) {
			exit('Incorrect product specification.');
		}
		if ( ! $_SESSION['cart']) {
			$_SESSION['cart'] = array();
		}
		$_SESSION['cart'] = cart_add_multiple_products($db, $_SESSION['cart'], $product_ids);
		hle('/cart');
		break;
		
	case 'cart-qty':
		$key = $_GET['key'];
		if ($_SESSION['cart'][$key] and $_GET['qty'] >= 0) {
			$_SESSION['cart'][$key]['qty'] = (int) $_GET['qty'];
			if ($_SESSION['cart'][$key]['qty'] <= 0) {
				unset($_SESSION['cart'][$key]);
			}
		}
		hle('/cart');
		break;

	case 'cart-remove':
		unset($_SESSION['cart'][$_GET['key']]);
		hle('/cart');
		break;

	case 'cart-shipping':
		form_save('cart-shipping', $_POST);
		$fields_to_copy = array('firstname', 'lastname', 'address', 'city', 'state', 'zip', 'country');
		$required_fields = array('bi_country', 'bi_firstname', 'bi_lastname', 'bi_address', 'bi_city', 'bi_zip', 'bi_email', 'bi_phone');
		$other_fields_to_save = array('remember_me', 'si_same', 'bi_state', 'si_state','mailinglist');

		if ($_POST['si_same']) {
			// copy billing to shipping
			foreach ($fields_to_copy as $field) {
				$tmp_customer['si_' . $field] = $_POST['bi_' . $field];
			}
		} else {
			// shipping fields must be checked
			$required_fields = array_merge($required_fields, array('si_country', 'si_firstname', 'si_lastname', 'si_address', 'si_city', 'si_zip'));
		}

		// check required fields; save customer data
		foreach ($required_fields as $field) {
			if (!$_POST[$field]) {
				$error = true;
			}
			$tmp_customer[$field] = $_POST[$field];
		}
		// save optional fields
		foreach ($other_fields_to_save as $field) {
			// don't overwrite if previously copied
			if (!isset($tmp_customer[$field])) {
				$tmp_customer[$field] = $_POST[$field];
			}
		}
		// USA and Canada only - check for state
		if (
				(!$tmp_customer['si_state'] and in_array($tmp_customer['si_country'], $g_state_required_countries) ) or
				(!$tmp_customer['bi_state'] and in_array($tmp_customer['bi_country'], $g_state_required_countries) )
		) {
			$error = true;
			e('State is required for your selected country');
		}

		// save customer to session
		unset($_SESSION['customer']);
		$_SESSION['customer'] = $tmp_customer;

		// save customer to cookie if requested
		if ($_POST['remember_me'] and is_array($_SESSION['customer'])) {
			setcookie('customer', serialize(array_map('stripslashes', $_SESSION['customer'])), time() + 60 * 60 * 24 * 365);
		} else {
			setcookie('customer', '', time() - 3600);
		}

		if ($error) {
			e('All fields are required');
			hle('/cart-address'); 
		} else {
			hle('/cart-payment');
		}
		break;		
	case 'cart-stripe-checkout':
		form_save('checkout_payment', $_POST);
		// validation
		if (!is_array($_SESSION['cart'])) {
			e('Your cart is empty');
			if ($_SESSION['purchase']['ordernumber']) {
				e("If you're trying to check out again, your order with number {$_SESSION['purchase']['ordernumber']} was already processed. Please check your email for a receipt, or contact us if you need help");
			}
		}

		// handle purchase orders
		if ($_POST['checkout_code']) {
			// validate that code exists
			$query = $db->prepare("SELECT * FROM customers WHERE allow_purchase_orders = 1 AND LOWER(checkout_code) = LOWER(?) AND checkout_code != ''");
			$query->execute(array($_POST['checkout_code']));
			$checkout_code_customer = $query->fetch();
			if ($checkout_code_customer['id']) {
				$is_purchase_order = true;
			} else {
				e('Invalid checkout code');
			}
		} else {
			if (!$_POST['cc_number'])
				e('Card Number is required');
			if (!$_POST['cc_code'])
				e('Card Security Code is required');
			if (!$_POST['cc_month'] or !$_POST['cc_year'])
				e('Card Expiration is required');
		}
		// cancel if validation errors
		if ($_SESSION['e']) {
			hle('/cart-payment');
		}
		// attempt to process order
		$order_grand_total = cart_get_grand_total($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
		$order_tax = cart_get_tax($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
		$order_shipping = cart_get_shipping($db, $_SESSION['cart'], $_SESSION['customer']);
		$order_vars = array(
			'tax' => $order_tax,
			'shipping' => $order_shipping,
			'bi_firstname' => $_SESSION['customer']['bi_firstname'],
			'bi_lastname'  => $_SESSION['customer']['bi_lastname'],
			'bi_country'   => $_SESSION['customer']['bi_country'],
			'bi_address'   => $_SESSION['customer']['bi_address'],
			'bi_city'      => $_SESSION['customer']['bi_city'],
			'bi_state'     => $_SESSION['customer']['bi_state'],
			'bi_zip'       => $_SESSION['customer']['bi_zip'],
			'bi_phone'     => $_SESSION['customer']['bi_phone'],
			'bi_email'     => $_SESSION['customer']['bi_email'],
			'si_firstname' => $_SESSION['customer']['si_firstname'],
			'si_lastname'  => $_SESSION['customer']['si_lastname'],
			'si_country'   => $_SESSION['customer']['si_country'],
			'si_address'   => $_SESSION['customer']['si_address'],
			'si_city'      => $_SESSION['customer']['si_city'],
			'si_state'     => $_SESSION['customer']['si_state'],
			'si_zip'       => $_SESSION['customer']['si_zip'],
			'si_phone'     => $_SESSION['customer']['si_phone'],
		);
		
		if ($is_purchase_order) {
			// no CC processing
			$purchase_type = 'Checkout Code';
		} else {
			// process Stripe transaction
			require_once('lib/stripe-php/lib/Stripe.php');
			
			if ($_SESSION['test']) {
				$stripe_secret_key = $config['stripe']['test_secret_key'];
			} else {
				$stripe_secret_key = $config['stripe']['live_secret_key'];
			}
			Stripe::setApiKey($stripe_secret_key);

			// get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
			
			try{
				// create the charge on Stripe's servers - this will charge the user's card
				$charge = Stripe_Charge::create(array(
					"amount" => $order_grand_total * 100, // amount in cents, again
					"currency" => "usd",
					"card" => $token,
					"description" => $order_vars['bi_email']
				));
			} catch(Exception $e) {
				e("Error processing payment.");
				hle('/cart-payment');
			}
			$purchase_type = 'Stripe';
			$order_stripe_id = $charge['id'];
	 	}
		$ordernumber = cart_new_order_number();
		// was order successful?
		if ($ordernumber) {
			// save to DB
			// order id is captured and copied to session so we can lookup download tickets and display on thankyou
			$id_orders = cart_checkout_create_order_db_records($db, array(
				'referer' => $_SESSION['referer'],
				'purchase_type' => $purchase_type,
				'customer' => $_SESSION['customer'],
				'cart' => $_SESSION['cart'],
				'tax_data' => $_SESSION['tax_data'],
				'tax' => $order_tax,
				'shipping' => $order_shipping,
				'grand_total' => $order_grand_total,
				'transaction_id' => $order_stripe_id,
				'ordernumber' => $ordernumber,
				'id_customer' => $_SESSION['log']['id'],
				'checkout_code' => $_POST['checkout_code'],
				'id_checkout_code_customer' => $checkout_code_customer['id']
			));

			// now that order is in db, create receipt - receipt needs order record id so it can link to downloads
			$receipt = cart_checkout_create_and_save_receipt($db, array(
				'order' => $order_vars,
				'receipt_template_path' => 'common/mod_receipt_content.php',
				'cart' => $_SESSION['cart'],
				'discounts' => $_SESSION['discounts'],
				'customer' => $_SESSION['customer'],
				'id_orders' => $id_orders,
				'checkout_code_customer_title' => $checkout_code_customer['title']
			));

			$_SESSION['purchase']['ordernumber'] = $ordernumber;
			$_SESSION['purchase']['id_orders'] = $id_orders;
			$_SESSION['purchase']['receipt'] = $receipt; // for display on thank you page
			// send receipts

			if ($_SESSION['test']) {
				$order_email = $config['email_developer'];
			} else {
				$order_email = $config['email_contact'];
			}
			$emails_to_send_receipt_to = array($order_email, $_SESSION['customer']['bi_email']);
			foreach ($emails_to_send_receipt_to as $email) {
				try{
					smtp_mail("\"{$config['company_name']}\"<{$config['receipt_contact']}>", $email, "Order #$ordernumber", $receipt);
				} catch(Exception $e) {
					e("The order went through, but there was an error sending the receipt email.");
				}
			}
            /*
			if($_SESSION['customer']['mailinglist']){
				//add to mailinglist
				$ml_fields = array('ml_name','ml_email');
				$ml_post['ml_email'] = strtolower($_SESSION['customer']['bi_email']);
				$ml_post['ml_name'] = $_SESSION['customer']['bi_firstname'].' '.$_SESSION['customer']['bi_lastname'];
				
				$ml_values = sql_organize_values($ml_fields, $ml_post);
				sql_upsert($db, 'emails', $ml_fields, $ml_values);
			}
            */
			
			unset($_SESSION['cart']);
			hle('/cart-thankyou');
		} else {
			hle('/cart-payment');
		}
		break;
    case 'cart-firstdata-checkout':
        form_save('checkout_payment', $_POST);
        // validation
        if (!is_array($_SESSION['cart'])) {
            e('Your cart is empty');
            if ($_SESSION['purchase']['ordernumber']) {
                e("If you're trying to check out again, your order with number {$_SESSION['purchase']['ordernumber']} was already processed. Please check your email for a receipt, or contact us if you need help");
            }
        }

        // handle purchase orders
        if ($_POST['checkout_code']) {
            // validate that code exists
            $query = $db->prepare("SELECT * FROM customers WHERE allow_purchase_orders = 1 AND LOWER(checkout_code) = LOWER(?) AND checkout_code != ''");
            $query->execute(array($_POST['checkout_code']));
            $checkout_code_customer = $query->fetch();
            if ($checkout_code_customer['id']) {
                $is_purchase_order = true;
            } else {
                e('Invalid checkout code');
            }
        } else {
            if (!$_POST['cc_number'])
                e('Card Number is required');
            if (!$_POST['cc_code'])
                e('Card Security Code is required');
            if (!$_POST['cc_month'] or !$_POST['cc_year'])
                e('Card Expiration is required');
        }
        // cancel if validation errors
        if ($_SESSION['e']) {
            hle('/cart-payment');
        }
        // attempt to process order
        $order_grand_total = cart_get_grand_total($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
        $order_tax = cart_get_tax($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
        $order_shipping = cart_get_shipping($db, $_SESSION['cart'], $_SESSION['customer']);
        $order_vars = array(
            'tax' => $order_tax,
            'shipping' => $order_shipping,
            'bi_firstname' => $_SESSION['customer']['bi_firstname'],
            'bi_lastname'  => $_SESSION['customer']['bi_lastname'],
            'bi_country'   => $_SESSION['customer']['bi_country'],
            'bi_address'   => $_SESSION['customer']['bi_address'],
            'bi_city'      => $_SESSION['customer']['bi_city'],
            'bi_state'     => $_SESSION['customer']['bi_state'],
            'bi_zip'       => $_SESSION['customer']['bi_zip'],
            'bi_phone'     => $_SESSION['customer']['bi_phone'],
            'bi_email'     => $_SESSION['customer']['bi_email'],
            'si_firstname' => $_SESSION['customer']['si_firstname'],
            'si_lastname'  => $_SESSION['customer']['si_lastname'],
            'si_country'   => $_SESSION['customer']['si_country'],
            'si_address'   => $_SESSION['customer']['si_address'],
            'si_city'      => $_SESSION['customer']['si_city'],
            'si_state'     => $_SESSION['customer']['si_state'],
            'si_zip'       => $_SESSION['customer']['si_zip'],
            'si_phone'     => $_SESSION['customer']['si_phone'],
        );

        if ($is_purchase_order) {
            // no CC processing
            $purchase_type = 'Checkout Code';
        } else {

            $trxnProperties = array(
                "User_Name"=>"",
                "Secure_AuthResult"=>"",
                "Ecommerce_Flag"=>"",
                "XID"=>"",
                "ExactID"=>$config['firstdata']['gateway_id'],				    //Payment Gateway
                "CAVV"=>"",
                "Password"=>$config['firstdata']['password'],					                //Gateway Password
                "CAVV_Algorithm"=>"",
                "Transaction_Type"=>'00', //Transaction Code I.E. Purchase="00" Pre-Authorization="01" etc.
                "Reference_No"=>$_POST["tbPOS_Reference_No"],
                "Customer_Ref"=>$_POST["tbPOS_Customer_Ref"],
                "Reference_3"=>$_POST["tbPOS_Reference_3"],
                "Client_IP"=>"",					                    //This value is only used for fraud investigation.
                "Client_Email"=>$order_vars['bi_email'],			//This value is only used for fraud investigation.
                "Language"=>'en',				//English="en" French="fr"
                "Card_Number"=>$_POST["cc_number"],		    //For Testing, Use Test#s VISA="4111111111111111" MasterCard="5500000000000004" etc.
                "Expiry_Date"=>$_POST["cc_month"] . $_POST["cc_year"],//This value should be in the format MM/YY.
                "CardHoldersName"=>$order_vars['bi_firstname'].' '.$order_vars['bi_lastname'],
                "Track1"=>"",
                "Track2"=>"",
                "Authorization_Num"=>$_POST["tbPOS_Authorization_Num"],
                "Transaction_Tag"=>$_POST["tbPOS_Transaction_Tag"],
                "DollarAmount"=>$order_grand_total,
                "VerificationStr1"=> $_SESSION['customer']['bi_address'].'|'.
                                        $_SESSION['customer']['bi_zip'].'|'.
                                        $_SESSION['customer']['bi_city'].'|'.
                                        $_SESSION['customer']['bi_state'].'|'.
                                        $_SESSION['customer']['si_country'],
                "VerificationStr2"=>$_POST["cc_code"],
                "CVD_Presence_Ind"=>"",
                "Secure_AuthRequired"=>"",
                "Currency"=>"",
                "PartialRedemption"=>"",

                // Level 2 fields
                "ZipCode"=>$_SESSION['customer']['bi_zip'],
                "Tax1Amount"=>$_POST["tbPOS_Tax1Amount"],
                "Tax1Number"=>$_POST["tbPOS_Tax1Number"],
                "Tax2Amount"=>$_POST["tbPOS_Tax2Amount"],
                "Tax2Number"=>$_POST["tbPOS_Tax2Number"],

                "SurchargeAmount"=>$_POST["tbPOS_SurchargeAmount"],	//Used for debit transactions only
                "PAN"=>$_POST["tbPOS_PAN"]							//Used for debit transactions only
            );
            try {
                $client = new SoapClientHMAC("https://api.globalgatewaye4.firstdata.com/transaction/v12/wsdl");
                $trxnResult = $client->SendAndCommit($trxnProperties);
            } catch(Exception $e){
                e($e);
            }
            if(@$client->fault){
                // there was a fault, inform
                e("<B>FAULT:  Code: {$client->faultcode} <BR />");
                e("String: {$client->faultstring} </B>");
                e("There was an error while processing. No TRANSACTION DATA IN CTR!");
                hle('/cart-payment');
            }
            if($trxnResult->Transaction_Approved == 1){
                $purchase_type = 'FirstData';
                $order_stripe_id = $trxnResult->SequenceNo;
            }else{
                e("Error: ". $trxnResult->EXact_Message .' '. $trxnResult->Bank_Message);
                hle('/cart-payment');
            }
        }
        $ordernumber = cart_new_order_number();
        // was order successful?
        if ($ordernumber) {
            // save to DB
            // order id is captured and copied to session so we can lookup download tickets and display on thankyou
            $id_orders = cart_checkout_create_order_db_records($db, array(
                'referer' => $_SESSION['referer'],
                'purchase_type' => $purchase_type,
                'customer' => $_SESSION['customer'],
                'cart' => $_SESSION['cart'],
                'tax_data' => $_SESSION['tax_data'],
                'tax' => $order_tax,
                'shipping' => $order_shipping,
                'grand_total' => $order_grand_total,
                'transaction_id' => $order_stripe_id,
                'ordernumber' => $ordernumber,
                'id_customer' => $_SESSION['log']['id'],
                'checkout_code' => $_POST['checkout_code'],
                'id_checkout_code_customer' => $checkout_code_customer['id']
            ));

            // now that order is in db, create receipt - receipt needs order record id so it can link to downloads
            $receipt = cart_checkout_create_and_save_receipt($db, array(
                'order' => $order_vars,
                'receipt_template_path' => 'common/mod_receipt_content.php',
                'cart' => $_SESSION['cart'],
                'discounts' => $_SESSION['discounts'],
                'customer' => $_SESSION['customer'],
                'id_orders' => $id_orders,
                'checkout_code_customer_title' => $checkout_code_customer['title']
            ));

            $_SESSION['purchase']['ordernumber'] = $ordernumber;
            $_SESSION['purchase']['id_orders'] = $id_orders;
            $_SESSION['purchase']['receipt'] = $receipt; // for display on thank you page
            // send receipts


            if ($_SESSION['test']) {
                $order_email = $config['email_developer'];
            } else {
                $order_email = $config['email_contact'];
            }
            $emails_to_send_receipt_to = array($order_email, $_SESSION['customer']['bi_email']);
            foreach ($emails_to_send_receipt_to as $email) {
                try{
                    smtp_mail("\"{$config['company_name']}\"<{$config['receipt_contact']}>", $email, "Order #$ordernumber", $receipt);
                } catch(Exception $e) {
                    e("The order went through, but there was an error sending the receipt email.");
                }
            }
			if($_SESSION['customer']['mailinglist']){
				//add to mailinglist
				$ml_fields = array('ml_name','ml_email');
				$ml_post['ml_email'] = strtolower($_SESSION['customer']['bi_email']);
				$ml_post['ml_name'] = $_SESSION['customer']['bi_firstname'].' '.$_SESSION['customer']['bi_lastname'];

				$ml_values = sql_organize_values($ml_fields, $ml_post);
				sql_upsert($db, 'emails', $ml_fields, $ml_values);
			}

            unset($_SESSION['cart']);
            hle('/cart-thankyou');
        } else {
            hle('/cart-payment');
        }
        break;

}

