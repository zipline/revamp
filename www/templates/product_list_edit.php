<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}
?>
<div class="formfield">
    <b>Page Heading</b><br/>
    <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
</div>

<div class="formfield">
    <b>Page Content</b><br/>
    <textarea name="content" class="rich_editor"
              style="width: 100% ; height: 700px ;"><?= $page_vars['content'] ?></textarea>
</div>

<div class="formfield">
    <b>Category</b><br/><?
    write_select(array(
        'rows' => $db->query("SELECT * FROM product_categories ORDER BY priority ASC"),
        'value' => 'id',
        'label' => 'title',
        'name' => 'id_categories',
        'current' => $r['id_categories'],
        'default' => $page_vars['id_categories'],
        'display_length' => 35,
    ));
    ?>
</div>