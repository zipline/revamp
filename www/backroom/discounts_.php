<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'discounts';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>
    <script>
        $(function(){
            $('#datepicker').datepicker({
                dateFormat : 'yy-mm-dd'
            });
        });
    </script>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Coupon Code</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" style="width:290px;" value="<?= htmlspecialchars($r['title']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Code</b><br/>
                            <input type="text" name="code" value="<?= htmlspecialchars($r['code']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Expires</b><br/>
                            <input type="text" id="datepicker" name="expires" value="<?= htmlspecialchars($r['expires']) ?>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Percentage Off</b><br/>
                            <input type="text" name="percent" value="<?= htmlspecialchars($r['percent']) ?>"/>%
                        </div>
                        <div class="formfield">
                            <b>Dollar Off</b><br/>
                            $<input type="text" name="dollaroff" value="<?= htmlspecialchars($r['dollaroff']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Minimum Amount for Order</b><br/>
                            $<input type="text" name="minimum" value="<?= htmlspecialchars($r['minimum']) ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';
