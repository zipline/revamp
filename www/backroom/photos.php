<?
include '../common/config.php';

$parent_table = 'galleries';
$basedir = "../upload/photos";
$parent = sql_fetch_by_key($db, $parent_table, 'id', $_GET['id']);
if (!$parent) {
    exit('No corresponding parent found');
}
$query = $db->prepare("SELECT * FROM photos WHERE id_parent = :id_parent ORDER BY priority ASC");
$query->execute(array(
    ':id_parent' => $_GET['id']
));
$rr = $query->fetchAll();


$urladdon = '';
$backlink = 'galleries.php';
$backlinktxt = 'Galleries';

if($_GET['pid']) {
    $urladdon = 'pid='.$_GET['pid'].'&';
    $backlink = 'products_.php?id='.$_GET['pid'];
    $backlinktxt = 'Products';
}


include 'common/header.php';

?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Controls</h4>
            <? button('add', 'photos_.php?'.$urladdon.'id_parent='.$_GET['id'], 'Add Single Image'); ?>
            <?if(!$_GET['pid']) { ?>
            <a class="btn btn-default" href="galleries_.php?<?=$urladdon ?>id=<?= htmlspecialchars($_GET['id']) ?>"><i
                    class="fa fa-fw fa-gear"></i>
                Edit Gallery Settings</a>
    <? } ?>
            <button class="btn btn-default rearrange-enable"><i class="fa fa-fw fa-arrows"></i> Rearrange</button>
            <hr>
            <? button('back', $backlink, 'Back'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Galleries</h1>


            <a href="<?=$backlink?>"><?=$backlinktxt ?></a> &gt; <?= htmlspecialchars($parent['title']) ?>
            <form action="a.php?a=photos-save"
                  class="dropzone"
                  id="my-dropzone" enctype="multipart/form-data">
                <input type="hidden" name="id_parent" value="<?= $_GET['id'] ?>"/>
                <input type="hidden" name="title" value=""/>
                <input type="hidden" name="content" value=""/>
            </form>
            <script type="text/javascript">
                $(document).ready(function () {
                    var myDropzone = new Dropzone("#my-dropzone");
                    myDropzone.on("queuecomplete", function(file) {
                        console.log('uploadcomplete');
                        location.reload();
                    });
                });
            </script>
            <hr>
            <h2>Uploaded Images:</h2>
            <?

            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <? print_rearrange_controls(); ?>
                <ol class="sortablegrid list root cf" cmsTable="photos" cmsHierarchyMode="nested" rootelementid="<?=$_GET['id'] ?>"><?
                    foreach ($rr as $r) {
                        ?>
                        <li class="ui-state-default no-nest" id="priorityitems-<?= $r['id'] ?>">
                        <div class="toolbar">
                            <? edit_button('photos_.php?'.$urladdon.'id='.$r['id'].'&id_parent='.$_GET['id']); ?>
                            <? delete_button('photos', $r['id']); ?>
                        </div>
                        <?
                        if (file_exists("$basedir/thumb/{$r['image']}")) {

                        }
                        ?>
                        <img class="galleryimgs" src="<?= "$basedir/thumb/{$r['image']}" ?>"/>
                        <div class="gal-title"><?=$r['title'] ?></div>
                        </li><?
                    }
                    ?></ol>
            <?
            }
            ?>
        </div>
    </div>
<?

include 'common/footer.php';

