/*
 * Embed Media Dialog based on http://www.fluidbyte.net/embed-youtube-vimeo-etc-into-ckeditor
 *
 * Plugin name:      mediaembed
 * Menu button name: MediaEmbed
 *
 * Youtube Editor Icon
 * http://paulrobertlloyd.com/
 *
 * @author Fabian Vogelsteller [frozeman.de]
 * @version 0.4
 */
( function() {
    CKEDITOR.plugins.add( 'flowplayer_embed',
        {
            init: function( editor )
            {
                var me = this;
                CKEDITOR.dialog.add( 'flowplayer_embedDialog', function (instance)
                {
                    return {
                        title : 'Embed Video',
                        minWidth : 550,
                        minHeight : 100,
                        contents :
                            [
                                {
                                    id : 'iframe',
                                    expand : true,
                                    elements :[{
                                        label : 'Video URL',
                                        type: 'text',
                                        id: 'txtUrl',
                                    }, {
                                        type: 'button',
                                        id: 'browse',
                                        filebrowser: 'iframe:txtUrl',
                                        label: editor.lang.common.browseServer,
                                    }]
                                }
                            ],
                        onOk: function() {
                            for (var i = 0; i < window.frames.length; i++) {
                                if (window.frames[i].name == 'iframeMediaEmbed') {
                                    var content = window.frames[i].document.getElementById("embed").value;
                                }
                            }
                            var vid_url = this.getContentElement('iframe', 'txtUrl').getValue();
                            this._.editor.insertHtml('<a class="myPlayer" href="'+vid_url+'"><img src="/flowplayer/play_large.png" /></a>');

                        }
                    };
                } );

                editor.addCommand( 'Flowplayer_Embed', new CKEDITOR.dialogCommand( 'flowplayer_embedDialog' ) );

                editor.ui.addButton( 'Flowplayer_Embed',
                    {
                        label: 'Embed Video',
                        command: 'Flowplayer_Embed',
                        icon: this.path + 'images/icon.png',
                        toolbar: 'flowplayer_embed'
                    } );
            }
        } );
} )();
