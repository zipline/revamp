<?
include '../common/config.php';

$table_name = 'blogs_entries';

$basedir_blog_images = '/upload/blogs';

$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);
$blog = sql_fetch_by_key($db, 'blogs', "id", $r['id_blogs']);

if (!admin_perm_check($db, $_SESSION['admin'], 'blogs.' . $blog['id'])) {
    exit('Permission denied');
}

if ($_GET['id_blogs']) {
    $r['id_blogs'] = $_GET['id_blogs'];
}

include 'common/header.php';


?>
    <script>
        $(function(){
            $('#datepicker').datepicker({
                dateFormat : 'yy-mm-dd'
            });
        });
    </script>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="id_blogs" value="<?= $_GET['id_blogs'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php?id_blogs=".$_GET['id_blogs'], 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Blogs</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>" id="title"
                                   style="width:290px;"/>
                        </div>
                        <div class="formfield">
                            <b>URL Keyword</b>
                            <br/>
                            <input type="text" name="keyword" class="autokeyword" rel="title"
                                   autokeywordbase="<?= htmlspecialchars($location_path_base) ?>"
                                   value="<?= $r['keyword'] ?>" style="width:75%;"/>
                        </div>
                        <div class="formfield">
                            <b>Date Posted</b><br/>
                            <input type="text" id="datepicker" style="width:290px;" name="date_posted"
                                   value="<?= htmlspecialchars($r['date_posted']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b><label><input type="checkbox"
                                             name="hidden" <?= $r['hidden'] ? 'checked="checked"' : '' ?>/> Hide
                                    this entry </label></b><br/>
                            <?
                            if (isset($config['blog_comments_possible']) and $$config['blog_comments_possible']) {
                                ?><label><input type="checkbox"
                                                name="comments_disabled" <?= $r['comments_disabled'] ? 'checked="checked"' : '' ?>/>
                                Disable comments </label><?
                            }
                            ?>
                        </div>
                        <b>Primary Image</b><br/>
                        <?
                        if ($r['image']) {
                            ?>
                            <small>
                                <img style="border: 0 ;" src="<?= "$basedir_blog_images/thumb/{$r['image']}" ?>"
                                <br/>
                                <input type="checkbox" name="image_clear" id="image_clear"/> <label
                                    for="image_clear">Clear
                                    image</label>
                            </small>
                            <br/>
                        <?
                        }
                        ?>
                        <input type="file" name="image"/>
                    </div>
                    <div class="col-md-6">
                        <h3>Categories</h3>
                        <?
                        $cc = $db->query("SELECT * FROM blog_tags ORDER BY title ASC");
                        foreach ($cc as $c) {
                            ?>
                            <?
                                $query = $db->prepare("SELECT * FROM  blog_entry_tags WHERE bid = ? AND tid = ?");
                                $query->execute(array($r['id'], $c['id']));
                                $po = $query->fetch();
                                ?>
                                <div class="col-md-6">
                                    <label>
                                        <input type="checkbox"
                                               name="option[]"
                                               value="<?= $c['id'] ?>" <? if ($po['id']) echo 'checked="checked"'; ?> />
                                        <?= $c['title'] ?>
                                    </label>
                                </div>
                        <?
                        }
                        ?>
                        <div class="checkbox-menu">
                            <button type="button" class="checkallbelow btn btn-primary"><i
                                    class="fa fa-check-square-o"></i> All
                            </button>
                            <button type="button" class="checknonebelow btn btn-cancel"><i
                                    class="fa fa-check-square-o"></i> None
                            </button>
                        </div>
                    </div>
                </div>
                <div class="formfield">
                    <b>Summary</b><br/>
                    <textarea name="summary" class="rich_editor basic"
                              style="width: 100% ; height: 150px ;"><?= $r['summary'] ?></textarea>
                </div>
                <div class="formfield">
                    <b>Content</b><br/>
                    <textarea name="content" class="rich_editor"
                              style="width: 100% ; height: 700px ;"><?= $r['content'] ?></textarea>
                </div>
            </div>
        </div>
    </form>
<?

include 'common/footer.php';

