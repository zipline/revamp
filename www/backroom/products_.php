<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'products';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if (!$r) {
    $r['enabled'] = 1;
    $r['taxable'] = 1;
    $r['arbitrary_price_allowed'] = 0;
    $r['shipping'] = 4.99;
}

include 'common/header.php';
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#reset_form_link').click(function () {
                $('#reset_form').slideToggle();
            });
        });
    </script>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="gallery_id" value="<?= $r['gallery_id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if ($r['gallery_id']) { ?>
                    <a href="photos.php?id=<?= $r['gallery_id'] ?>&pid=<?= $r['id'] ?>" class="btn btn-default"><i
                            class="fa fa-fw fa-picture-o"></i> Edit Gallery</a>
                <? } ?>
                <hr>
                <? button('back', "product_categories.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Product</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Partnumber</b><br/>
                            <input type="text" name="partnumber" value="<?= htmlspecialchars($r['partnumber']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <!--<div class="formfield">
                            URL for more information<br/>
                            <input type="text" name="url" value="<?= htmlspecialchars($r['url']) ?>"
                                   style="width: 100%;"/>
                        </div>-->
                        <div class="formfield">
                            <b>Category</b><br/><?
                            write_select(array(
                                'rows' => $db->query("SELECT * FROM product_categories ORDER BY priority ASC"),
                                'value' => 'id',
                                'label' => 'title',
                                'name' => 'id_categories',
                                'current' => $r['id_categories'],
                                'default' => $_GET['id_categories'],
                                'display_length' => 35,
                            ));
                            ?>
                        </div>
                        <div class="formfield">
                            <b>Price</b><br/>
                            $<input type="text" name="price" value="<?= htmlspecialchars($r['price']) ?>"/>
                        </div>
                        <!--<div class="col-md-4">
                            <div class="formfield">
                                <b>Shipping cost</b><br/>
                                <input type="text" name="shipping" value="<?= htmlspecialchars($r['shipping']) ?>"/>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="formfield">
                                    <b>X Size</b> <span class="note">in inches</span><br/>
                                    <input type="text" name="xsize" value="<?= htmlspecialchars($r['xsize']) ?>"
                                           style="width:100%"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="formfield">
                                    <b>Y Size</b> <span class="note">in inches</span><br/>
                                    <input type="text" name="ysize" value="<?= htmlspecialchars($r['ysize']) ?>"
                                           style="width:100%"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="formfield">
                                    <b>Depth</b> <span class="note">in inches</span><br/>
                                    <input type="text" name="zsize" value="<?= htmlspecialchars($r['zsize']) ?>"
                                           style="width:100%"/>
                                </div>
                            </div>
                        </div>

                        <div class="formfield">
                            <b>Weight</b> <span class="note">in lbs</span><br/>
                            <input type="text" name="weight" value="<?= htmlspecialchars($r['weight']) ?>"
                                   style="width:100%"/>
                        </div>
                        <div class="formfield">
                            <b>Product Settings</b><br>
                            <!--<label>
                                <input type="checkbox"
                                       name="arbitrary_price_allowed" <?= $r['arbitrary_price_allowed'] ? 'checked="checked"' : '' ?>/>
                                Arbitrary price allowed <span class="note">Use this for donations or payments.</span>
                            </label><br/>-->
                            <label>
                                <input type="checkbox" name="taxable" <?= $r['taxable'] ? 'checked="checked"' : '' ?>/>
                                Taxable
                            </label><br/>
                        </div>
                        <? /*
                        <div class="formfield">
                            <b>Primary Image</b><br/>
                            <?
                            if ($r['image']) {
                                ?>
                                <small>
                                    <img style="border: 0 ;" src="<?= "$basedir_product_images_small/{$r['image']}" ?>"
                                    <br/>
                                    <input type="checkbox" name="image_clear" id="image_clear"/> <label
                                        for="image_clear">Clear
                                        image</label>
                                </small>
                                <br/>
                            <?
                            }
                            ?>
                            <input type="file" name="image"/>
                        </div>
                        */ ?>
                        <div class="formfield">
                            <b>Description</b><br/>
                            <textarea name="description" class="rich_editor basic"
                                      style="width: 100%; height: 200px ;"><?= $r['description'] ?></textarea>
                        </div>


                        <h2>Related Products</h2>
                        <?
                            $query = $db->prepare("SELECT * FROM products WHERE id != ? ORDER BY priority DESC");
                            $query->execute(array($r['id']));
                            $oo = $query->fetchAll();
                            foreach ($oo as $o) {
                                $query = $db->prepare("SELECT * FROM  products_related WHERE pid_target = ? AND pid = ?");
                                $query->execute(array($o['id'], $r['id']));
                                $po = $query->fetch();
                                ?>
                                <div class="col-md-6">
                                    <label>
                                        <input type="checkbox"
                                               name="related[]"
                                               value="<?= $o['id'] ?>" <? if ($po['id']) echo 'checked="checked"'; ?> />
                                        <?= $o['title'] ?>
                                    </label>
                                </div>
                            <?
                            }
                            ?>
                            <div class="checkbox-menu">
                                <button type="button" class="checkallbelow btn btn-primary"><i
                                        class="fa fa-check-square-o"></i> All
                                </button>
                                <button type="button" class="checknonebelow btn btn-cancel"><i
                                        class="fa fa-check-square-o"></i> None
                                </button>
                            </div>
                    </div>
                    <div class="col-md-6">
                        <h2>Options</h2>
                        <?
                        $cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
                        foreach ($cc as $c) {

                            $query = $db->prepare("SELECT * FROM options WHERE id_categories = ? ORDER BY priority DESC");
                            $query->execute(array($c['id']));
                            $oo = $query->fetchAll();
                            if (count($oo)) {
                                ?>
                                <h3><?= $c['title'] ?></h3>
                                <?
                                foreach ($oo as $o) {
                                    $query = $db->prepare("SELECT * FROM  product_options WHERE oid = ? AND pid = ?");
                                    $query->execute(array($o['id'], $r['id']));
                                    $po = $query->fetch();
                                    ?>
                                    <div class="col-md-6">
                                        <label>
                                            <input type="checkbox"
                                                   name="option[]"
                                                   value="<?= $o['id'] ?>" <? if ($po['id']) echo 'checked="checked"'; ?> />
                                            <?= $o['title'] ?>
                                        </label>
                                    </div>
                                <?
                                }
                                ?>
                                <div class="checkbox-menu">
                                    <button type="button" class="checkallbelow btn btn-primary"><i
                                            class="fa fa-check-square-o"></i> All
                                    </button>
                                    <button type="button" class="checknonebelow btn btn-cancel"><i
                                            class="fa fa-check-square-o"></i> None
                                    </button>
                                </div>
                                <hr>
                            <?
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function check_option_box() {
                console.log(parseFloat($(this).val()));
                if (parseFloat($(this).val()) > 0) {
                    $(this).parent().parent().parent().find('input[type="checkbox"]').prop('checked', true);
                }
            }
            $(document).ready(function () {
                $('.checkallbelow').click(function () {
                    $(this).parent().prevUntil('h3').each(function () {
                        $(this).find('input[type="checkbox"]').prop('checked', true);
                    });
                });
                $('.checknonebelow').click(function () {
                    $(this).parent().prevUntil('h3').each(function () {
                        $(this).find('input[type="checkbox"]').prop('checked', false);
                    });
                });
            });
        </script>
    </form>
<?
include 'common/footer.php';

