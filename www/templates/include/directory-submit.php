<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);



//$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;
$directory = sql_fetch_by_key($db, 'directory', 'id_user', $_SESSION['log']['id']) ;


$query = $db->prepare("SELECT * FROM facilitator_options WHERE type = 'specialty'") ;
$query->execute() ;
$specialty = $query->fetchAll() ;
$query = $db->prepare("SELECT * FROM facilitator_options WHERE type = 'credential'") ;
$query->execute() ;
$credential = $query->fetchAll() ;
$query = $db->prepare("SELECT * FROM facilitator_options WHERE type = 'language'") ;
$query->execute() ;
$language = $query->fetchAll() ;


$query = $db->prepare("SELECT * FROM directory_facilitator_options WHERE did = ?") ;
$query->execute(array($directory['id'])) ;
$options = $query->fetchAll() ;
foreach($options as $o){
    $directory['options'][$o['oid']] = 1;
}

$content = $twigpanel->render('directory-submit.twig', array(
    'f' => $directory,
    'specialty' => $specialty,
    'credential' => $credential,
    'language' => $language,
    'session' => $_SESSION,
    'user' => $_SESSION['log'],
    'post' => $_POST,
    'config' => $config
));

$page['page_vars']['content'] .= $content;