<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}

$table_name = 'admins' ;

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']) ;

include 'common/header.php' ;

?>
<script src="/lib/bower_components/zxcvbn/zxcvbn-async.js"></script>
<script>
	$(function(){
		$('[name=password]').passwordStrength({
			strength_text: '#score',
			user_inputs: [$('[name=username]').val()]
		});
		$('#generate').generatePassword({
			fill_password: '.fill_password',
			unhide: '#password_unhide'
		}, function() {
			$('[name=password]').trigger('change');
		});
	});
</script>
<form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?= $r['id'] ?>"/>
	<? print_controls(array('back'=>"$table_name.php")) ; ?>
	<table class="editor">
		<tr>
			<td style="vertical-align: top;">Username<br/><input type="text" name="username" value="<?= htmlspecialchars($r['username']) ?>"/></td>
			<td style="vertical-align: top;">
				New Password
				<small>(It is highly recommended to use a random password)</small><br/>
				<input type="password" id="password" class="fill_password" name="password" value="<?= htmlspecialchars($r['password']) ?>"/>
				<input type="text" id="password_unhide" class="fill_password" disabled style="display: none;"/>
				<span id="score"></span>
				<br/>
				<span id="generate" class="btn btn-default">Generate</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">Notes<br/>
				<textarea name="notes" style="width:590px; height: 400px ;"><?= $r['notes'] ?></textarea>
			</td>
		</tr>
	</table>
</form>
<?
include 'common/footer.php' ;
