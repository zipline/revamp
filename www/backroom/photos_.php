<?
include '../common/config.php';

$parent_table = 'galleries';
$basedir = "../upload/photos";

$r = sql_fetch_by_key($db, 'photos', 'id', $_GET['id']);

if (!$r['id_parent']) {
    $r['id_parent'] = $_GET['id_parent'];
}

$editing_item = sql_fetch_by_key($db, $parent_table, 'id', $r['id_parent']);


$urladdon = '';
$backlink = 'galleries.php';
$backlinktxt = 'Galleries';

if($_GET['pid']) {
    $urladdon = 'pid='.$_GET['pid'].'&';
    $backlink = 'products_.php?id='.$_GET['pid'];
    $backlinktxt = 'Products';
}

include 'common/header.php';

?>
    <form action="a.php?<?=$urladdon ?>a=photos-save" method="post" class="editor-form" enctype="multipart/form-data" id="editor_form">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="id_parent" value="<?= $r['id_parent'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <button class="btn btn-default" name="submit" type="submit" value="Save and add another">
                    <i class="fa fa-fw fa-plus"></i> Save and add another
                </button>
                <hr>
                <? button('back', "photos.php?{$urladdon}id=" . $r['id_parent'], 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Photos</h1>

                <a href="<?=$backlink?>"><?=$backlinktxt ?></a> &gt;
                <a href="galleries_.php?<?=$urladdon ?>id=<?= htmlspecialchars($editing_item['id']) ?>"><?= htmlspecialchars($editing_item['title']) ?></a>
                &gt;
                <a href="photos.php?<?=$urladdon ?>id=<?= htmlspecialchars($editing_item['id']) ?>">Manage photos</a> &gt;
                Edit

                <div class="formfield">
                    <?= cms_photo_selector(
                        'Image',
                        'file',
                        $r['image'],
                        $basedir . '/400/',
                        $basedir . '/800/'
                    )
                    ?>
                </div>
                <div class="formfield">
                    Title<br/>
                    <input type="text" name="title" style="width: 100%;" value="<?= htmlspecialchars($r['title']) ?>"/>
                </div>
                <div class="formfield">
                    Alt Text <span class="note">Used for SEO and displayed if image doesn't load.</span> <br/>
                    <input type="text" name="alt" style="width: 100%;" value="<?= htmlspecialchars($r['alt']) ?>"/>
                </div>
                <div class="formfield">
                    Description<br/>
                    <textarea name="content" style="width: 100% ; height: 200px;"
                              class="rich_editor basic"><?= htmlspecialchars($r['content']) ?></textarea>
                </div>


                <h2>Tags <span class="note">Used for grid galleries like the inspiration page</span></h2>
                <?
                
                $cc = $db->query("SELECT * FROM gallery_tag_cats ORDER BY title ASC");
                foreach ($cc as $c) {

                    $query = $db->prepare("SELECT * FROM gallery_tags WHERE cat = ? ORDER BY title DESC");
                    $query->execute(array($c['id']));
                    $oo = $query->fetchAll();
                    if (count($oo)) {
                        ?>
                        <h3><?= $c['title'] ?></h3>
                        <?
                        foreach ($oo as $o) {
                            $query = $db->prepare("SELECT * FROM  photo_tags WHERE tid = ? AND pid = ?");
                            $query->execute(array($o['id'], $r['id']));
                            $po = $query->fetch();
                            ?>
                            <div class="col-md-4">
                                <label>
                                    <input type="checkbox"
                                           name="tag[]"
                                           value="<?= $o['id'] ?>" <? if ($po['id']) echo 'checked="checked"'; ?> />
                                    <?= $o['title'] ?>
                                </label>
                            </div>
                            <?
                        }
                        ?>
                        <div class="checkbox-menu">
                            <button type="button" class="checkallbelow btn btn-primary"><i
                                    class="fa fa-check-square-o"></i> All
                            </button>
                            <button type="button" class="checknonebelow btn btn-cancel"><i
                                    class="fa fa-check-square-o"></i> None
                            </button>
                        </div>
                        <hr>
                        <?
                    }
                }
                ?>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

