<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);


$content = $twigpanel->render('panel_design_build.twig', array(
    'form' => $formdata
));


$page['page_vars']['content'] .= $content;

if(isset($_POST['formsubmit'])){
    //submit form
    ob_start();
?>
    <div class="section receipt">
        <h2>Design/Build</h2>
        <h4>Contact Information</h4>
        <b>Name</b><br>
        <?=$_POST['name']?>
        <br><br>
        <b>Company</b><br>
        <?=$_POST['company']?>
        <br><br>
        <b>Email</b><br>
        <?=$_POST['bi_email']?>
        <br><br>
        <b>Phone</b><br>
        <?=$_POST['bi_phone']?>
        <br><br>
        <b>Preferred Contact Method</b><br>
        <?
        foreach($_POST['contactpref'] as $cp){
            echo $cp.' <br>';
        }
        ?>
        <br><br><br><b>If we need to contact you by phone, what would be the best time?
            Revamp Panels business hours are 8am to 5pm Pacific Time, Monday through Friday.
            Please check next to day and time. You may check multiple boxes.</b>
        <br><br>
        <b>Day(s) of the Week</b><br>
        <?
        foreach($_POST['contactdate'] as $cd){
            echo $cd.' <br>';
        }
        ?>
        <br>
        <b>Time(s)</b><br>
        <?
        foreach($_POST['contacttime'] as $ct){
            echo $ct.' <br>';
        }
        ?>
        <br><br>

        <h4>Project Information</h4>
        <b>Please provide project name or reference, if applicable</b><br>
        <?=$_POST['project']?>
        <br><br>
        <b>Please provide a description of your project using the following criteria:</b>
        <ul class="small-list">
            <li>If this is a commercial building, what type?</li>
            <li>If this is a business, what type?</li>
            <li>Is this a residence?</li>
            <li>Is this a landscape project, building exterior, or building interior?</li>
            <li>Is this new construction or a remodel?</li>
        </ul>
        <?=$_POST['description']?>
        <br><br>
        <b>Please provide an estimate of the scale of your project?</b><br>
        <?=$_POST['scale']?>
        <br><br>
        <b>What services would you like from Revamp Panels&trade;?</b><br>
        <?=$_POST['services']?>
        <br><br>
        <b>Would you like to use one of Revamp Panels&trade; patterns or would you like a custom design?</b><br>
        <?=$_POST['patterns']?>
        <br><br>
        <b>What is the estimated delivery date for Revamp products?</b><br>
        <?=$_POST['date']?>
        <br><br>
    </div>
<?

    $receipt = ob_get_clean();
    smtp_mail($_POST['bi_email'], $config['receipt_contact'], "Design/Build Request", $receipt);

    $page['page_vars']['content'] = "Thank you for your request. A representative will get in touch with you as soon as possible.";
}



