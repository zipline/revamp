<?
$fields = array('heading','content') ;

$basedir_thumbnail = '../upload/photos/';

if ($_FILES['thumbnail']['name']) {
    $tempimage = $_FILES['thumbnail'];
    list($image) = relocateImage($tempimage['tmp_name'], array(
        array('name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/orig"),
        array('width' => 1600, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/1600"),
        array('width' => 800, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/800"),
        array('width' => 400, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/400"),
        array('width' => 150, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/thumb")
    ));
    $_POST['thumbnail'] = $image;
    $fields[] = 'thumbnail';
} elseif ($_POST['thumbnail_clear']) {
    $_POST['thumbnail'] = '';
    $fields[] = 'thumbnail';
}


include '../templates/basic_template_save.php' ;

