<?
include '../common/config.php';

$r = sql_fetch_by_key($db, 'orders', 'id', $_GET['id']);

include 'common/header.php';
?>
    <style type="text/css">
        .receipt table {
            border-spacing: 4px;
            border-collapse: separate;
        }
    </style>
    <script>
        $(function () {
            $('#datepicker').datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>

    <form action="a.php?a=orders-save" method="post" class="editor-form">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Options</h4>
                <button value="save" name="submit" class="btn btn-primary" type="submit"><i
                        class="fa fa-fw fa-floppy-o"></i> Save Notes
                </button>
                <hr>
                <? button('back', "orders.php", 'Back to Orders'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Orders</h1>

                <div class="row">
                    <div class="col-md-3">
                        <div class="formfield">
                            <b>Date</b><br/>
                            <?= date('Y-m-d h:i a', strtotime($r['order_date'])) ?>
                        </div>
                        <div class="formfield">
                            <b>Order Number</b><br/>
                            <?= $r['ordernumber'] ?>
                        </div>
                        <div class="formfield">
                            <b>Type</b><br/>
                            <?= $r['purchase_type'] ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="formfield">
                            <b>Transaction ID</b><br/>
                            <?= $r['transaction_id'] ?>
                        </div>
                        <div class="formfield">
                            <b>Customer</b><br/>
                            <?= htmlspecialchars("{$r['bi_firstname']} {$r['bi_lastname']}") ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Notes</b><br/>
                            <textarea name="notes" style="width: 100% ; height: 150px ;"><?= $r['notes'] ?></textarea>
                        </div>
                    </div>
                </div><br>
        </div>
    </form>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-2">
            <div class="section">
                <h2>Resend Receipt</h2>

                <div class="pad">
                    <form action="a.php?a=orders-resend-receipt" method="post">
                        <input type="hidden" name="id_orders" value="<?= $r['id'] ?>"/>
                        <input type="text" name="email" value="<?= htmlspecialchars($r['bi_email']) ?>"/>
                        <input type="submit" class="btn btn-primary" value="Send"/>
                    </form>
                </div>
            </div>

            <div class="section">
                <h2>Shipping Records</h2>

                <div class="pad">
                    <?

                    $query = $db->prepare("SELECT * FROM order_shipments WHERE id_orders = ? ORDER BY date_shipped DESC");
                    $query->execute(array($r['id']));
                    $shipments = $query->fetchAll();
                    if ($shipments) {
                        ?>
                        <table class="list">
                        <tr>
                            <th>Shipped</th>
                            <th>Carrier</th>
                            <th>Email</th>
                            <th>Link</th>
                            <th>Number</th>
                        </tr><?
                        foreach ($shipments as $shipment) {
                            ?>
                            <tr>
                            <td><?= date('Y-m-d', strtotime($shipment['date_shipped'])) ?></td>
                            <td><?= $shipment['carrier_title'] ?></td>
                            <td><?= $shipment['email_to'] ?></td>
                            <td><a href="<?= $shipment['tracking_link'] ?>" target="_blank">Link</a></td>
                            <td><?= htmlspecialchars($shipment['tracking_number']) ?></td>
                            </tr><?
                            if ($shipment['notes']) {
                                ?>
                                <tr>
                                <td colspan="99"
                                    style="padding-bottom: 15px ; background-color: #f2f2f2 ;"><?= nl2br(htmlspecialchars($shipment['notes'])) ?></td>
                                </tr><?
                            }
                        }
                        ?></table><?
                    }
                    ?>
                    <p><a href="#" class="btn btn-default" onclick="$('.shipping_form').slideToggle('fast'); return false;"><i
                                class="fa fa-fw fa-plus"></i> New Record</a></p>

                    <div class="shipping_form" style="display: none ; border:">
                        <form action="a.php?a=orders-send-tracking-info" method="post" class="editor-form"
                              style="margin: 0 ;">
                            <input type="hidden" name="id_orders" value="<?= $r['id'] ?>"/>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="formfield">
                                        <b>Date Shipped</b><br/>
                                        <input type="text" id="datepicker" name="date_shipped"
                                               value="<?= htmlspecialchars($r['date_shipped']) ?>"/>
                                    </div>
                                    <div class="formfield">
                                        <b>Recipient Email</b><br/>
                                        <input type="text" name="email_to"
                                               value="<?= htmlspecialchars($r['bi_email']) ?>"  style="width:100%;"/>
                                    </div>
                                    <div class="formfield">
                                        <b>Carrier</b><br/><?

                                        write_select(array(
                                            'rows' => $db->query("SELECT * FROM shipping_carriers ORDER BY title ASC"),
                                            'value' => 'id',
                                            'label' => 'title',
                                            'name' => 'id_shipping_carriers',
                                            'current' => $r['id_shipping_carriers'],
                                            'display_length' => 35,
                                        ));

                                        ?>
                                    </div>
                                    <div class="formfield">
                                        <b>Tracking Number</b><br/>
                                        <input type="text" name="tracking_number" style="width:100%;" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="formfield">
                                        <b>Notes</b><br/>
                                        <textarea name="notes"
                                                  style="width: 100% ; height: 150px ;"><?= $r['notes'] ?></textarea>
                                    </div>
                                    <input type="submit" class="btn btn-primary" value="Save and Send"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="section receipt">
                <h2>Receipt</h2>
                <?= $r['receipt'] ?>
            </div>
        </div>
    </div>
<?
include 'common/footer.php';

