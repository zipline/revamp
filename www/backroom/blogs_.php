<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'blogs';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if (!$r['index_entries']) {
    $r['index_entries'] = $config['blog_index_entry_limit_default'];
}

include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Blogs</h1>

                <div class="formfield">
                    <b>Blog Title</b><br/>
                    <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>" style="width:290px;"/>
                </div>
                <div class="formfield">
                    <b>Entries on Index Page</b><br/>
                    <input type="text" name="index_entries" value="<?= $r['index_entries'] ?>"/>
                </div>
                <div class="formfield">
                    <b>Location</b>
                    <span class="note">Location should match the keyword of the page that serves as the blog's index.</span><br/>
                    <?= $config['site_address'] ?>/
                    <input type="text" name="keyword" value="<?= $r['keyword'] ?>"
                                                          style="width:110px;"/>
                    <input type="button" value="Generate" onclick="generate_page_keyword();"
                                                                                       class="small"/><br/>
                </div>
                <div class="formfield">
                    <b>Blog Description</b><br/>
                    <textarea name="description" class="rich_editor basic"
                              style="width: 100%; height: 200px ;"><?= $r['description'] ?></textarea>
                </div>
            </div>
        </div>
    </form>
<?

include 'common/footer.php';

