<html>
<body>
<table align="center" width="590"><tr><td>

<table width="100%" style="border-spacing:0px; border-collapse:collapse;">
	<tr>
		<td style="font-family:Arial;font-size:90%;">
			<b style="font-size: 120%"><?= $config['company_name'] ?></b><br/>
            Order Number: <?=$o['ordernumber'] ?><br/>
			<?=$config['email_contact'] ?><br/>
			<a href="<?=$config['site_address'] ?>"><?= $config['site_address'] ?></a>
		</td>
	</tr>
</table>

<hr/>

<p><b style="font-size: 120% ;">Items Purchased</b></b>

<table cellspacing="3" style="width:100%">
<?

cart_display($db, array(
	'cart'     =>$o['cart'],
	'discounts'=>$o['discounts'],
	'customer' =>$o['customer'],
	'show_tax'=>true,
	'show_controls'=>false,
	'tax_override_amount'=>$o['tax_override_amount'],
	'grand_total_override_amount'=>$o['grand_total_override_amount'],
	)) ;
?>
</table>
<?
	$query = $db->prepare("SELECT d.*, p.title, DATE_FORMAT(d.expiration,'%b %d %y %l:%i%p') AS format_date FROM download_tickets d, products p WHERE d.id_products = p.id AND d.id_orders=? AND d.expiration>=NOW()");
	$query->execute(array($o['id_orders']));
	$rr2 = $query->fetchAll();
	if( $rr2 ){
		?>
<p><b style="font-size: 120% ;">Downloadable Files</b></b>
		<table>
		<tr>
			<td colspan="8"> 
				<div style="padding:0px 15px;">
				<ul> 
				<?
					foreach( $rr2 as $r2 ){
							echo '<li><a target="_blank" href="'.$g_site_address.'/download.php?purchase_key='.$r2['purchase_key'].'">'.$r2['title'].'</a> (exp: '.$r2['format_date'].')</li>';
					}
				?>
				</ul>
				</div>
			</td>
		</tr>
		</table>
		<?
	}		
?>

<hr/>

<p><b style="font-size: 120% ;">Billing and Shipping</b></p>
<table>
	<tr>
		<td style="vertical-align: top ;">
			<table class="chart">
				<tr><th colspan="2" style="text-align: left ;">Billing</th></tr>
				<tr><td><b>First Name</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_firstname']) ?></b></td></tr>
				<tr><td><b>Last Name</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_lastname']) ?></td></tr>
				<tr><td><b>Country</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_country']) ?></td></tr>
				<tr><td><b>Address</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_address']) ?></td></tr>
				<tr><td><b>City</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_city']) ?></td></tr>
				<tr><td><b>State/Province</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_state']) ?></td></tr>
				<tr><td><b>Zip/Postal Code</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_zip']) ?></td></tr>
				<tr><td><b>Email</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_email']) ?></td></tr>
				<tr><td><b>Phone</b></td>
					<td><?= htmlspecialchars($o['customer']['bi_phone']) ?></td></tr>
			</table>
		</td>
		<td style="width: 40px ;"></td>
		<td style="vertical-align: top ;">
			<table class="chart">
				<tr><th colspan="2" style="text-align: left ;">Shipping</th></tr>
				<tr><td><b>First Name</b></td>
					<td><?= htmlspecialchars($o['customer']['si_firstname']) ?></td></tr>
				<tr><td><b>Last Name</b></td>
					<td><?= htmlspecialchars($o['customer']['si_lastname']) ?></td></tr>
				<tr><td><b>Country</b></td>
					<td><?= htmlspecialchars($o['customer']['si_country']) ?></td></tr>
				<tr><td><b>Address</b></td>
					<td><?= htmlspecialchars($o['customer']['si_address']) ?></td></tr>
				<tr><td><b>City</b></td>
					<td><?= htmlspecialchars($o['customer']['si_city']) ?></td></tr>
				<tr><td><b>State/Province</b></td>
					<td><?= htmlspecialchars($o['customer']['si_state']) ?></td></tr>
				<tr><td><b>Zip/Postal Code</b></td>
					<td><?= htmlspecialchars($o['customer']['si_zip']) ?></td></tr>
			</table>
		</td>
</table>
<p>
You have ordered a custom product that will take some time to be manufactured and shipped. 
Plan on receiving your purchase within 4-6 weeks. We will update you with tracking information for your product prior to being shipped. 
You are always welcome to inquire about the status of your order via phone or email. We are open 8am - 5pm PST, Mon-Fri. 
Our phone number is 509-919-0460 and email is <a href="mailto:revamp@revamppanels.com">revamp@revamppanels.com</a>
</p>
</td>
</tr>
</table>
</body>
</html>