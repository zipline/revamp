<?
$page['head'] = '<script src="/lib/revamp.min.js" type="text/javascript"></script>';

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$x_default = 24;
$y_default = 36;
$size_default = 63;
if(isset($_GET['id'])){
    $size_default = $_GET['id'];

    $default_p = sql_fetch_by_key($db, 'products', 'id', $_GET['id']);
    $x_default = $default_p['xsize'];
    $y_default = $default_p['ysize'];
}



$sizes = array();

$query = $db->prepare("SELECT * FROM products WHERE id_categories = '1' AND display ORDER BY priority ASC");
$query->execute();
$ss = $query->fetchall();

$available_options =  array();
/* SET SIZES FOR SIDEBAR */
foreach ($ss as $s) {
    //get list of included options for this size
    $available_option_single =  array();
    $query = $db->prepare("SELECT oid FROM product_options WHERE pid = ?");
    $query->execute(array($s['id']));
    $rr = $query->fetchall();
    foreach ($rr as $r) {
        $available_option_single[] = $r['oid'];
    }
    $s['available_options'] = $available_option_single;
    $available_options = array_unique(array_merge($available_options,$available_option_single), SORT_REGULAR);
    $sizes[] = $s;
}



/* SET OPTIONS FOR SIDEBAR */
$options = array();
$option_cat_display = array();

$cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
foreach ($cc as $c) {
    $query = $db->prepare("SELECT * FROM options WHERE id IN(".implode(',',$available_options).") AND id_categories = ? AND display ORDER BY priority ASC");
    $query->execute(array($c['id']));
    $rr = $query->fetchall();
    if (count($rr)) {
        $options[$c['title']]['cat'] = $c;
        $options[$c['title']]['opts'] = $rr;
    }
    if($c['option_id']){
        $option_cat_display[$c['title']] = $c['option_id'];
    }
}
//get list of options that exclude other options
$exclude_options = array();
foreach($available_options AS $option){
    $query = $db->prepare("SELECT hide_oid FROM option_hide WHERE oid = ?");
    $query->execute(array($option));
    $hidelist = $query->fetchAll();
    if($hidelist){
        $list = array();
        foreach($hidelist as $hl){
            $list[] = $hl['hide_oid'];
        }

        $exclude_options[$option] = $list;
    }
}

if(!$x_default && !$y_default) {
    $x_default = $sizes[0]['xsize'];
    $y_default = $sizes[0]['ysize'];
    $size_default = $sizes[0]['id'];
}

/* SET PANELS FOR MAIN CONTENT */
$patterns = array();
$default_img = $x_default . 'x' . $y_default . '.svg';

$rr = $db->query("SELECT * FROM options WHERE id IN(".implode(',',$available_options).") AND id_categories = '1' AND display ORDER BY priority ASC");
foreach ($rr as $r) {
    $link = '/pattern/'.$r['keyword'].'/';

    //prehide patterns that aren't available for currently selected size
    $hide = false;
    $query = $db->prepare("SELECT * FROM product_options WHERE pid=? AND oid=?");
    $query->execute(array($size_default,$r['id']));
    $po = $query->fetchAll();
    if(!$po){
        $hide = true;
    }

    $patterns[] = array('id' => $r['id'], 'title' => $r['title'],'link' => $link, 'hide'=>$hide, 'image' => '../upload/patterns/' . $r['id'] . '/' . $default_img);
}

$currentopts = $_GET['opt'];
$currentopts[] = $p['id'];

$filters = $twigpanel->render('panel_nav.twig', array(
    'options' => $options,
    'options_cat_display' => $option_cat_display,
    'exclude_options' => $exclude_options,
    'sizes' => $sizes,
    'default' => array('x'=>$x_default,'y'=>$y_default),
    'currentsize' => $size_default,
    'currentopts' => $currentopts,
    'panels' => $patterns
));

$content = $twigpanel->render('panels.twig', array(
    'panels' => $patterns,
	'filters' => $filters
));


$page['page_vars']['content'] .= $content;

