<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'shipping_locations';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Shipping Locations</h1>

                <div class="row">
                    <div class="col-md-12">
                        <div class="formfield">
                            <h2><?=$r['name'] ?></h2>
                            <br/>
                            <b>Shipping Price <span class="note">Static price that overrides other shipping costs (enter 0.00 to use normal shipping calculator instead)</span></b><br/>
                            $<input type="text" name="price" value="<?= htmlspecialchars($r['price']) ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';
