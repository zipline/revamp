
var vg;

$(document).ready(function () {


    var magnificPopup = $('.svg-thumb').magnificPopup({
        type: 'inline',
        closeOnContentClick: true,
        showCloseBtn: true,
        closeBtnInside: false
    });
    var magnificPopuptext = $('.text-popup').magnificPopup({
        type: 'inline',
        closeOnContentClick: true,
        showCloseBtn: true,
        closeBtnInside: false
    });
    //Calls the selectBoxIt method on your HTML select box.
    $(".purchase_options").each(function() {
        dom_id = '#'+$(this).attr('id');
        $(dom_id).selectBoxIt();
    });

    vg = $("#patterns-grid").vgrid({
        easing: "easeOutQuint",
        time: 400,
        delay: 20,
        fadeIn: {
            time: 500,
            delay: 50,
            wait: 500
        }
    });

    //triggers when the color option is changed
    $('option[data-color]').parent().change(function(){
        hex = $('option:selected', this).attr('data-color');
        svg_color_change('svg-target',hex);
    });

    //when changing the use option reset the available w/l values
    $('[name="use"]').change(function() {
        if($(this).val()!=''){
            reset_size_options(window[$(this).val()]);
        }else{
            reset_size_options(all_sizes);
        }
    });

    $('.size-form').change(function() {
        filename = $('option:selected', this).attr('data-image');
        filename2 = filename;
        selectedsize = $('option:selected').val();
        console.log('1'+filename);

        //disable unavailable options
        $('select[name^="opt["] option').each(function() {
            optval = $(this).val();
            if($.grep(sizes_options[selectedsize], function(e){ return e == optval; }).length > 0){
                $(this).removeAttr('disabled');
                $('.pattern-item[data-id='+$(this).attr('value')+']').show();
            }else{
                $(this).attr('disabled','disabled');
                //$('.pattern-item[data-id='+$(this).attr('value')+']').hide();
            }
        });

        $('.pattern-item').each(function() {
            var pat_id = $(this).attr('data-id');
            svg_resize($(this),filename2);
        });

        $('select.purchase_options[name^="opt["]').each(function() {
            dom_id = '#'+$(this).attr('id');
            console.log(dom_id);
            if($(this).val()===null) {
                $("select" + dom_id + " option").removeAttr('selected');
                $("select" + dom_id + " option:not(:disabled)").first().attr("selected", "selected").parent().change();
            }
            $("select" + dom_id).data("selectBox-selectBoxIt").refresh();

            svg_color_change('svg-target',$('option[data-color]:selected').attr('data-color'));
        });




        vg.vgrefresh(null, null, null, function(){ });
    });

    $('.svg-target').each(function() {
        var url = $(this).attr('imgsrc');
        load_svg(this, url);
    });
});



function load_svg(el, url){

    $(el).empty();

    var bbw = parseInt($(el).attr('width'));
    var bbh = parseInt($(el).attr('height'));

    var s = Snap(el);
    var w, h;
    var e = Snap.load(url, function (loadedFragment) {
        s.append(loadedFragment);

        //remove fill style for all contained elements
        s.selectAll('path').forEach(function (el) {
            el.attr('style', '');
        });
        s.selectAll('line').forEach(function (el) {
            el.attr('stroke', '');
        });

        var f = s.select('svg');
        w = parseInt(f.attr('width'));
        h = parseInt(f.attr('height'));


        f.attr('width', bbw);
        f.attr('height', bbh);
        s.attr({fill: "#" + svgcolor});

        //this will scale the image to relative to original size
        //s.attr({fill: "#"+svgcolor, "transform" : "t"+(bbw-w)/2+","+(bbh-h)/2+" s1.65"});
    });

}

function reset_size_options(sizes){
    var $width = $('[name="width"]');
    var $length = $('[name="length"]');
    $width.empty(); // remove old options
    $length.empty(); // remove old options

    var warr = [];
    var larr = [];

    $.each(sizes, function(index, value) {
        if(warr.indexOf(value[0]) > -1) {
            $width.append($("<option></option>").attr("value", value[0]).text(value[0]));
        }
        if(larr.indexOf(value[1]) > -1) {
            $length.append($("<option></option>").attr("value", value[1]).text(value[1]));
        }
        warr.push(value[0]);
        larr.push(value[1]);
    });
}


function svg_resize(obj,filename){
    obj.find('.svg-target').each(function(){
        image = this;

        file_path = $(image).attr('imgsrc');
        patharray = file_path.split("/");
        patharray.pop();
        file_path = patharray.join('/');

        url = file_path+'/'+filename;
        if($(image).attr('imgsrc')!= url) {
            $(image).attr('imgsrc', url);
            $(image).attr('href', url);
            load_svg(image, url);
        }
    });
}
function svg_color_change(classname,hex){
    svgcolor = hex;
    if(calcLuminance(hex) < 85) {
        $('.pattern-item').addClass('pattern-dark');
    }else{
        $('.pattern-item').removeClass('pattern-dark');
    }
    $('.svg-target').each(function() {
        $(this).attr({fill: "#"+hex});
    });
}

function calcLuminance(c){

    var rgb = parseInt(c, 16);   // convert rrggbb to decimal
    var r = (rgb >> 16) & 0xff;  // extract red
    var g = (rgb >>  8) & 0xff;  // extract green
    var b = (rgb >>  0) & 0xff;  // extract blue

    return 0.2126 * r + 0.7152 * g + 0.0722 * b;
}



/*************************
 * Panel page functions
 * ************************/

function change_panel_price(el){
    var options = {};
    options = $('[name^="opt["]').serialize();
    var qty = $('[name="qty"]').val();

    $('.pattern-item').each(function () {
        var optpat = options;
        var price_contain = $('.price', this);
        price_contain.html('<i class="fa fa-cog fa-spin"></i>');

        var pattern_id = $('[name="pattern_id"]', this).val();
        if(pattern_id) {
            optpat.replace('opt%5BPattern%5D', 'ignoreme');

            optpat += '&opt%5BPattern%5D=' + pattern_id;
        }
        $.ajax({
            type: 'POST',
            url: "/a_ajax.php?_=" + jQuery.now(),
            cache : false,
            data: {
                'a': 'get_price',
                'options': optpat,
                'qty': qty,
                'id': $('select[name="id"], input[name="id"]').val(),
            }
        }).done(function (content) {
            price_contain.html(content);
        });

    });
}
function change_panel_weight(el){
    var options = {};
    options = $('[name^="opt["]').serialize();
    var qty = $('[name="qty"]').val();

    $('.pattern-item').each(function () {
        var optpat = options;
        var weight_contain = $('.weight', this);
        weight_contain.html('<i class="fa fa-cog fa-spin"></i>');

        var pattern_id = $('[name="pattern_id"]', this).val();
        if(pattern_id) {
            optpat.replace('opt%5BPattern%5D', 'ignoreme');

            optpat += '&opt%5BPattern%5D=' + pattern_id;
        }

        $.ajax({
            type: 'POST',
            url: "/a_ajax.php?_=" + jQuery.now(),
            cache : false,
            data: {
                'a': 'get_weight',
                'options': optpat,
                'qty': qty,
                'id': $('select[name="id"], input[name="id"]').val(),
            }
        }).done(function (content) {
            weight_contain.html(content);
        });

    });
}