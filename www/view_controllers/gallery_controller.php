<?

$page['head'] = '<script src="/lib/isotope.pkgd.min.js" type="text/javascript"></script>';

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$basedir = "$basedir_photos";
$query = $db->prepare("SELECT *
		FROM photos
		WHERE id_parent = :id_parent
		ORDER BY priority ASC
		");
$query->execute(array(
    ':id_parent' => $page['page_vars']['gallery'],
));
$photos = $query->fetchAll();

$available_tags =  array();
$photo_final =  array();
foreach ($photos as $photo) {
    $query = $db->prepare("SELECT tid FROM photo_tags WHERE pid = ?");
    $query->execute(array($photo['id']));
    $rr = $query->fetchall();
    $individual_tags = array();
    foreach ($rr as $r) {
        $tag = sql_fetch_by_key($db, 'gallery_tags', 'id', $r['tid']);
        $c = sql_fetch_by_key($db, 'gallery_tag_cats', 'id', $tag['cat']);
        if($tag['display']) {
            $available_tags[] = $r['tid'];
            $individual_tags[] = $c['safename'].'-'.$tag['safename'];
        }
    }
    $photo['tags'] = $individual_tags;
    $photo_final[] = $photo;
}
$available_tags = array_unique($available_tags, SORT_REGULAR);

$tags = array();
foreach ($available_tags as $t) {
    $r = sql_fetch_by_key($db, 'gallery_tags', 'id', $t);
    $c = sql_fetch_by_key($db, 'gallery_tag_cats', 'id', $r['cat']);
    $tags[$c['title']][] = array('safename'=>$c['safename'].'-'.$r['safename'],'title'=>$r['title']);
}

$filters = $twigpanel->render('gallery_nav.twig', array(
    'page' => $page,
    'tags' => $tags
));

$content = $twigpanel->render('gallery.twig', array(
    'page'   => $page,
    'photos' => $photo_final,
	'filters' => $filters
));


$page['page_vars']['content'] = $content;
