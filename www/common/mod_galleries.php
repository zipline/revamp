<?
function gallery_get_main_photo($db, $gallery_id)
{
    $query = $db->prepare("SELECT * FROM photos WHERE featured = 1 AND type = 'galleries' AND id_parent = ?");
    $query->execute([$gallery_id]);
    return $query->fetch();
}

function gallery_display($db, $basedir_photos, $id_parent, $max = 100)
{

    $query = $db->prepare("SELECT * FROM galleries WHERE id=?");
    $query->execute(array($id_parent));
    $gallery = $query->fetch();

    $basedir = "$basedir_photos";
    $query = $db->prepare("SELECT *
		FROM photos
		WHERE id_parent = :id_parent
		ORDER BY priority ASC
		");
    $query->execute(array(
        ':id_parent' => $id_parent,
    ));
    $photos = $query->fetchAll();
    if ($query->rowCount()) {
        ob_start();
        $rel = "$type-$id_parent-gallery";
        ?>
        <div class="slick-gallery row">
            <div class="col-md-8">
                <div class="slick-img">
                    <?
                    $i = 0;
                    foreach ($photos as $photo) {
                        $imgpath = '/' . $basedir . '/thumb/' . $photo['image'];
                        $bigimgpath = '/' . $basedir . '/800/' . $photo['image'];
                        $zoomedimgpath = '/' . $basedir . '/1600/' . $photo['image'];
                        $link = '/' . $basedir . '/800/' . $photo['image'];
                        $i++;
                        ?>
                        <div class="slick-img-item">
                            <a href="<?=$zoomedimgpath ?>" class="pic_container">
                                <img alt="<?= $photo['alt'] ?>" <? if($i==1) echo 'itemprop="image"'; ?> src="<?= htmlspecialchars(addslashes($bigimgpath)) ?>"/>
                            </a>
                            <div class="caption"><h5><?= $photo['title'] ?></h5> <?= $photo['content']?$photo['content']:$gallery['description'] ?></div>
                        </div>
                    <?
                    }
                    ?>
                </div>
                <? if(count($photos)>1){ ?>
                <div class="slick-nav <?=(count($photos)==5) ? 'slick-five-fix':'' ?>">
                    <?
                    $i = 0;
                    foreach ($photos as $photo) {
                        $imgpath = '/' . $basedir . '/thumb/' . $photo['image'];
                        $bigimgpath = '/' . $basedir . '/800/' . $photo['image'];
                        $link = '/' . $basedir . '/800/' . $photo['image'];
                        ?>
                        <div class="slick-nav-item" data-slick-index="<?=$i ?>">
                            <img alt="<?= $photo['alt'] ?>" src="<?= htmlspecialchars(addslashes($imgpath)) ?>"/>
                        </div>
                    <?
                        $i++;
                    }
                    ?>
                </div>
                <? } ?>
            </div>
            <div class="col-md-4">
                <div class="slick-caption"></div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.slick-gallery').each(function( index ) {
                    var s_gallery = $(this);
                    var s_nav = $(this).find('.slick-nav');
                    var s_imgs = $(this).find('.slick-img');

                    <? if($i<5){ ?>
                        $('.slick-nav-item').click(function() {
                            s_imgs.slick("slickGoTo", $(this).attr('data-slick-index'))
                        });
                        s_imgs.slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: false,
                            prevArrow: '<div class="btn-prev-arrow"><i class="fa fa-chevron-left"></i></div>',
                            nextArrow: '<div class="btn-next-arrow"><i class="fa fa-chevron-right"></i></div>',
                            infinite: false,
                            draggable: false
                        }).on('afterChange', function(event, slick, currentSlide, nextSlide){
                            s_gallery.find('.slick-caption').html($('.slick-active').find('.caption').html());
                        });
                    <? }else{ ?>
                        s_imgs.slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: false,
                            prevArrow: '<div class="btn-prev-arrow"><i class="fa fa-chevron-left"></i></div>',
                            nextArrow: '<div class="btn-next-arrow"><i class="fa fa-chevron-right"></i></div>',
                            infinite: false,
                            asNavFor: s_nav,
                        }).on('afterChange', function(event, slick, currentSlide, nextSlide){
                            s_gallery.find('.slick-caption').html($('.slick-active').find('.caption').html());
                        });
                        s_nav.slick({
                            slidesToShow: <?= $i < 6 ? $i-1:5 ?>,
                            slidesToScroll: 5,
                            arrows: false,
                            asNavFor: s_imgs,
                            dots: true,
                            infinite: false,
                            centerMode: false,
                            focusOnSelect: true,
                            draggable: false,
                        });
                        s_nav.find('.slick-slide').click(function() {
                            s_imgs.slick("slickGoTo", $(this).attr('data-slick-index'))
                        });
                    <? } ?>

                    s_gallery.find('.slick-caption').html($('.slick-active').find('.caption').html());
                });
            });
        </script>
        <?
        $html = ob_get_clean();
        return $html;
    }
}

?>