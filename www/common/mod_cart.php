<?
function logged_in()
{
    global $_SESSION;
    if (isset($_SESSION['log']['username']) && isset($_SESSION['log']['id'])) {
        return true;
    } else {
        return false;
    }
}


function set_customer_data($customer)
{
    global $_SESSION;

    $_SESSION['customer']['bi_firstname'] = $customer['firstname'];
    $_SESSION['customer']['bi_lastname'] = $customer['lastname'];
    $_SESSION['customer']['bi_phone'] = $customer['phone'];
    $_SESSION['customer']['bi_city'] = $customer['city'];
    $_SESSION['customer']['bi_state'] = $customer['state'];
    $_SESSION['customer']['bi_zip'] = $customer['zip'];
    $_SESSION['customer']['bi_address'] = $customer['address'];
    $_SESSION['customer']['bi_email'] = $customer['email'];

}

/******** products and discounts ********/
function cart_get_product(PDO $db, $id)
{
    if (!is_numeric($id)) {
        return false;
    }
    $row = sql_fetch_by_key($db, 'products', 'id', $id);
    return $row;
}

function cart_get_product_url($product)
{
    return "product?id={$product['id']}";
}

function cart_get_category_url($category)
{
    return "category.php?id={$category['id']}";
}

/******** cart ********/
function cart_total_qty($cart)
{
    if (!is_array($cart)) {
        return 0;
    }
    $total_qty = 0;
    foreach ($cart as $item) {
        $total_qty += $item['qty'];
    }
    return $total_qty;
}

function cart_get_product_code($opts)
{

    $product_code_options = '';
    foreach (range(1, count($opts)) as $ma_option_num) {
        if ($opts['option_' . $ma_option_num]) {
            $selected_option_number = $opts['option_' . $ma_option_num];
            $product_code_options .= "$ma_option_num-$selected_option_number.";
        }
    }
    $product_code_options = rtrim($product_code_options, '.');
    $product_code = $opts['id'];
    if ($product_code_options) {
        $product_code .= ":$product_code_options";
    }
    return $product_code;
}

function cart_recreate_from_product_codes($crude_cart)
{
    $cart = array();
    foreach ($crude_cart as $crude_opts) {
        $opts = array();
        $opts['title'] = $crude_opts['title'];
        $opts['qty'] = $crude_opts['qty'];
        $product_code_string = $crude_opts['product_code']; // something like 124:1-15.2-4.3-1
        list($product_id, $product_options_string) = explode(':', $product_code_string);
        $opts['id'] = $product_id;
        $product_options_array = explode('.', $product_options_string);
        foreach ($product_options_array as $single_option_string) {
            if (!trim($single_option_string)) {
                continue;
            }
            list($ma_option_num, $selected_option_number) = explode('-', $single_option_string);
            $opts['option_' . $ma_option_num] = $selected_option_number;
        }
        $cart[] = $opts;
    }
    return $cart;
}

function cart_get_product_price(PDO $db, $cart_lineitem)
{

    $final_price = 0;
    $product = cart_get_product($db, $cart_lineitem['id']);

    /*
	// if this is an arbitrarily-priced item, get the price from the session and be done
	if ($product['arbitrary_price_allowed']) {
		return $cart_lineitem['arbitrary_price'] * $cart_lineitem['qty'];
	}
	// find base price of product
	if( $product['sale_price'] and time() < strtotime($product['sale_expiration']) ){
		$base_price = $product['sale_price'] ;
	}else{
		$base_price = $product['price'] ;
	}
	// find any extra pricing based on options
	*/


    $opt = array();

    $extra_price = 0;
    foreach (range(1, count($cart_lineitem)) as $i) {
        $selected_option = $cart_lineitem['option_' . $i];
        if ($selected_option) {
            /*
			$option_value = $db->prepare("SELECT price FROM product_options WHERE id=?");
			$option_value->execute(array($selected_option));
			$option_value = $option_value->fetch();
			$extra_price += $option_value['price'];
            */
            $opts[] = $selected_option;
        }
    }

    $final_price = get_product_price($db, $product['id'], array('opt' => $opts));

    //$final_price = $base_price + $extra_price ;
    d('Product price calculation');
    d('Base price: ' . $base_price);
    d('Extra price: ' . $extra_price);
    d('Final price: ' . $final_price);
    return $final_price;
}

function cart_get_product_taxable_price(PDO $db, $cart_lineitem)
{
    $product = cart_get_product($db, $cart_lineitem['id']);
    if ($product['taxable']) {
        return cart_get_product_price($db, $cart_lineitem);
    } else {
        return 0;
    }
}

function cart_get_option_details(PDO $db, $option_id)
{

    $option_value = $db->prepare("SELECT * FROM product_options WHERE id=?");
    $option_value->execute(array($option_id));
    $options = $option_value->fetch();

    return $options;
}

function cart_get_product_key($product_id, array $selected_options)
{

    $key = $product_id;
    foreach (range(1, count($selected_options)) as $i) {
        if ($selected_options["option_$i"]) {
            $key .= "-$i-{$selected_options['option_'.$i]}";
        }
    }
    $key = strtolower(preg_replace('@[^\w-]+@i', '-', $key));
    return $key;
}

function cart_get_discount_single(PDO $db, $cart, $discount_id)
{
    $total = 0;
    $discount = cart_get_discount($db, $discount_id);
    d('Discount calculation');
    d("discount id: $discount_id");
    d($discount);

    $carttotal = cart_get_subtotal($db,$cart);

    if($carttotal >= $discount['minimum']) {
        $total = ($discount['percent'] / 100) * $carttotal;
        $total += $discount['dollaroff'];
    }

    d("discount: $total");
    return round($total, 2);
}
function cart_get_discount(PDO $db, $discount_id){
    return sql_fetch_by_key($db, 'discounts', 'id', $discount_id);
}
function cart_find_key_by_id($cart, $id)
{
    $results = array();
    foreach ($cart as $key => $opts) {
        if ($id == $opts['id']) {
            $results[] = $key;
        }
    }
    return $results;
}

function cart_get_discount_total(PDO $db, $cart, $discounts)
{
    $total = 0;
    if (is_array($discounts)) {
        foreach ($discounts as $discount_id) {
            $total += cart_get_discount_single($db, $cart, $discount_id);
        }
    }
    return round($total, 2);
}

function cart_get_subtotal(PDO $db, $cart)
{
    $total = 0;
    if (is_array($cart)) {
        foreach ($cart as $key => $opts) {
            $id = $opts['id'];
            $price = cart_get_product_price($db, $opts);
            $total += $opts['qty'] * $price;
        }
    }
    return round($total, 2);
}

function cart_get_taxable_subtotal(PDO $db, $cart)
{
    $total = 0;
    if (is_array($cart)) {
        foreach ($cart as $key => $opts) {
            $id = $opts['id'];
            $price = cart_get_product_taxable_price($db, $opts);
            $total += $opts['qty'] * $price;
        }
    }
    return round($total, 2);
}

function cart_get_grand_total(PDO $db, $cart, $discounts, $customer)
{
    $total = cart_get_subtotal($db, $cart)
        - cart_get_discount_total($db, $cart, $discounts)
        + cart_get_shipping($db, $cart, $customer)
        + cart_get_tax($db, $cart, $discounts, $customer);
    return round($total, 2);
}

function cart_get_shipping(PDO $db, $cart, $customer)
{
    global $settings, $db;



    $r = sql_fetch_by_key($db, 'shipping_locations', 'abbreviation', $customer['si_state']);
    if($r['price']){
        return $r['price'];
    }

    $handling_fee = 1+floatval($settings['handling_fee']);

    $total = 0;
    $weight = cart_get_total_weight($db, $cart);
    $dimensions = cart_get_total_dimensions($db, $cart);
    $value = cart_get_subtotal($db, $cart);
    if($weight<1) $weight = 1;
    //FEDEX_FREIGHT_ECONOMY, FEDEX_GROUND
    $total = fedex_request($db, $customer, $weight, $dimensions, $value, "FEDEX_GROUND");
    if(!$total) { //ground failed so try freight
        $total = fedex_request($db, $customer, $weight, $dimensions, $value, "FEDEX_FREIGHT_ECONOMY");
    }

    return round($total*$handling_fee, 2);
}

function cart_get_total_weight(PDO $db, $cart){
    global $settings;
    $weight_addon = 1+floatval($settings['weight_addon']);

    $total = 0;
    if (is_array($cart)) {
        foreach ($cart as $key => $opts) {
            $id = $opts['id'];

            //convert cart options array into format get_product_weight can read
            $optionarray = array();
            foreach (range(1, count($opts)) as $i) {
                $selected_option = $opts['option_' . $i];
                if ($selected_option) {
                    $optionarray['opt'][] = $selected_option;
                }
            }

            $total += $opts['qty'] * get_product_weight($db, $id, $optionarray);
        }
    }
    return ceil($total*$weight_addon);
}
function cart_get_total_dimensions(PDO $db, $cart){
    global $settings;
    $dimension_addon = floatval($settings['dimension_addon']);

    $total_dims = array();
    if (is_array($cart)) {
        foreach ($cart as $key => $opts) {
            $id = $opts['id'];
            $p_dim = cart_get_product_dimensions($db,$id);
            //Add all z sizes and take the biggest of the x and y sizes to get single package dimensions
            $total_dims['z'] += $opts['qty'] * $p_dim['z'];
            if($total_dims['x']<$p_dim['x']){
                $total_dims['x']=$p_dim['x'];
            }
            if($total_dims['y']<$p_dim['y']){
                $total_dims['y']=$p_dim['y'];
            }
        }
        $total_dims['z'] = ceil($total_dims['z']);
    }
    $total_dims['x'] += $dimension_addon;
    $total_dims['y'] += $dimension_addon;
    $total_dims['z'] += $dimension_addon;


    return $total_dims;
}
function cart_get_product_dimensions(PDO $db, $id){
    $dims = array();
    $p = cart_get_product($db, $id);
    $dims['z'] = $p['zsize'];
    $dims['x'] = $p['xsize'];
    $dims['y'] = $p['ysize'];

    return $dims;
}


function cart_get_tax(PDO $db, $cart, $discounts, $customer)
{
    global $config;
    if (!is_array($config['tax_states'])) {
        return 0;
    }
    if (!in_array(strtoupper($customer['si_state']), $config['tax_states'])) {
        return 0;
    }
    $taxable_subtotal = cart_get_taxable_subtotal($db, $cart);
    $discounts = cart_get_discount_total($db, $cart, $discounts);
    $taxable_total = $taxable_subtotal - $discounts;
    $total = cart_get_dest_tax($db, $taxable_total, $customer['si_address'], $customer['si_city'], $customer['si_zip']);
    return round($total, 2);
}

function cart_get_dest_tax(PDO $db, $amount, $address, $city, $zip)
{

    global $config;


    require_once($config['tax_calc_include_path']);

    $total = 0;

    $rate_look = new WAXMLParser2($address, $city, $zip);
    $rate_look->readAttributes();

    // Check for and process any errors
    if (($error = $rate_look->checkErrorCode()) != FALSE) { // ERRORS FOUND!!
        #echo 'Tax rates not found, using default tax rate, see error below..<br>' ;
        $rate_look->setDefaultRate($config['tax_default_rate']); // Default Tax Rate at 8.6%
        #e($error) ;
    } else { // No Errors Found
        #echo 'Transaction may proceed, tax rates found..<br>' ;
    }
    $my_lookup = $rate_look->getTaxData();
    foreach ($my_lookup as $k => $v) {
        $tax_string .= "$k:$v;";
    }
    $_SESSION['tax_data'] = $tax_string;
    $rate = $my_lookup['rate'];
    $tax = $rate * $amount;
    return $tax;
}

function cart_display(PDO $db, $o)
{
    $cart = $o['cart'];
    $discounts = $o['discounts'];
    $customer = $o['customer'];
    $cart_shipping    = cart_get_shipping($db, $cart,$customer) ;
    $cart_tax = cart_get_tax($db, $cart, $discounts, $customer);
    $cart_grand_total = cart_get_grand_total($db, $cart, $discounts, $customer);
    if (isset($o['tax_override_amount'])) {
        $cart_tax = $o['tax_override_amount'];
    }
    if (isset($o['grand_total_override_amount'])) {
        $cart_grand_total = $o['grand_total_override_amount'];
    }
    // show_controls, show_tax
    /*
    ?>
    <tr>
        <td colspan="5">
            <?

            $weight = cart_get_total_weight($db, $cart);
            $dimensions = cart_get_total_dimensions($db, $cart);
            $value = cart_get_subtotal($db, $cart);
            echo 'weight:'.$weight;
            echo '<pre>Dimensions: ';
            print_r($dimensions);
            echo '</pre>';
            echo 'cubic feet: '.ceil(($dimensions['x'] * $dimensions['y'] * $dimensions['z'])/1728);
            ?>
        </td>
    </tr>
    */ ?>
    <tr>
        <th style="text-align: left ;">Product</th>
        <th style="text-align: left ;">Part Number</th>
        <th style="text-align: left ;">Price</th>
        <th style="text-align: left ;">Qty</th>
        <th colspan="2"></th>
    </tr>
    <?
    // show cart items
    foreach ($cart as $key => $opts) {
        $id = $opts['id'];
        $p = cart_get_product($db, $id);
        $price = cart_get_product_price($db, $opts);
        $qty_price = $opts['qty'] * $price;

        $opt_list = array();
        foreach (range(1, count($opts)) as $i) {
            $selected_option = $opts['option_' . $i];
            if ($selected_option) {
                /*
                $option_value = $db->prepare("SELECT price FROM product_options WHERE id=?");
                $option_value->execute(array($selected_option));
                $option_value = $option_value->fetch();
                $extra_price += $option_value['price'];
                */
                $opt_list[] = $selected_option;
            }
        }
        $inQuery = implode(',', $opt_list);
        $inurl = implode('&opt[]=', $opt_list);

        $query = $db->prepare("SELECT * FROM options WHERE id_categories = '1' AND id IN(?)");
        $query->execute(array($inQuery));
        $panel = $query->fetch();

        if ($p['id_categories'] == 1) {
            $link = "pattern/" . $panel['keyword'] . "/?opt[]=" . $inurl;
        } else {
            $link = "product?id=$id&opt[]=" . $inurl;
        }
        ?>
        <tr>
        <td><?
            if ($o['show_controls']) {
                ?><a href="/<?= $link ?>"><?= $opts['title'] ?></a><?
            } else {
                echo $opts['title'];
            }
            ?></td>
        <td><?= $opts['partnumber'];?></td>
        <td><?= fmt_price($price) ?></td>
        <?
        if ($o['show_controls']) {
            ?>
            <td><? write_select(array(
                    'rows' => range(0, 50),
                    'name' => 'qty',
                    'current' => $opts['qty'],
                    'attribs' => "onchange=\"window.location='checkout_process.php?a=cart-qty&key=$key&qty='+this[this.selectedIndex].value\"",
                ));
                ?></td>
        <?
        } else {
            ?>
            <td>
                <?= $opts['qty'] ?>
            </td>
        <?
        }
        ?>
        <td><?= fmt_price($qty_price) ?></td>
        <?
        if ($o['show_controls']) {
            ?>
            <td><a class="delete_btn" title="Delete" href="/checkout_process.php?a=cart-remove&key=<?= $key ?>"><i
                    class="fa fa-times-circle"></i></a></td><?
        }
        ?>
        </tr><?
    }
    // show discounts
    if (is_array($discounts)) {
        foreach ($discounts as $discount_id) {
            $discount = cart_get_discount($db, $discount_id);
            $discount_amt = cart_get_discount_single($db, $cart, $discount_id);
            if ($discount) {
                ?>
                <tr>
                <td style="text-align: right ;" colspan="4">Discount: <?= $discount['title'] ?></td>
                <td style="text-align: left ;" colspan="2">- <?= fmt_price($discount_amt) ?>
                </tr><?
            }
        }
    }
    if($cart_shipping) {
        ?>
        <tr>
            <td style="text-align: right ;" colspan="4">Shipping &amp; Handling</td>
            <td style="text-align: left ;" colspan="2"><?= fmt_price($cart_shipping) ?></td>
        </tr>
    <?
    }
    if ($o['show_tax']) {
        ?>
        <tr>
            <td style="text-align: right ;" colspan="4">Tax:</td>
            <td style="text-align: left ;" colspan="2"><?= fmt_price($cart_tax) ?></td>
        </tr>
    <?
    }
    ?>
    <tr>
        <td style="text-align: right ;" colspan="4"><b>Total</b></td>
        <td style="text-align: left ;" colspan="2"><b><?= fmt_price($cart_grand_total) ?></b></td>
    </tr>
<?
}

function cart_checkout_create_receipt($o)
{
    global $config;

    if (is_file($o['receipt_template_path'])) {
        ob_start();
        include $o['receipt_template_path'];
        $receipt = ob_get_clean();
    } else {
        exit('Could not generate receipt');
    }

    return $receipt;
}

function cart_checkout_create_and_save_receipt($db, $o)
{
    global $config;

    if (is_file($o['receipt_template_path'])) {
        ob_start();
        include $o['receipt_template_path'];
        $receipt = ob_get_clean();
    } else {
        exit('Could not generate receipt');
    }
    // save to db
    $query = $db->prepare("UPDATE orders SET
		receipt = :receipt
		WHERE id = :id_orders");
    $query->execute(array(
        ':receipt' => $receipt,
        ':id_orders' => $o['id_orders']
    ));

    return $receipt;
}

function cart_checkout_ip_pay_process_payment($amount, $order, $cc, $terminal_id, $ip_pay_url, $test_mode = false)
{

    $ipp_site_url = '';
    $amount_in_cents = $amount * 100;

    $xml_jetpay = new SimpleXMLElement("<JetPay/>");
    $xml_jetpay->addChild('TransactionType', 'SALE');
    $xml_jetpay->addChild('TerminalID', $terminal_id);
    $xml_jetpay->addChild('TransactionID', cart_new_ip_pay_transaction_id());
    $xml_jetpay->addChild('CardName', "{$order['bi_firstname']} {$order['bi_firstname']}");
    $xml_jetpay->addChild('CardNum', $cc['cc_number']);
    $xml_jetpay->addChild('CardExpMonth', str_pad($cc['cc_exp_month'], 2, '0', STR_PAD_LEFT));
    $xml_jetpay->addChild('CardExpYear', substr($cc['cc_exp_year'], -2, 2));
    $xml_jetpay->addChild('CVV2', $cc['cc_code']);
    $xml_jetpay->addChild('TotalAmount', $amount_in_cents);
    $xml_jetpay->addChild('BillingAddress', $order['bi_address']);
    $xml_jetpay->addChild('BillingCity', $order['bi_city']);
    $xml_jetpay->addChild('BillingStateProv', $order['bi_state']);
    $xml_jetpay->addChild('BillingPostalCode', $order['bi_zip']);
    $xml_jetpay->addChild('BillingCountry', $order['bi_country']);
    $xml_jetpay_str = $xml_jetpay->asXML();

    // following code is copied from IP Pay's "ippay.php", and modified
    $header = array();
    $header[0] = "Content-type: text/xml";
    $header[1] = "Content-length: " . strlen($xml_jetpay_str);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ip_pay_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_jetpay_str);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, "30");
    $jetpay_result = curl_exec($ch);

    if ($jetpay_result == NULL) {
        if ($test_mode) {
            e("Curl error: " . curl_errno($ch) . " - " . curl_error($ch));
        }
        return false;
    }

    curl_close($ch);

    try {
        $result_xml = new SimpleXMLElement($jetpay_result);
    } catch (Exception $e) {
        if ($test_mode) {
            e('Could not parse XML: ' . $e->getMessage() . ' - ' .
                htmlspecialchars($xml_jetpay_str) . ' - ' .
                htmlspecialchars($jetpay_result)
            );
        }
        return false;
    }
    $matches = $result_xml->xpath('ResponseText');
    $response_text = $matches[0] . '';
    if ($response_text != 'APPROVED') {
        e('Transaction Declined');
        if ($test_mode) {
            e(htmlspecialchars($xml_jetpay_str . ' - ' . $jetpay_result));
        }
        return false;
    }

    $result = array(
        'ordernumber' => cart_new_order_number(),
        'transaction_id' => $result_xml->xpath('TransactionID'),
        'invoice_number' => '',
        'first_name' => $order['bi_firstname'],
        'last_name' => $order['bi_lastname'],
        'address' => $order['bi_address'],
        'zip' => $order['bi_zip'],
        'response' => $jetpay_result,
    );

    return $result;
}

// process payment
function cart_checkout_authnet_process_payment($amount, $order, $cc)
{
    global $g_authnet_url, $g_authnet_login, $g_authnet_tran_key;

    $o = $order;
    $authnet_sale = false;
    $authnet_ordernumber = cart_new_order_number();
    $authnet_transtype = 'AUTH_CAPTURE';
    if ($_SESSION['test']) {
        $testmode = true;
    } else {
        $testmode = false;
    }
    $authnet_url = $g_authnet_url;
    $authnet_login = $g_authnet_login;
    $authnet_tran_key = $g_authnet_tran_key;

    $data = array(
        "x_Test_Request" => $testmode ? 'true' : 'false'
    , "x_Version" => "3.1"
    , "x_Delim_Data" => "True"
    , "x_Login" => $authnet_login
    , "x_Tran_Key" => $authnet_tran_key

    , "x_Method" => 'CC'
    , "x_Type" => $authnet_transtype
    , "x_Card_Num" => $cc['cc_number']
    , "x_Card_Code" => $cc['cc_code']
    , "x_Exp_Date" => $cc['cc_expdate']

    , "x_Amount" => $amount
    , "x_Tax" => $order['tax']
    , "x_Freight" => $order['shipping']
    , "x_Invoice_Num" => $authnet_ordernumber
    , "x_duplicate_window" => 300
    , "x_Description" => ''

    , "x_Customer_IP" => $_SERVER["REMOTE_ADDR"]
    , "x_Email" => $o['bi_email']

    , "x_First_Name" => $o['bi_firstname']
    , "x_Last_Name" => $o['bi_lastname']
    , "x_Company" => $o['bi_company']
    , "x_Address" => $o['bi_address']
    , "x_City" => $o['bi_city']
    , "x_State" => $o['bi_state']
    , "x_Zip" => $o['bi_zip']
    , "x_Country" => $o['bi_country']
    , "x_Phone" => $o['bi_phone']

    , "x_Ship_To_First_Name" => $o['si_firstname']
    , "x_Ship_To_Last_Name" => $o['si_lastname']
    , "x_Ship_To_Company" => $o['si_company']
    , "x_Ship_To_Address" => $o['si_address']
    , "x_Ship_To_City" => $o['si_city']
    , "x_Ship_To_State" => $o['si_state']
    , "x_Ship_To_Zip" => $o['si_zip']
    , "x_Ship_To_Country" => $o['si_country']
    , "x_Ship_To_Phone" => $o['si_phone']
    );
    $result = cart_authnet_transact($authnet_url, $data, "Design Spike");
    $result = explode(",", $result);

    if ($result[0] != 1) {
        e($result[3] . " ({$result[0]}-{$result[2]})");
    }
    if ($_SESSION['e']) {
        return false;
    } else {
        $return = array(
            'ordernumber' => $authnet_ordernumber,
            'transaction_id' => $result[6],
            'invoice_number' => $result[7],
            'first_name' => $result[13],
            'last_name' => $result[14],
            'address' => $result[16],
            'zip' => $result[19],
        );
        return $return;
    }
}

function cart_checkout_create_order_db_records($db, $o)
{
    global $g_download_expiration_days;
    $order_values = array(
        'order_tax' => $o['tax'],
        'order_shipping' => $o['shipping'],
        'order_grand_total' => $o['grand_total'],
        'purchase_type' => $o['purchase_type'],
        'bi_firstname' => $o['customer']['bi_firstname'],
        'bi_lastname' => $o['customer']['bi_lastname'],
        'bi_country' => $o['customer']['bi_country'],
        'bi_address' => $o['customer']['bi_address'],
        'bi_city' => $o['customer']['bi_city'],
        'bi_state' => $o['customer']['bi_state'],
        'bi_zip' => $o['customer']['bi_zip'],
        'bi_email' => $o['customer']['bi_email'],
        'bi_phone' => $o['customer']['bi_phone'],
        'si_firstname' => $o['customer']['si_firstname'],
        'si_lastname' => $o['customer']['si_lastname'],
        'si_country' => $o['customer']['si_country'],
        'si_address' => $o['customer']['si_address'],
        'si_city' => $o['customer']['si_city'],
        'si_state' => $o['customer']['si_state'],
        'si_zip' => $o['customer']['si_zip'],
        'transaction_id' => $o['transaction_id'],
        'ordernumber' => $o['ordernumber'],
        'tax_data' => $o['tax_data'],
        'referer' => $o['referer'],
        'id_customer' => $o['id_customer'],
    );
    $id_orders = sql_upsert($db, 'orders', array_keys($order_values), $order_values, null, null, "order_date = NOW()");

    // create line item records and/or download tickets for each item purchased
    foreach ($o['cart'] as $key => $opts) {
        $id_products = $opts['id'];
        $product = cart_get_product($db, $id_products);
        if ($product['downloadable'] && $product['filename']) {
            cart_create_download_ticket($db, $product['id'], $o['id_customer'], $id_orders);
        }

        $options = array();
        foreach (range(1, count($opts)) as $i) {
            $selected_option = $opts['option_' . $i];
            if ($selected_option) {
                $options[] = $selected_option;
            }
        }
        $options = implode('|',$options);

        // create line item record
        $query = $db->prepare("INSERT INTO order_items SET
			id_orders = :id_orders,
			id_products = :id_products,
			qty = :qty,
			options = :options"
        );
        $query->execute(array(
            ':id_orders' => $id_orders,
            ':id_products' => $id_products,
            ':qty' => $opts['qty'],
            ':options' => $options,
        ));

        $id_order_items = $db->lastInsertId();
    }
    return $id_orders;
}

function cart_authnet_transact($url, $fields, $user_agent)
{
    $string = "";
    foreach ($fields as $key => $value) $string .= "$key=$value&";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  // this line makes it work under https
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function cart_new_purchase_key()
{
    return cart_randstring(10);
}

function cart_new_order_number()
{
    return date("ymd-His-") . cart_randstring(6);
}

function cart_new_ip_pay_transaction_id()
{
    // following code was copied straight from from IP Pay's "ippay.php" sample code
    $dtest = "IP" . date('dmhs');
    $alph = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    for ($i = 0; $i < 8; $i++) {
        $dtest = $dtest . $alph[rand(0, 25)];
    }
    return $dtest;
}

function cart_randstring($len)
{
    $chars = '1234567890abcdef';
    $charslen = strlen($chars);
    $str = '';
    for ($i = 0; $i < $len; $i++) {
        $str .= $chars[rand(0, $charslen - 1)];
    }
    return $str;
}

function cart_add_product(PDO $db, array $cart, $product_id, array $selected_product_options = array(), $qty = 1, $arbitrary_price = null, $arbitrary_title = '', $partnumber_append = '')
{

    $product = cart_get_product($db, $product_id);
    $partnumber = $product['partnumber'].'-';
    if (!$product) {
        return $cart;
    }
    $key = cart_get_product_key($product_id, $selected_product_options);

    if (!$cart[$key]) {
        $title = $product['title'];
        foreach (range(1, count($selected_product_options)) as $i) {
            if ($selected_product_options["option_$i"]) {
                list($option_number, $option_name, $option_key) = explode('|', $selected_product_options['option_' . $i]);
                $option_number = htmlspecialchars($option_number);
                $option_name = htmlspecialchars($option_name);
                if($option_key && $option_name) {
                    $title .= "<div class='cart-option'>$option_key: " . $option_name . "</div>";
                }
                $cart[$key]['option_' . $i] = $option_number;


                $opt = sql_fetch_by_key($db, 'options', 'id', $option_number);

                $prepend_array = array(); //1,8,13
                $rr = $db->query("SELECT id FROM option_categories WHERE prepend_partnum");

                foreach ($rr as $r) {
                    $prepend_array[] = $r['id'];
                }


                if($opt['partnumber']) {
                    if (in_array($opt['id_categories'],$prepend_array)) {
                        $partnumber = $opt['partnumber'] . '-' . $partnumber;
                    } else {
                        $partnumber = $partnumber . $opt['partnumber'] . '-';
                    }
                }
            }
        }
        if ($arbitrary_title) {
            $title = $arbitrary_title;
        }
        $cart[$key]['title'] = $title;
        $cart[$key]['partnumber'] = trim($partnumber,'-');
        $cart[$key]['qty'] = $qty;
        $cart[$key]['id'] = $product_id;
        if ($product['arbitrary_price_allowed'] and $arbitrary_price and $arbitrary_price > 0) {
            $cart[$key]['arbitrary_price'] = round($arbitrary_price, 2);
        }
    }else{
        $cart[$key]['qty'] += $qty;
    }

    if($partnumber_append){
        $cart[$key]['partnumber'] = $cart[$key]['partnumber'] .'-'. $partnumber_append;
    }
    return $cart;
}

function cart_add_multiple_products(PDO $db, array $cart, array $product_ids)
{
    foreach ($product_ids as $product_id) {
        $cart = cart_add_product($db, $cart, $product_id);
    }
    return $cart;
}

function cart_create_download_ticket(PDO $db, $product, $customer, $id_orders)
{
    global $g_download_expiration_days;

    $table_name = 'download_tickets';

    $fields = array('id_products', 'id_customer', 'id_orders', 'purchase_key');

    $raw_values = $_POST;
    $raw_values['id_products'] = $product;
    $raw_values['id_customer'] = $customer;
    $raw_values['id_orders'] = $id_orders;
    $raw_values['purchase_key'] = cart_new_purchase_key();

    $insert_only_setlist = "expiration = DATE_ADD(NOW(),INTERVAL $g_download_expiration_days DAY), ";

    $values = sql_organize_values($fields, $raw_values);
    $id = sql_upsert($db, $table_name, $fields, $values, 'id', '', $insert_only_setlist);
}


function get_product_price(PDO $db, $product_id, $option = array())
{
    $price = 0;
    $product = cart_get_product($db, $product_id);
    $price = $product['price'];
    $finalprice = $price;
    if($option['opt']) {
        foreach ($option['opt'] as $o) {
            $row = sql_fetch_by_key($db, 'options', 'id', $o);
            if($row['price']!=0) {
                $finalprice = $finalprice * $row['price'];
            }

            if($row['price_fixed']!=0) {
                $finalprice = $finalprice + $row['price_fixed'];
            }
            //$price .= $row['title'];
        }
    }


    return $finalprice;
}
function get_product_weight(PDO $db, $product_id, $option = array())
{
    $weight = 0;
    $product = cart_get_product($db, $product_id);
    $weight = $product['weight'];
    $finalweight = $weight;
    if($option['opt']) {
        foreach ($option['opt'] as $o) {
            $row = sql_fetch_by_key($db, 'options', 'id', $o);
            if($row['weight']!=0) {
                $finalweight = $finalweight * $row['weight'];
            }
        }
    }
    return $finalweight;
}


class SoapClientHMAC extends SoapClient {
    public function __doRequest($request, $location, $action, $version, $one_way = NULL) {
        global $context, $config;
        $hmackey = $config['firstdata']['live_hmac']; // <-- Insert your HMAC key here
        $keyid = $config['firstdata']['live_key']; // <-- Insert the Key ID here
        $hashtime = date("c");
        $hashstr = "POST\ntext/xml; charset=utf-8\n" . sha1($request) . "\n" . $hashtime . "\n" . parse_url($location,PHP_URL_PATH);
        $authstr = base64_encode(hash_hmac("sha1",$hashstr,$hmackey,TRUE));
        if (version_compare(PHP_VERSION, '5.3.11') == -1) {
            ini_set("user_agent", "PHP-SOAP/" . PHP_VERSION . "\r\nAuthorization: GGE4_API " . $keyid . ":" . $authstr . "\r\nx-gge4-date: " . $hashtime . "\r\nx-gge4-content-sha1: " . sha1($request));
        } else {
            stream_context_set_option($context,array("http" => array("header" => "authorization: GGE4_API " . $keyid . ":" . $authstr . "\r\nx-gge4-date: " . $hashtime . "\r\nx-gge4-content-sha1: " . sha1($request))));
        }
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    public function SoapClientHMAC($wsdl, $options = NULL) {
        global $context;
        $context = stream_context_create();
        $options['stream_context'] = $context;
        return parent::SoapClient($wsdl, $options);
    }
}