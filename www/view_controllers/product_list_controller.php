<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$query = $db->prepare("SELECT * FROM products WHERE display AND id_categories=? ORDER BY priority ASC");
$query->execute(array($page['page_vars']['id_categories']));
$pp = $query->fetchall();

$products = array();
foreach ($pp as $p) {
    $query = $db->prepare("SELECT ph.* FROM photos ph, galleries g WHERE g.projectid=? AND g.id = ph.id_parent ORDER BY ph.priority ASC, ph.id ASC LIMIT 1");
    $query->execute(array($p['id']));
    if(count($query)) {
        $image = $query->fetch();
        $p['image'] = $image['image'];
    }
    $products[] = $p;
}

$content = $twigpanel->render('product_list.twig', array(
    'page' => $page,
    'products' => $products
));


$page['page_vars']['content'] .= $content;

