<?
include '../common/config.php';

$rr = $db->query("SELECT * FROM shipping_locations ORDER BY province ASC, name ASC");

if( ! admin_perm_check($db, $_SESSION['admin'], 'shipping_locations', $news['id']) ){
	exit('Permission denied') ;
}

include 'common/header.php' ;
?>
    <div class="row">
    <div class="col-lg-12">
    <h1>Shipping Locations</h1>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
	<table class="list">
		<tr>
			<th>Abbreviation</th>
			<th>Name</th>
			<th>Fixed Shipping Price</th>
		</tr>
		<?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			if(!$r['display']){ $class .= ' dead'; }
			?><tr class="<?= $class ?>">
				<td><?= htmlspecialchars($r['abbreviation']) ?></td>
				<td><?= htmlspecialchars($r['name']) ?></td>
				<td><?= $r['price']>0?'$'.$r['price']:'None' ?></td>
				<td><? edit_button("shipping_locations_.php?id={$r['id']}") ; ?></td>
				<td>
				<a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=display&amp;t=shipping_locations&amp;id=<?=$r['id'] ?>"
				title="Click to <?=$r['display']?'hide':'show' ?>">
                            <span class="fa fa-eye<?=$r['display']?'':'-slash' ?>"></span>                        </a>
				</td>
			</tr><?
		}
	?></table><?
}

include 'common/footer.php' ;

