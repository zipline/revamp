<?
#$page['head'] = '<script src="/lib/revamp.min.js" type="text/javascript"></script>';

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);



$tt = $db->query("SELECT * FROM blog_tags ORDER BY title ASC");
$blogstags = array();
foreach($tt as $t){

    $query = $db->prepare("SELECT t.id FROM blog_entry_tags t, blogs_entries e WHERE NOT e.hidden AND e.date_posted <= CURDATE() AND e.id=t.bid AND t.tid = ?");
    $query->execute(array($t['id']));
    $rr = $query->fetchall();
    $t['count'] = count($rr);
    if($t['count']) {
        $blogstags[] = $t;
    }
}


$sidebar = $twigpanel->render('media_nav.twig', array(
    'blogstags' => $blogstags,
    'current' => array('cat'=>$_GET['category'],'startd'=>$_GET['startd'],'endd'=>$_GET['endd'])
));

$where = 'AND e.date_posted <= CURDATE()';

if(isset($_GET['post'])){
    $blog = $db->prepare("SELECT e.* FROM blogs_entries e WHERE NOT e.hidden AND e.keyword = ? $where");
    $blog->execute(array($_GET['post']));
    $blog = $blog->fetchall();

    $content = $twigpanel->render('media-post.twig', array(
        'blog' => $blog[0]
    ));
}else {
    $where_vars = array();
    if($_GET['startd'] && $_GET['endd']){
        $where .= ' AND e.date_posted >= ? AND e.date_posted <= ? ';
        $where_vars[] = $_GET['startd'];
        $where_vars[] = $_GET['endd'];
    }

    if(isset($_GET['category'])) {
        $where_vars[] = $_GET['category'];

        $blogs = $db->prepare("SELECT e.* FROM blogs_entries e, blog_entry_tags t WHERE NOT e.hidden AND e.id = t.bid $where AND t.tid=? ORDER BY date_posted DESC");
        $blogs->execute($where_vars);
        $blogs = $blogs->fetchall();
        $r = sql_fetch_by_key($db, 'blog_tags', 'id', $_GET['category']);
        $page['page_vars']['heading'] .= ': '.$r['title'];

    }else{
        $blogs = $db->prepare("SELECT e.* FROM blogs_entries e WHERE NOT e.hidden AND e.id_blogs=1 $where ORDER BY date_posted DESC");
        $blogs->execute($where_vars);
        $blogs = $blogs->fetchall();
    }
    $content = $twigpanel->render('media.twig', array(
        'blogs' => $blogs
    ));
}


$page['sidebar_title'] = 'Filter Media <i class="fa fa-filter"></i>';

$page['page_vars']['sidebar'] .= $sidebar;
$page['page_vars']['content'] .= $content;

