<?
$id_pages = $_POST['id'] ;

$page = get_page($db, array('id'=>$id_pages)) ;

if( ! $page ){
	exit('Page not found for saving') ;
}

$save_draft    = isset($_POST['save_draft'])    ? true : false;
$publish_draft = isset($_POST['publish_draft']) ? true : false;

if ($save_draft and ! $_POST['version']) {
	$version_number_length = 16;
	$version_number_alphabet = '1234567890abcdefghijklmnopqrstuvwxyz';
	$version = 'draft-';
	foreach (range(1, $version_number_length) as $i) {
		$version .= $version_number_alphabet[rand(0, strlen($version_number_alphabet)-1)];
	}
} else {
	$version = isset($_POST['version']) ? $_POST['version'] : '';
}

foreach( $fields as $field ){
	try {
		$sql = "INSERT INTO pages_vars SET
				id_pages = :id_pages,
				title    = :title,
				value    = :value,
				date_created      = NOW(),
				id_admins_created = :id_admin,
				version = :version
			ON DUPLICATE KEY UPDATE
				value        = :value,
				date_updated = NOW(),
				id_admins_updated = :id_admin
				" ;
		$query = $db->prepare($sql) ;
		$query->execute(array(
			':id_pages' => $id_pages,
			':title'    => $field,
			':value'    => $_POST[$field],
			':id_admin' => $_SESSION['admin']['id'],
			':version'  => $version
		)) ;
	} catch (Exception $e) {
		if (! isset($_POST[$field])) {
			e('Programming error - field "'.$field.'" was expected but wasn\'t supplied.');
		} else {
			e('Error saving field "'.$field.'" to database.');
		}
	}
}

if ($publish_draft) {
	// delete vars that don't belong to this version
	$query = $db->prepare("DELETE FROM pages_vars WHERE id_pages = :id_pages AND version != :version");
	$query->execute(array(
		':id_pages'=>$id_pages,
		':version'=>$version,
	));
	// set vars that do belong to this version to have no version (they're the "main" or live ones now)
	$query = $db->prepare("UPDATE pages_vars SET version = '' WHERE version = :version AND id_pages = :id_pages");
	$query->execute(array(
		':id_pages'=>$id_pages,
		':version'=>$version,
	));
}

if ($save_draft) {
	$return_url = "pages_.php?id=$id_pages&version=$version";
}
