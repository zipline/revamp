<?
function get_blog_entry_url($blog, $entry){
	return "{$blog['keyword']}-entry-{$entry['id']}-".fmt_urlify($entry['title']) ;
}
function get_blog_url($blog){
	return $blog['keyword'] ;
}
function get_blog_rss_url($blog){
	return "{$blog['keyword']}/rss.xml" ;
}
function get_blog_disqus_identifier($blog){
	return "blog-entry-{$blog['id']}" ;
}
function get_page_disqus_identifier($page){
	return "page-{$page['id']}" ;
}

