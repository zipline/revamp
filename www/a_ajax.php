<?
include 'common/config.php';

switch ($_POST['a']) {
	case 'get_price' :
        $options = '';
        parse_str($_POST['options'],$options);

        echo '<span class="note">price:</span> $<span itemprop="price">'.number_format(round(get_product_price($db, $_POST['id'],$options)*$_POST['qty'],2),2).'</span>';
		break;
    case 'get_weight' :
        $options = '';
        parse_str($_POST['options'],$options);
        echo '<span class="note">weight:</span> '.ceil(get_product_weight($db, $_POST['id'],$options)*$_POST['qty']).'lbs';
        break;
}

