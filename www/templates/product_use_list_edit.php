<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}
$basedir_thumbnail = '../upload/photos/';
?>
<div class="formfield">
    <b>Page Heading</b><br/>
    <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
</div>
<div class="formfield">
    <b>Page Content</b><br/>
    <textarea name="content" class="rich_editor"
              style="width: 100% ; height: 400px ;"><?= $page_vars['content'] ?></textarea>
</div>


<div class="formfield">
    <b>Thumbnail <span class="note">Used on parent Product Use Listing page.  If empty, child page image will be used.</span></b>
    <?= cms_photo_selector(
        '',
        'thumbnail',
        $page_vars['thumbnail'],
        $basedir_thumbnail.'/thumb/',
        $basedir_thumbnail.'/400/'
    )
    ?>
    <? if($page_vars['thumbnail']){ ?>
        <br /><label>Clear Image <input type="checkbox" name="thumbnail_clear" /></label>
    <? } ?>
</div>