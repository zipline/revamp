<?
$fields = array('heading','content','products','gallery','related_products','hide_options','pn_append','hidden_option') ;

$basedir_thumbnail = '../upload/photos/';

if ($_FILES['thumbnail']['name']) {
    $tempimage = $_FILES['thumbnail'];
    list($image) = relocateImage($tempimage['tmp_name'], array(
        array('name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/orig"),
        array('width' => 1600, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/1600"),
        array('width' => 800, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/800"),
        array('width' => 400, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/400"),
        array('width' => 150, 'name' => $tempimage['name'], 'dir' => "$basedir_thumbnail/thumb")
    ));
    $_POST['thumbnail'] = $image;
    $fields[] = 'thumbnail';
} elseif ($_POST['thumbnail_clear']) {
    $_POST['thumbnail'] = '';
    $fields[] = 'thumbnail';
}


if($_POST['hide_option']) {
    $_POST['hide_options'] = implode(',', $_POST['hide_option']);
}else{
    $_POST['hide_options'] = '';
}

if($_POST['related_product']) {
    $_POST['related_products'] = implode(',', $_POST['related_product']);
}else{
    $_POST['related_products'] = '';
}

if($_POST['product']) {
    $_POST['products'] = implode(',', $_POST['product']);
}else{
    $_POST['products'] = '';
}

include '../templates/basic_template_save.php' ;

