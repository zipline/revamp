<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

//get lat/long for current location
if($_POST['location_latitude'] && $_POST['location_longitude']){
    //geolocation was used to set the lat/long and zip
    echo $_POST['location_latitude'].' '.$_POST['location_longitude'];

    $_SESSION['latitude'] = $_POST['location_latitude'];
    $_SESSION['longitude'] = $_POST['location_longitude'];
    if($_POST['location_zip'] != 'Zip Code'){
        $_SESSION['zip'] = $_POST['location_zip'];
    }
}elseif($_POST['location_zip']){
    //zip code was manually entered check database for lat/long
    $z = sql_fetch_by_key($db, 'postalcodes', 'postal', $_POST['location_zip']) ;
    if($z){
        $_SESSION['latitude'] = $z['latitude'];
        $_SESSION['longitude'] = $z['longitude'];
        $_SESSION['zip'] = $_POST['location_zip'];
    }else{
        e('Zip Code not found in database');
    }

}elseif(!$_SESSION['latitude'] && !$_SESSION['longitude']){
    //no data set so default to circle of security admin office
    $_SESSION['latitude'] = 47.658935;
    $_SESSION['longitude'] = -117.412736;
}


$querystring = "SELECT * FROM directory WHERE approved";
$params = array();
if($_POST['specialty'] || $_POST['credential'] || $_POST['language']){
    $placeholders = '';
    $vars = array($_POST['specialty'],$_POST['credential'],$_POST['language']);
    foreach ($vars as $var) {
        if($var) {
            $params[] = $var;
            $placeholders .= 'o.id = ? AND ';
        }
    }
    $placeholders = rtrim($placeholders,'AND ');
    $querystring = "SELECT d.* FROM directory d INNER JOIN directory_facilitator_options o ON d.id = o.did WHERE $placeholders AND d.approved";
}

//$page_vars['address'];
//$page_vars['address_google'] = str_replace(array(PHP_EOL,' '), array('',"+"), $page_vars['address']);


$types = array();
$query = $db->prepare("SELECT * FROM facilitator_options ORDER BY type, priority ASC") ;
$query->execute() ;
$rr = $query->fetchAll() ;
foreach($rr as $r){
    $types[$r['type']][$r['id']] = $r['title'];
}


if(!$_POST['distance']){
    $_POST['distance'] = 250;
}


$locations = '';
$location_results = array();

$query = $db->prepare($querystring) ;
$query->execute($params) ;
$rr = $query->fetchAll() ;
if( $rr ){
    foreach( $rr as $r ){

        if($r['coordinates']){
            list($c1,$c2) = explode(',',$r['coordinates']);
            $loc_distance = distance($c1, $c2, $_SESSION['latitude'], $_SESSION['longitude']);
            if($loc_distance <= $_POST['distance']){
                $location_results[$r['id']] = $r;
                $location_results[$r['id']]['distance'] = $loc_distance;

                $locations .= '["'. $r['name'] .'", "'. $r['address'] .'",'.$c1.','.$c2.'],';

                $opts = array();
                $query = $db->prepare("SELECT * FROM directory_facilitator_options WHERE did = ?") ;
                $query->execute(array($r['id'])) ;
                $options = $query->fetchAll() ;
                foreach($options as $o){
                    $option = sql_fetch_by_key($db, 'facilitator_options', 'id', $o['oid']) ;
                    $opts[$option['type']] .= $option['title'].', ';
                }
                //strip extra comma from end of each option type.
                foreach($opts as $t => $v){
                    $opts[$t] = rtrim($v,", ");
                }
                $location_results[$r['id']]['opts'] = $opts;
            }
        }
    }
}
$locations = rtrim($locations,',');


//$query = $db->prepare("SELECT value FROM pages_vars WHERE title='name'");
//$query->execute();
//$provider_options = $query->fetchAll();
//$provider_options = convert_to_1d($provider_options, 'value');
//$availableTags = '"'.implode('","',$provider_options).'"';

//
//$page_vars['content'] = preg_replace_callback(
//    '@\[gallery-(\d+)\]@',
//    function($matches) use ($db, $basedir_photos, $settings) {
//        return '<div class="gallery">'.gallery_display(
//            $db,
//            $basedir_photos,
//            'galleries',
//            $matches[1]
//        ).'</div>';
//    },
//    $page_vars['content']
//);

$content = $twigpanel->render('directory.twig', array(
    'availableTags' => $availableTags,
    'location_results' => $location_results,
    'locations' => $locations,
    'session' => $_SESSION,
    'post' => $_POST,
    'types' => $types,
    'config' => $config
));

$page['page_vars']['content'] .= $content;