<?
$keyword = $_GET['keyword'];

$page = get_page($db, array('keyword' => $_GET['keyword']), $_GET['cmsversion']);

if (!$page or ($_GET['cmsversion'] and !$_SESSION['admin']['id'])) {
	header("HTTP/1.0 404 Not Found");
	exit('Page Not Found');
}

$page['nav_keyword'] = $page['current_nav'] = $keyword; // for use in navigation in header
$page['nav_chain'] = find_nav_chain($db, $page); // find the IDs of the nav heirarchy, for use in header navigation
$page['breadcrumbs'] = get_breadcrumbs($page['nav_chain'], '&rsaquo;', true);

// set the title of last item in nav_chain - may have been set by special content include
$page['nav_chain_keys'] = array_keys($page['nav_chain']);
$page['nav_chain'][end($page['nav_chain_keys'])]['title'] = $page['title'];

$page = cms_render_page($db, $page, $settings, $config);
