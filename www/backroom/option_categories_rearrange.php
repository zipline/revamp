<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$rr = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");

include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('back', "option_categories.php", 'Back to Options'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Rearrange Option Categories</h1>

            <div class="rearrange-enable"></div>
            <script>
                $(document).ready(function () {
                    $('.rearrange-enable').trigger('click');
                });
            </script>
            <? print_rearrange_controls(false); ?>
            <? cms_show_sub_items($db, 'option_categories', 0, [], true, true); ?>
        </div>
    </div>
<?
include 'common/footer.php';
?>