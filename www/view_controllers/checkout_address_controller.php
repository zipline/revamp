<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);



$meta_title = 'Checkout - Billing and Shipping Information' ;
$current_nav = 'cart' ;

// make sure cart isn't empty
if( ! is_array($_SESSION['cart']) or ! $_SESSION['cart'] ){
    hle('/cart') ;
}

// retrieve customer info from cookie to session, if found
if( $_COOKIE['customer'] ){
    #error_reporting(E_ALL) ;
    $customer_array = unserialize(stripslashes($_COOKIE['customer'])) ;
    d('Customer Data') ;
    d($_COOKIE['customer']) ;
    d($customer_array) ;
    d('/Customer Data') ;
    if( is_array($customer_array) ){
        foreach( $customer_array as $k=>$v ){
            $_SESSION['customer'][$k] = $v ;
        }
    }
}

$form_attempt = form_load_clear('cart-shipping') ;
d('form_attempt') ;
d($form_attempt) ;
if( $form_attempt ){
    $r = $form_attempt ;
}else{
    $r = $_SESSION['customer'] ;
}

// set defaults
if( ! $_SESSION['customer'] ){
    $r['bi_country'] = 'USA' ;
    $r['si_country'] = 'USA' ;
}

ob_start();
write_countries('bi_country',$r['bi_country']);
$bi_country = ob_get_clean();

ob_start();
write_countries('si_country',$r['si_country']);
$si_country = ob_get_clean();

ob_start();
write_states_checkout('bi_state',$r['bi_state']);
$bi_state = ob_get_clean();

ob_start();
write_states_checkout('si_state',$r['si_state']);
$si_state = ob_get_clean();

$content = $twigpanel->render('cart_address.twig', array(
    'r' => $r,
    'config' => $config,
    'bi_country' => $bi_country,
    'si_country' => $si_country,
    'bi_state' => $bi_state,
    'si_state' => $si_state
));


$page['title'] = 'Shopping Cart: Address';
$page['page_vars']['content'] = $content;
