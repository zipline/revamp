<?

$page['head'] = '<script src="/lib/revamp.min.js" type="text/javascript"></script>';

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);



$p = sql_fetch_by_key($db, 'options', 'keyword', trim($_GET['pattern'],'/'));

$page['title'] =$p['title'];


/* SET OPTIONS FOR SIDEBAR */
$options = array();
$option_cat_display = array();

$cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
foreach ($cc as $c) {
    $query = $db->prepare("SELECT * FROM options WHERE id_categories = ? AND display ORDER BY priority ASC");
    $query->execute(array($c['id']));
    $rr = $query->fetchall();
    if (count($rr)) {
        $options[$c['title']]['cat'] = $c;
        $options[$c['title']]['opts'] = $rr;
    }
    if($c['option_id']){
        $option_cat_display[$c['title']] = $c['option_id'];
    }
}

/* SET SIZES FOR SIDEBAR */
$sizes = array();

$query = $db->prepare("SELECT * FROM products WHERE id_categories = '1' AND display ORDER BY priority ASC");
$query->execute();
$ss = $query->fetchall();

$available_options =  array();
foreach ($ss as $s) {
    $available_option_single =  array();
    $query = $db->prepare("SELECT oid FROM product_options WHERE pid = ?");
    $query->execute(array($s['id']));
    $rr = $query->fetchall();
    foreach ($rr as $r) {
        $available_option_single[] = $r['oid'];
    }

    $query = $db->prepare("SELECT oid FROM product_options WHERE pid = ? AND oid = ?");
    $query->execute(array($s['id'],$p['id']));
    $rr = $query->fetchall();
    if(count($rr)){
        //only add sizes that are available for the current pattern
        $s['available_options'] = $available_option_single;
        $sizes[] = $s;
        $available_options = array_unique(array_merge($available_options,$available_option_single), SORT_REGULAR);
    }
    if($_GET['id']==$s['id']){
        $x_default = $s['xsize'];
        $y_default = $s['ysize'];
    }
}

if(!$x_default || !$y_default) {
    $x_default = $sizes[0]['xsize'];
    $y_default = $sizes[0]['ysize'];
}

/* SET OPTIONS FOR SIDEBAR */
$options = array();
$option_cat_display = array();

$cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
foreach ($cc as $c) {
    $query = $db->prepare("SELECT * FROM options WHERE id IN(".implode(',',$available_options).") AND id_categories = ? AND display ORDER BY priority ASC");
    $query->execute(array($c['id']));
    $rr = $query->fetchall();
    if (count($rr)) {
        $options[$c['title']]['cat'] = $c;
        $options[$c['title']]['opts'] = $rr;
    }
    if($c['option_id']){
        $option_cat_display[$c['title']] = $c['option_id'];
    }
}
//get list of optoins that exclude other options
$exclude_options = array();
foreach($available_options AS $option){
    $query = $db->prepare("SELECT hide_oid FROM option_hide WHERE oid = ?");
    $query->execute(array($option));
    $hidelist = $query->fetchAll();
    if($hidelist){
        $list = array();
        foreach($hidelist as $hl){
            $list[] = $hl['hide_oid'];
        }

        $exclude_options[$option] = $list;
    }
}


/* SET PANELS FOR MAIN CONTENT */
$patterns = array();
$default_img = $x_default . 'x' . $y_default . '.svg';

$rr = $db->query("SELECT * FROM options WHERE id IN(".implode(',',$available_options).") AND id_categories = '1' AND display ORDER BY priority ASC");
foreach ($rr as $r) {
    $link = '/pattern/'.$r['keyword'].'/';
    $patterns[] = array('id' => $r['id'], 'title' => $r['title'],'link' => $link, 'image' => '../upload/patterns/' . $r['id'] . '/' . $default_img);
}
$p['image'] = '/upload/patterns/' . $p['id'] . '/' . $default_img;


$currentopts = $_GET['opt'];
$currentopts[] = $p['id'];



$filter = $twigpanel->render('pattern_nav.twig', array(
    'options' => $options,
    'options_cat_display' => $option_cat_display,
    'exclude_options' => $exclude_options,
    'sizes' => $sizes,
    'default' => array('x'=>$x_default,'y'=>$y_default),
    'currentopts' => $currentopts,
    'selectedsize' => $_GET['id'],
    'p' => $p,
    'page' => $page
));

$content = $twigpanel->render('pattern.twig', array(
    'p' => $p,
    'filter' => $filter
));

$page['page_vars']['content'] = $content;
