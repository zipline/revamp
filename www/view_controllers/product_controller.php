<?
$page['head'] = '<script src="/lib/revamp.min.js" type="text/javascript"></script>';

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);


$query = $db->prepare("SELECT * FROM products WHERE display AND id=? ORDER BY priority ASC");
$query->execute(array($_GET['id']));
$pp = $query->fetch();

$query = $db->prepare("SELECT * FROM product_categories WHERE id=?");
$query->execute(array($pp['id_categories']));
$pc = $query->fetch();



$query = $db->prepare("SELECT oid FROM product_options WHERE pid = ?");
$query->execute(array($pp['id']));
$ao = $query->fetchall();
$available_options = array();

foreach($ao as $a){
    $available_options[]= $a['oid'];
}


$available_sizes = explode(',',$page['page_vars']['products']);


/* SET OPTIONS FOR SIDEBAR */
$options = array();
$option_cat_display = array();
$svg_cat = 0;
$svg_ref = 'none-exists';
$cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
if($available_options) {
    foreach ($cc as $c) {
        $query = $db->prepare("SELECT * FROM options WHERE id IN(" . implode(',', $available_options) . ") AND id_categories = ? AND display ORDER BY priority ASC");
        $query->execute(array($c['id']));
        $rr = $query->fetchall();
        if (count($rr)) {
            if ($c['svg_cat']) {
                $svg_cat = $rr[0]['id'];
                $svg_ref = 'opt-'.str_replace(' ','-',$c['title']);
            }
            $options[$c['title']]['cat'] = $c;
            $options[$c['title']]['opts'] = $rr;
        }
        if ($c['option_id']) {
            $option_cat_display[$c['title']] = $c['option_id'];
        }
    }
}

//get list of optoins that exclude other options
$exclude_options = array();
foreach($available_options AS $option){
    $query = $db->prepare("SELECT hide_oid FROM option_hide WHERE oid = ?");
    $query->execute(array($option));
    $hidelist = $query->fetchAll();
    if($hidelist){
        $list = array();
        foreach($hidelist as $hl){
            $list[] = $hl['hide_oid'];
        }

        $exclude_options[$option] = $list;
    }
}




$query = $db->prepare("SELECT * FROM galleries WHERE projectid = ?");
$query->execute(array($pp['id']));
$gallery = $query->fetch();

$basedir_photos = 'upload/photos';

$pp['gallery'] = gallery_display($db, $basedir_photos, $gallery['id']);


$currentopts = $_GET['opt'];
//$currentopts[] = $pp['id'];


$query = $db->prepare("SELECT p.keyword, p.title FROM pages_vars v, pages p WHERE p.id=v.id_pages AND v.title='id_categories' AND v.value = ?");
$query->execute(array($pp['id_categories']));
$cat_page = $query->fetch();

$returnlink = '<a href="/products" class="panel-link prm-btn">Back to Products</a>';
if($cat_page['keyword']){
    $returnlink = '<a href="/'.$cat_page['keyword'].'" class="panel-link prm-btn">Back to '.$cat_page['title'].'</a>';
}


$related = array();
$query = $db->prepare("SELECT pid_target FROM products_related WHERE pid = ? ORDER BY RAND() LIMIT 5");
$query->execute(array($pp['id']));
$related_products = $query->fetchAll();
foreach($related_products AS $rp){
    $p2 = sql_fetch_by_key($db, 'products', 'id', $rp['pid_target']);
    if($p2 && $p2['id_categories']!=1) {
        $query = $db->prepare("SELECT ph.* FROM photos ph, galleries g WHERE g.projectid=? AND g.id = ph.id_parent ORDER BY ph.priority LIMIT 1");
        $query->execute(array($p2['id']));
        if (count($query)) {
            $image = $query->fetch();
            $p2['image'] = $image['image'];
        }
        $related[$p2['id']] = $p2;
    }
}

$content = $twigpanel->render('product.twig', array(
    'returnlink' => $returnlink,
    'related' => $related,
    'p' => $pp
));
if($pc['svg_cat']) {
    $x_default = $pp['xsize'];
    $y_default = $pp['ysize'];
    $default_img = $x_default . 'x' . $y_default . '.svg';
    $pp['image'] = '/upload/patterns/' . $svg_cat . '/' . $default_img;
    $sidebar = $twigpanel->render('product_nav_svg.twig', array(
        'options' => $options,
        'options_cat_display' => $option_cat_display,
        'exclude_options' => $exclude_options,
        'default' => array('x'=>$x_default,'y'=>$y_default),
        'currentopts' => $currentopts,
        'svg_ref' => $svg_ref,
        'p' => $pp
    ));
}else {
    $sidebar = $twigpanel->render('product_nav.twig', array(
        'options' => $options,
        'exclude_options' => $exclude_options,
        'options_cat_display' => $option_cat_display,
        'currentopts' => $currentopts,
        'p' => $pp
    ));
}

$page['title'] = $pp['title'];
$page['page_vars']['content'] = $content;
$page['page_vars']['sidebar'] = $sidebar;
