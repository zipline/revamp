<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$where = 'WHERE enddate >= NOW()';

$query = $db->prepare("SELECT DISTINCT location FROM trainings $where ORDER BY location") ;
$query->execute() ;
$locations = $query->fetchAll() ;

if($_POST['location']){
    $where .= " AND location like '%".$_POST['location']."%'";
}
if($_POST['type']){
    $where .= " AND type = '".$_POST['type']."'";
}
if($_POST['search']){
    $where .= " AND ( location like '%".$_POST['search']."%'";
    $where .= " OR date_time like '%".$_POST['search']."%'";
    $where .= " OR venue like '%".$_POST['search']."%'";
    $where .= " OR cost like '%".$_POST['search']."%'";
    $where .= " OR presenter like '%".$_POST['search']."%'";
    $where .= " OR description like '%".$_POST['search']."%'";
    $where .= " OR registration_text like '%".$_POST['search']."%'";
    $where .= " OR alt_registration_text like '%".$_POST['search']."%' )";
}


$query = $db->prepare("SELECT * FROM training_types ORDER BY title") ;
$query->execute() ;
$typetemp = $query->fetchAll() ;

$types = array();
foreach($typetemp as $t){
    $types[$t['id']] = $t;
}

$query = $db->prepare("SELECT * FROM trainings $where") ;
$query->execute() ;
$trainings = $query->fetchAll() ;




$content = $twigpanel->render('trainings.twig', array(
    'types' => $types,
    'trainings' => $trainings,
    'locations' => $locations,
    'session' => $_SESSION,
    'post' => $_POST,
    'config' => $config
));

$page['page_vars']['content'] .= $content;