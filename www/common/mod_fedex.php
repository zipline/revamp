<?

require_once(__DIR__.'/../lib/fedex/library/fedex-common.php');

$path_to_wsdl = __DIR__.'/../lib/fedex/RateService_v16.wsdl';

function fedex_request($db, $customer, $weight, $dimensions,$value, $servicetypecode = "FEDEX_FREIGHT_ECONOMY"){
    global $path_to_wsdl;

    ini_set("soap.wsdl_cache_enabled", "0");

    $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

    $request = fedex_build_request($db, $customer, $weight, $dimensions,$value, $servicetypecode);

    try{
        if(setEndpoint('changeEndpoint')){
            $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        }

        $response = $client -> getRates($request);

        if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
            /*
            $rateReply = $response -> RateReplyDetails;
            echo '<table border="1">';
            echo '<tr><th>Rate Details</th><th>&nbsp;</th></tr>';
            trackDetails($rateReply, '');
            echo '</table>';

            */
            //printSuccess($client, $response);
            return $response->RateReplyDetails->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount;
        }else{
            //printError($client, $response);
            return false;
        }

        //writeToLog($client);    // Write to log file
    } catch (SoapFault $exception) {
        //printFault($exception, $client);
    }
}

function fedex_build_request($db, $customer,  $weight, $dimensions,$value, $servicetypecode){
    $request = array();
    $request['WebAuthenticationDetail'] = array(
        'UserCredential' =>array(
            'Key' => getProperty('key'),
            'Password' => getProperty('password')
        )
    );
    $request['ClientDetail'] = array(
        'AccountNumber' => getProperty('shipaccount'),
        'MeterNumber' => getProperty('meter')
    );
    $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request using PHP ***');
    $request['Version'] = array(
        'ServiceId' => 'crs',
        'Major' => '16',
        'Intermediate' => '0',
        'Minor' => '0'
    );

    $request['ReturnTransitAndCommit'] = true;
    $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
    $request['RequestedShipment']['ShipTimestamp'] = date('c');
    $request['RequestedShipment']['ServiceType'] = $servicetypecode; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
    $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...

    $request['RequestedShipment']['Recipient'] = array(
        'Contact' => array(
            'PersonName' => $customer['si_firstname'].' '.$customer['si_lastname'],
            'CompanyName' => $customer['si_company'],
            'PhoneNumber' => $customer['si_phone']
        ),
        'Address' => array(
            'StreetLines' => array($customer['si_address']),
            'City' => $customer['si_city'],
            'StateOrProvinceCode' => strtoupper($customer['si_state']),
            'PostalCode' => $customer['si_zip'],
            'CountryCode' => $customer['si_country']
        )
    );
    $request['RequestedShipment']['PackageCount'] = '1';

    if($servicetypecode == "FEDEX_FREIGHT_ECONOMY") {
        $request['RequestedShipment']['ShippingChargesPayment'] = array(
            'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor' => array(
                'ResponsibleParty' => array(
                    'AccountNumber' => getProperty('freightaccount'),
                    'CountryCode' => 'US')
            )
        );
        $request['RequestedShipment']['Shipper'] =  getProperty('shipper');
        $request['RequestedShipment']['FreightShipmentDetail'] = array(
            'FedExFreightAccountNumber' => getProperty('freightaccount'),
            'FedExFreightBillingContactAndAddress' => getProperty('freightbilling'),
            'Role' => 'SHIPPER',
            'PaymentType' => 'PREPAID',
            'CollectTermsType' => 'STANDARD',
            'DeclaredValuePerUnit' => array(
                'Currency' => 'USD',
                'Amount' => $value
            ),
            'LiabilityCoverageDetail' => array(
                'CoverageType' => 'NEW',
                'CoverageAmount' => array(
                    'Currency' => 'USD',
                    'Amount' => $value
                )
            ),
            'TotalHandlingUnits' => 15,
            'ClientDiscountPercent' => 0,
            'PalletWeight' => array(
                'Units' => 'LB',
                'Value' => 20
            ),
            'ShipmentDimensions' => array(
                'Length' => $dimensions['x'],
                'Width' => $dimensions['z'],
                'Height' => $dimensions['y'],
                'Units' => 'IN'
            ),
            'LineItems' => array(
                'FreightClass' => 'CLASS_050',
                'ClassProvidedByCustomer' => false,
                'HandlingUnits' => 15,
                'Packaging' => 'PALLET',
                'BillOfLaddingNumber' => 'BOL_12345',
                'PurchaseOrderNumber' => 'PO_12345',
                'Description' => 'Revamp Panels',
                'Weight' => array(
                    'Value' => $weight,
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => $dimensions['x'],
                    'Width' => $dimensions['z'],
                    'Height' => $dimensions['y'],
                    'Units' => 'IN'
                ),
                'Volume' => array(
                    'Units' => 'CUBIC_FT',
                    'Value' => ceil(($dimensions['x'] * $dimensions['y'] * $dimensions['z'])/1728)
                )
            )
        );
    }else{
        $request['RequestedShipment']['ShippingChargesPayment'] = array(
            'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor' => array(
                'ResponsibleParty' => array(
                    'AccountNumber' => getProperty('billaccount'),
                    'CountryCode' => 'US'
                )
            )
        );
        $request['RequestedShipment']['Shipper'] = addShipper();
        $request['RequestedShipment']['RequestedPackageLineItems'] = array(
            'SequenceNumber'=>1,
            'GroupPackageCount'=>1,
            'Weight' => array(
                'Value' => $weight,
                'Units' => 'LB'
            ),
            'Dimensions' => array(
                'Length' => $dimensions['x'],
                'Width' => $dimensions['z'],
                'Height' => $dimensions['y'],
                'Units' => 'IN'
            )
        );
    }
    return $request;
}



function addShipper(){
    $shipper = array(
        'Contact' => array(
            'PersonName' => 'Tom Novotney',
            'Title' => 'Manager',
            'CompanyName' => 'Revamp Panels',
            'PhoneNumber' => '5099191014'
        ),
        'Address' => array(
            'StreetLines' => array('1711 N Madson St'),
            'City' => 'Liberty Lake',
            'StateOrProvinceCode' => 'WA',
            'PostalCode' => '99019',
            'CountryCode' => 'US'
        )
    );
    return $shipper;
}




function addRecipient(){
    $recipient = array(
        'Contact' => array(
            'PersonName' => 'Mike Smith',
            'CompanyName' => 'Random Company',
            'PhoneNumber' => '5095555555'
        ),
        'Address' => array(
            'StreetLines' => array('123 N Main'),
            'City' => 'Spokane  ',
            'StateOrProvinceCode' => 'WA',
            'PostalCode' => '99201',
            'CountryCode' => 'US'
        )
    );
    return $recipient;
}
function addShippingChargesPayment(){
    $shippingChargesPayment = array(
        'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
        'Payor' => array(
            'ResponsibleParty' => array(
                'AccountNumber' => getProperty('freightaccount'),
                'CountryCode' => 'US')
        )
    );
    return $shippingChargesPayment;
}