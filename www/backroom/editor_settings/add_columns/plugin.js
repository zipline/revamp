/*
 * Embed Media Dialog based on http://www.fluidbyte.net/embed-youtube-vimeo-etc-into-ckeditor
 *
 * Plugin name:      mediaembed
 * Menu button name: MediaEmbed
 *
 * Youtube Editor Icon
 * http://paulrobertlloyd.com/
 *
 * @author Fabian Vogelsteller [frozeman.de]
 * @version 0.4
 */
( function() {
    CKEDITOR.plugins.add( 'add_columns',
        {
            init: function( editor )
            {
                var me = this;
                var columns = '1-1';
                CKEDITOR.dialog.add( 'add_columnsDialog', function (instance)
                {
                    return {
                        title : 'Add Columns',
                        minWidth : 250,
                        minHeight : 100,
                        contents :
                            [
                                {
                                    id : 'iframe',
                                    expand : true,
                                    elements :[{
                                        label : 'Column Structure',
                                        type: 'select',
                                        items: [
                                            ['1-1'],
                                            ['1-1-1'],
                                            ['1-1-1-1'],
                                            ['1-2'],
                                            ['1-1-2'],
                                            ['1-2-1'],
                                            ['2-1'],
                                            ['2-1-1'],
                                            ['1-3'],
                                            ['3-1']
                                        ],
                                        id: 'columncount',
                                        'default': 2,
                                        onChange: function(api){
                                            columns = this.getValue();
                                        }
                                    }]
                                }
                            ],
                        onOk: function() {
                            switch(columns){
                                case '1-1-1':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-4"><p>Column 1</p></div><div class="col-md-4"><p>Column 2</p></div><div class="col-md-4"><p>Column 3</p></div></div>');
                                    break;
                                case '1-2':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-4"><p>Column 1</p></div><div class="col-md-8"><p>Column 2</p></div></div>');
                                    break;
                                case '2-1':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-8"><p>Column 1</p></div><div class="col-md-4"><p>Column 2</p></div></div>');
                                    break;
                                case '1-1-1-1':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-3"><p>Column 1</p></div><div class="col-md-3"><p>Column 2</p></div><div class="col-md-3"><p>Column 3</p></div><div class="col-md-3"><p>Column 4</p></div></div>');
                                    break;
                                case '1-1-2':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-3"><p>Column 1</p></div><div class="col-md-3"><p>Column 2</p></div><div class="col-md-6"><p>Column 3</p></div></div>');
                                    break;
                                case '1-2-1':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-3"><p>Column 1</p></div><div class="col-md-6"><p>Column 2</p></div><div class="col-md-3"><p>Column 3</p></div></div>');
                                    break;
                                case '2-1-1':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-6"><p>Column 1</p></div><div class="col-md-3"><p>Column 2</p></div><div class="col-md-3"><p>Column 3</p></div></div>');
                                    break;
                                case '1-3':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-3"><p>Column 1</p></div><div class="col-md-9"><p>Column 2</p></div></div>');
                                    break;
                                case '3-1':
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-9"><p>Column 1</p></div><div class="col-md-3"><p>Column 2</p></div></div>');
                                    break;
                                default:
                                    this._.editor.insertHtml('<div class="row"><div class="col-md-6"><p>Column 1</p></div><div class="col-md-6"><p>Column 2</p></div></div>');
                            }

                        }
                    };
                } );

                editor.addCommand( 'add_columns', new CKEDITOR.dialogCommand( 'add_columnsDialog' ) );

                editor.ui.addButton( 'add_columns',
                    {
                        label: 'Add Columns',
                        command: 'add_columns',
                        icon: this.path + 'images/icon.png',
                        toolbar: 'add_columns'
                    } );
            }
        } );
} )();
