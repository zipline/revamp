<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'gallery_tags';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "gallery_tags.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Gallery Tag</h1>

                <div class="formfield">
                    <b>Title</b><br/>
                    <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"/>
                </div>
                <div class="formfield">
                    <b>Category</b><br/><?
                    write_select(array(
                        'rows' => $db->query("SELECT * FROM gallery_tag_cats ORDER BY title ASC"),
                        'value' => 'id',
                        'label' => 'title',
                        'name' => 'cat',
                        'current' => $r['cat'],
                        'display_length' => 35,
                    ));
                    ?>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';
