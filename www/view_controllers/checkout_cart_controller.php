<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$product_list = '';
foreach ($_SESSION['cart'] as $key => $opts) {
    $product_list .= $opts['id'].', ';
}
if($product_list){
    $related = array();
    $query = $db->prepare("SELECT * FROM products_related WHERE pid_target IN(".trim($product_list,' ,').") OR pid IN(".trim($product_list,' ,').") ORDER BY RAND() LIMIT 5");
    $query->execute();
    $related_products = $query->fetchAll();
    foreach($related_products AS $rp){
        $p = sql_fetch_by_key($db, 'products', 'id', $rp['pid']);
        if($p && $p['id_categories']!=1) {
            $query = $db->prepare("SELECT ph.* FROM photos ph, galleries g WHERE g.projectid=? AND g.id = ph.id_parent ORDER BY ph.priority LIMIT 1");
            $query->execute(array($p['id']));
            if (count($query)) {
                $image = $query->fetch();
                $p['image'] = $image['image'];
            }
            $related[$p['id']] = $p;
        }
        $p = sql_fetch_by_key($db, 'products', 'id', $rp['pid_target']);
        if($p && $p['id_categories']!=1) {
            $query = $db->prepare("SELECT ph.* FROM photos ph, galleries g WHERE g.projectid=? AND g.id = ph.id_parent ORDER BY ph.priority LIMIT 1");
            $query->execute(array($p['id']));
            if (count($query)) {
                $image = $query->fetch();
                $p['image'] = $image['image'];
            }
            $related[$p['id']] = $p;
        }
    }
}
$tax = cart_get_tax($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
ob_start();
cart_display($db, array(
    'cart' => $_SESSION['cart'],
    'discounts' => $_SESSION['discounts'],
    'customer' => $_SESSION['customer'],
    'show_controls' => true,
    'show_tax' => $tax > 0 ? true : false,
));
$cart = ob_get_clean();

$content = $twigpanel->render('cart.twig', array(
    'c' => $_SESSION['cart'],
    'carthtml' => $cart,
    'xval' => session_id(),
    'related' => $related,
    'config' => $config
));


$page['title'] = 'Shopping Cart';
$page['page_vars']['content'] = $content;
