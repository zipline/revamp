<?
include '../common/config.php';
include 'common/mod_upload_w.php';

// this page performs all actions available in the cms and redirects the brower to the appropriate url

$deletable = array('pages', 'permissions', 'admins', 'products', 'product_categories', 'users', 'galleries', 'photos',
    'news', 'blogs', 'options', 'option_categories', 'blogs', 'blogs_entries', 'blog_tags', 'bkg_image','discounts');

// broad security check
if (!$_SESSION['admin']) {
    exit('Permission denied');
}

switch ($_GET['a']) {

    case 'logout' :
        unset($_SESSION['admin']);
        hle("./");
        break;

    case 'permissions-save' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'permissions';
        $id = sql_upsert($db, $table_name, array('id_admins', 'id_items', 'item_type'), $_POST, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("admins_permissions_.php?id=$id");
        break;

    case 'flag-switch' :
        $whitelist = array(
            array('pages', 'navhide'),
            array('products', 'display'),
            array('options', 'display'),
            array('shipping_locations', 'display'),
            array('gallery_tags', 'display'),
            array('blogs_entries', 'hidden'),
        );
        $table = $_GET['t'];
        $flag = $_GET['f'];
        if (in_array(array($table, $flag), $whitelist)) {
            $query = $db->prepare("UPDATE `$table` SET `$flag` = NOT `$flag` WHERE id = ?");
            $query->execute(array($_GET['id']));
            m(ucwords($table) . ' updated');
        } else {
            e("Bad flag specification");
        }
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'admins-save' :
        $fields_to_update = array('username', 'notes');
        $values = $_POST;
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        if (!$_POST['username']) {
            e("Username shouldn't be blank");
        }
        if ($_POST['password']) {
            $fields_to_update[] = 'hashed_password';
            $hasher = new Hautelook\Phpass\PasswordHash(8, false);
            $values['hashed_password'] = $hasher->HashPassword($_POST['password']);
        }
        $table_name = 'admins';
        $id = sql_upsert($db, $table_name, $fields_to_update, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'drafts-delete' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_GET['id'])) {
            exit('Permission denied');
        }
        $id_pages = $_REQUEST['id'];
        $query = $db->prepare("DELETE FROM pages_vars WHERE id_pages = :id_pages AND version != ''");
        $query->execute(array(':id_pages' => $id_pages));
        m("Draft deleted");
        hle('pages_.php?id=' . $id_pages);
        break;

    case 'delete' :
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        $table = $_GET['t'];
        if (!in_array($table, $deletable)) {
            exit('Bad delete specification');
        }
        // permission check
        switch ($table) {
            case 'options':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                $query = $db->prepare("DELETE FROM product_options WHERE oid = ?");
                $query->execute(array($_GET['id']));
                break;

            case 'products':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                $query = $db->prepare("DELETE FROM product_options WHERE pid = ?");
                $query->execute(array($_GET['id']));
                $query = $db->prepare("DELETE FROM products_related WHERE pid = ? OR  pid_target = ?");
                $query->execute(array($_GET['id'],$_GET['id']));
                break;
            case 'bkg_image':
                $bkgdir = '../upload/background';
                $bkg_image = sql_fetch_by_key($db, 'bkg_image', "id", $_GET['id']);
                unlink($bkgdir.'/thumb/'.$bkg_image['image']);
                unlink($bkgdir.'/orig/'.$bkg_image['image']);
                unlink($bkgdir.'/2400/'.$bkg_image['image']);
                break;

            default:
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                break;
        }
        try {
            $query = $db->prepare("DELETE FROM `$table` WHERE id = ?");
            $query->execute(array($_GET['id']));
            m('Item deleted');
        } catch (Exception $e) {
            e("Couldn't delete item - make sure it doesn't have any dependencies.");
        }
        hle($_GET['return_url']);
        break;

    case 'copy' :
        $table = $_GET['t'];
        if (!in_array($table, $deletable)) {
            exit('Bad copy specification');
        }
        // permission check
        switch ($table) {
            case 'products':
                error_reporting(E_ALL);
                ini_set("display_errors", 1);
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }

                try {
                    $id = db_row_copy($db, $table, $_GET['id'], 'id', array('gallery_id' => ''));

                    $query = $db->prepare("SELECT * FROM product_options WHERE pid = ?");
                    $query->execute(array($_GET['id']));
                    $oo = $query->fetchAll();
                    foreach ($oo as $o) {
                        db_row_copy($db, "product_options", $o['id'], 'id', array('pid' => $id));
                    }
                    m("Copy successful");

                } catch (Exception $e) {
                    e("Error occurred: " . $e->getMessage());
                }
                break;

            default:
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                try {
                    db_row_copy($db, $table, $_GET['id']);
                    m("Copy successful");
                } catch (Exception $e) {
                    e("Error occurred: " . $e->getMessage());
                }
                break;
        }
        hle($_GET['return_url']);
        break;
    case 'account-password':
        if (strlen($_POST['new_password']) < 6) {
            e("New password must be at least 6 characters long.");
        }
        if (!$_SESSION['e']) {
            try {
                $hasher = new Hautelook\Phpass\PasswordHash(8, false);
                $pw = $hasher->HashPassword($_POST['new_password']);
                $query = $db->prepare("UPDATE admins SET hashed_password = :pw WHERE id = :admin_id");
                $query->execute(array(
                    ':pw' => $pw,
                    ':admin_id' => $_SESSION['admin']['id']
                ));
                m('Password updated');
            } catch (Exception $e) {
                e("Database error while updating password.");
            }
        }
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'blogs-save' :
        $table_name = 'blogs';
        $checkbox_fields = array();
        $fields = array('title', 'keyword', 'index_entries', 'description');

        // validation
        if (!$_POST['keyword']) {
            e("Location is required - the location keyword is used for the blog's address");
        }
        $values = sql_organize_values($fields, $_POST, $checkbox_fields);

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'blogs_entries-save' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'blogs.' . $_POST['id_blogs'])) {
            exit('Permission denied');
        }

        $table_name = 'blogs_entries';
        $basedir_blog_images = '../upload/blogs';
        $checkbox_fields = array('hidden');
        $fields = array('content', 'id_blogs', 'title', 'keyword', 'summary', 'date_posted', 'date_modified','hidden');
        $_POST['date_modified'] = date('Y-m-d H:i:s');
        $raw_values = $_POST;

        // resize image if provided
        if ($_FILES['image']['name']) {
            list($image) = relocateImage($_FILES['image']['tmp_name'], array(
                array('width' => 250, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/thumb"),
                array('width' => 400, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/400"),
                array('width' => 700, 'height' => 500, 'function' => 'resizeprop', 'name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/600"),
                array('name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/orig"),
            ));
            $fields[] = 'image';
            $raw_values['image'] = $image;
        } elseif ($_POST['image_clear']) {
            $fields[] = 'image';
            $raw_values['image'] = '';
        }

        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id'], $insert_only);


        //edit product options
        $query = $db->prepare("DELETE FROM blog_entry_tags WHERE bid = ?");
        $query->execute(array($id));

        foreach ($_POST['option'] as $option) {
            sql_upsert($db, "blog_entry_tags", array('tid', 'bid'), array('tid' => $option, 'bid' => $id), 'id', '');
        }


        m("Saved at " . $config['date']);
        hle("$table_name" . "_.php?id_blogs=" . $_POST['id_blogs'] . "&id=$id");
        break;

    case 'blog_tags-save' :
        $table_name = 'blog_tags';
        $fields = array('title');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("blog_tags_.php?id=$id");
        break;

    case 'pages-copy' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_GET['id'])) {
            exit('Permission denied');
        }
        $page = sql_fetch_by_key($db, 'pages', 'id', $_GET['id']);
        $db->beginTransaction();
        try {
            $new_page_id = cms_deep_page_copy($db, $page['id'], $page['id_parent']);
            $db->commit();
            m("Copy successful");
            $_SESSION['selectors_to_highlight'][] = "#pages-$new_page_id>div";
        } catch (Exception $e) {
            $db->rollback();
            e("Error occurred: " . $e->getMessage());
        }
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'pages-save' :
        if ($_POST['id'] and !admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['id'])) {
            exit('Permission denied');
        }
        $id_pages = $_POST['id'];
        $page = get_page($db, array('id' => $_POST['id']));
        $template = $page['template'];
        $template_save_path = "../templates/$template" . "_save.php";
        if (!$page) {
            exit('Page with ID ' . $_POST['id'] . ' not found for saving');
        }
        if (!$config['templates'][$template]) {
            exit('Template "' . $template . '" not found');
        }
        if (!is_file($template_save_path)) {
            include "../templates/page_save.php";
        }
        $return_url = "pages_.php?id={$_POST['id']}";
        include $template_save_path;
        m("Saved at " . $config['date']);
        hle($return_url);
        break;

    case 'pages-config' :
        if ($_POST['id'] and !admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['id'])) {
            exit('Permission denied');
        }
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['id_parent'])) {
            exit('Permission denied - can\'t move a page to be a child of a page you don\'t have permission to edit');
        }
        $table_name = 'pages';
        $checkbox_fields = array('navhide', 'hide_h1', 'hide_in_breadcrumbs', 'hide_breadcrumbs', 'show_subnav', 'comments_disabled', 'footershow');
        $fields = array_merge($checkbox_fields, array('image', 'id_parent', 'keyword', 'title', 'type', 'frontend_include', 'link', 'template', 'meta_description'));

        // validation
        if (!$_POST['keyword']) {
            e("Location is required - the location keyword is used for the page's address");
        }
        if (!$_POST['template']) {
            e("Pages can't be displayed without a template");
        }
        // check for duplicate keyword
        $query = $db->prepare("SELECT * FROM pages WHERE keyword = ? AND id != ?");
        $query->execute(array($_POST['keyword'], $_POST['id']));
        $page_with_same_keyword = $query->fetch();
        if ($page_with_same_keyword) {
            e("Another page uses the same location: <a href=\"pages_.php?id={$page_with_same_keyword['id']}\">{$page_with_same_keyword['title']}</a>");
        }

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("pages_config_.php?id=$id");
        break;

    case 'settings-save' :
        if ($_SESSION['test']) {
            $sql = "INSERT INTO settings SET
					name = :name,
					title = :title,
					description = :description,
					value = :value,
					date_updated = NOW(),
					id = :id
				ON DUPLICATE KEY UPDATE
					name = :name,
					title = :title,
					description = :description,
					value = :value,
					date_updated = NOW()";
            $params = array(
                ':name' => $_POST['name'],
                ':title' => $_POST['title'],
                ':description' => $_POST['description'],
                ':value' => trim($_POST['value']),
                ':id' => $_POST['id'],
            );
        } else {
            $sql = "UPDATE settings SET
					value = :value,
					date_updated = NOW()
				WHERE id = :id";
            $params = array(
                ':value' => trim($_POST['value']),
                ':id' => $_POST['id'],
            );
        }
        $query = $db->prepare($sql);
        $query->execute($params);
        m("Saved at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'prioritize' :
        // need to have permission to edit the root element of the tree which they're editing
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['root_element_id'])) {
            exit('Permission denied');
        }
        $allowed_tables = array('products', 'product_categories', 'pages', 'photos', 'options', 'option_categories');

        $table_name = $_POST['table'];
        $hierarchy_mode = $_POST['mode'];

        if (!in_array($table_name, $allowed_tables)) {
            exit('Not allowed to sort that table.');
        }
        if (!in_array($hierarchy_mode, array('nested', 'flat'))) {
            exit('Heirarchy mode not allowed');
        }

        $positions_arr = $_POST['priorityitems'];

        if ($hierarchy_mode == 'nested') {
            $query = $db->prepare("UPDATE `$table_name` SET priority = :priority, id_parent = :id_parent WHERE id = :id");
        } else {
            $query = $db->prepare("UPDATE `$table_name` SET priority = :priority                         WHERE id = :id");
        }
        $i = 0;

        foreach ($positions_arr as $id => $id_parent) {
            $i++;
            if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $id)) {
                exit('Permission denied: trying to move #' . $id);
            }
            $params = array(
                ':priority' => $i,
                ':id' => $id,
            );

            if ($hierarchy_mode == 'nested') {
                if ($id_parent == 'root') {
                    $id_parent = $_POST['root_element_id'];
                }
                // need to have permission to edit both this element's newly-specified parent to add it as a sub-item
                if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $id_parent)) {
                    exit('Permission denied: trying to move an item under #' . $id_parent);
                }
                $params[':id_parent'] = $id_parent;
            }

            $query->execute($params);
            print_r($db->errorInfo(), true);
        }
        m("Positions saved.");
        break;

    case 'photos-save' :
        $table_name = 'photos';
        $basedir = "../upload/photos";
        $fields = array('id_parent', 'title', 'content', 'image','alt');
        $values = $_POST;

        // resize image if provided
        if ($_FILES['file']['name']) {
            list($image) = relocateImage($_FILES['file']['tmp_name'], array(
                array('width' => 100, 'height' => 100, 'function' => 'resizethumb', 'name' => $_FILES['file']['name'], 'dir' => "$basedir/thumb"),
                array('width' => 400, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/400"),
                array('width' => 800, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/800"),
                array('width' => 1600, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/1600"),
                array('name' => $_FILES['file']['name'], 'dir' => "$basedir/orig"),
            ));
            $values['image'] = $image;
        } elseif ($_POST['image_clear']) {
            $values['image'] = $image;
        } else {
            unset($fields['image']);
        }
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);



        //edit product options
        $query = $db->prepare("DELETE FROM photo_tags WHERE pid = ?");
        $query->execute(array($id));

        foreach ($_POST['tag'] as $tag) {
            sql_upsert($db, "photo_tags", array('tid', 'pid'), array('tid' => $tag, 'pid' => $id), 'id', '');
        }

        $urladdon = '';
        if ($_GET['pid']) {
            $urladdon = 'pid=' . $_GET['pid'] . '&';
        }

        #if submitted through form add save message and redirect, otherwise dropzone was used.
        if ($_POST['submit']) {
            m("Saved at " . $config['date']);
            if ($_POST['submit'] == 'Save and add another') {
                hle("$table_name" . "_.php?{$urladdon}id_parent={$_POST['id_parent']}");
            } else {
                hle("$table_name" . "_.php?{$urladdon}id={$id}");
            }
        }
        break;

    case 'galleries-set-main':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $photo = sql_fetch_by_key($db, 'photos', 'id', $_GET['id']);
        if (!$photo) {
            exit('Photo not found.  ID provided: ' . $_GET['id']);
        }
        try {
            $query = $db->prepare("UPDATE photos SET featured = 0 WHERE id_parent = :id_parent AND type = :type");
            $query->execute(array(
                ':id_parent' => $photo['id_parent'],
                ':type' => $photo['type']
            ));
            $query = $db->prepare("UPDATE photos SET featured = 1 WHERE id = :id");
            $query->execute(array(
                ':id' => $_GET['id']
            ));
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        echo "Main photo was updated.";
        break;

    case 'galleries-save':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'galleries';
        $id = sql_upsert($db, $table_name, array('title','description'), $_POST, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'product_categories-save' :
        $table_name = 'product_categories';
        $checkbox_fields = array('svg_cat');
        $fields = array('title', 'description', 'id_parent','svg_cat');

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("product_categories_.php?id=$id");
        break;

    case 'products-save' :
        $product = sql_fetch_by_key($db, 'products', 'id', $_POST['id']);
        $table_name = 'products';
        $checkbox_fields = array('taxable', 'arbitrary_price_allowed');
        $fields = array('title', 'partnumber', 'price', 'xsize', 'ysize', 'zsize', 'weight', 'description', 'id_categories', 'taxable', 'arbitrary_price_allowed');
        //strip non numeric characters from number values
        $_POST['price'] = preg_replace("/[^0-9.]/", '', $_POST['price']);
        $_POST['xsize'] = preg_replace("/[^0-9.]/", '', $_POST['xsize']);
        $_POST['ysize'] = preg_replace("/[^0-9.]/", '', $_POST['ysize']);
        $_POST['zsize'] = preg_replace("/[^0-9.]/", '', $_POST['zsize']);
        $_POST['weight'] = preg_replace("/[^0-9.]/", '', $_POST['weight']);

        $raw_values = $_POST;

        // resize image if provided
        if ($_FILES['image']['name']) {
            list($image) = relocateImage($_FILES['image']['tmp_name'], array(
                array('width' => 200, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_product_images_small",),
                array('width' => 600, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_product_images_large",),
            ));
            $fields[] = 'image';
            $raw_values['image'] = $image;
        } elseif ($_POST['image_clear']) {
            $fields[] = 'image';
            $raw_values['image'] = '';
        }

        // save product record
        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        $_POST['projectid'] = $id;

        //create or edit gallery title
        $_POST['gallery_id'] = sql_upsert($db, 'galleries', array('title', 'projectid'), $_POST, 'id', $_POST['gallery_id']);

        //add gallery_id to product
        $id = sql_upsert($db, $table_name, array('gallery_id'), array('gallery_id' => $_POST['gallery_id']), 'id', $id);

        //edit product options
        $query = $db->prepare("DELETE FROM product_options WHERE pid = ?");
        $query->execute(array($id));

        foreach ($_POST['option'] as $option) {
            sql_upsert($db, "product_options", array('oid', 'pid'), array('oid' => $option, 'pid' => $id), 'id', '');
        }
        //edit related product
        $query = $db->prepare("DELETE FROM products_related WHERE pid = ?");
        $query->execute(array($id));

        foreach ($_POST['related'] as $option) {
            sql_upsert($db, "products_related", array('pid', 'pid_target'), array('pid' => $id, 'pid_target' => $option), 'id', '');
        }

        if ($id) {
            m("Saved at " . $config['date']);
        } else {
            e("Problem saving product");
        }
        hle("products_.php?id=$id");
        break;

    case 'product_single-save' :
        $product = sql_fetch_by_key($db, 'products', 'id', $_POST['id']);
        $table_name = 'products';
        $checkbox_fields = array('taxable');
        $fields = array('title', 'partnumber', 'price', 'xsize', 'ysize', 'zsize', 'weight', 'description', 'id_categories', 'taxable');
        //strip non numeric characters from number values
        $_POST['price'] = preg_replace("/[^0-9.]/", '', $_POST['price']);
        $_POST['xsize'] = preg_replace("/[^0-9.]/", '', $_POST['xsize']);
        $_POST['ysize'] = preg_replace("/[^0-9.]/", '', $_POST['ysize']);
        $_POST['zsize'] = preg_replace("/[^0-9.]/", '', $_POST['zsize']);
        $_POST['weight'] = preg_replace("/[^0-9.]/", '', $_POST['weight']);

        $raw_values = $_POST;

        // save product record
        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        if ($id) {
            m("Saved at " . $config['date']);
        } else {
            e("Problem saving product");
        }
        hle("product_single_.php?id=$id");
        break;
    case 'option_categories-save' :
        $table_name = 'option_categories';
        $checkbox_fields = array('svg_cat','prepend_partnum');
        $fields = array('title','option_id','description','svg_cat','prepend_partnum');

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("option_categories_.php?id=$id");
        break;

    case 'options-save' :
        $options = sql_fetch_by_key($db, 'options', 'id', $_POST['id']);
        $table_name = 'options';
        $checkbox_fields = array();
        $fields = array('title', 'partnumber', 'price','price_fixed', 'weight', 'id_categories', 'color_option', 'content', 'keyword');
        $_POST['keyword'] = toAscii($_POST['title']);
        $raw_values = $_POST;

        // save product record
        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        //edit product options
        $query = $db->prepare("DELETE FROM option_hide WHERE oid = ?");
        $query->execute(array($id));
        foreach ($_POST['option'] as $option) {
            sql_upsert($db, "option_hide", array('hide_oid', 'oid'), array('hide_oid' => $option, 'oid' => $id), 'id', '');
        }

        //create pattern image directory
        $pattern_dir = "../upload/patterns/" . $id;
        if (!file_exists($pattern_dir)) {
            mkdir($pattern_dir, 0777, true);
        }

        if ($id) {
            m("Saved at " . $config['date']);
        } else {
            e("Problem saving product");
        }
        hle("options_.php?id=$id");
        break;

    case 'option-photos-save' :
        $basedir = '../upload/patterns/' . $_POST['oid'] . '/';
        // These images must be sized before upload so no processing
        if ($_FILES['file']['name']) {
            list($image) = relocateImage($_FILES['file']['tmp_name'], array(
                array('name' => $_FILES['file']['name'], 'dir' => $basedir)
            ));
        }
        break;

    case 'orders-save' :
        $table_name = 'orders';
        $fields = array('notes',);
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'get_permission_item_selector':
        $permission_item_type = sql_fetch_by_key($db, 'permissions_item_types', "keyword", $_GET['item_type']);
        $table = $permission_item_type['table'];
        if (!$table) {
            exit();
        } else {
            echo $permission_item_type['title'] . "<br/>";
            if ($permission_item_type['tree']) {
                $rows = cms_get_sub_items_list($db, $table, 0);
                foreach ($rows as $key => $row) {
                    $rows[$key]['title'] = str_repeat("&nbsp;", 3 * $row['depth']) . $row['title'];
                }
            } else {
                $rows = $db->query("SELECT * FROM `$table` ORDER BY title ASC");
            }

            write_select(array(
                'db' => $db,
                'rows' => $rows,
                'label' => 'title',
                'value' => 'id',
                'current' => $_GET['current'],
                'name' => 'id_items',
            ));
        }
        break;

    case 'util_page_keyword_find_replace':
        sql_modify_recursively($db, 'pages', 'id', 'id_parent', $_REQUEST['id_parent'], function (PDO $db, $row) {
            $new_keyword = str_replace($_REQUEST['find'], $_REQUEST['replace'], $row['keyword']);
            $query = $db->prepare("UPDATE pages SET keyword = ? WHERE id = ?");
            $query->execute(array($new_keyword, $row['id']));
        });
        m("Update complete");
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'news-save':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'news';
        $fields = array('title', 'description', 'content', 'date_posted', 'homepage');
        $checkbox_fields = array('homepage');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'gallery_tag_cats-save':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'gallery_tag_cats';
        $_POST['safename'] = safename($_POST['title']);
        $fields = array('title', 'safename');

        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . ".php?id=$id");
        break;
    case 'gallery_tags-save':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'gallery_tags';
        $_POST['safename'] = safename($_POST['title']);
        $fields = array('cat','title', 'safename');

        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'discounts-save':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'discounts';
        $fields = array('title', 'code', 'percent', 'minimum', 'dollaroff','expires');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'shipping_locations-save':
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'shipping_locations';
        $fields = array('price');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'bkg_image-save' :
        $table_name = 'bkg_image';
        $basedir = "../upload/background";
        $fields = array('image');
        $values = $_POST;

        // resize image if provided
        if ($_FILES['file']['name']) {
            list($image) = relocateImage($_FILES['file']['tmp_name'], array(
                array('width' => 100, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/thumb"),
                array('width' => 500, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/500"),
                array('width' => 2400, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/2400"),
                array('name' => $_FILES['file']['name'], 'dir' => "$basedir/orig"),
            ));
            $values['image'] = $image;
        }

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        break;

    case 'orders-resend-receipt' :
        if( ! $_POST['email'] ){
            e('Email is required to resend receipt') ;
            hle("orders_.php?id={$_POST['id_orders']}") ;
        }
        $order = sql_fetch_by_key($db, 'orders', "id", $_POST['id_orders']);
        if( ! $order ){
            e('Order not found') ;
            hle("orders_.php?id={$_POST['id_orders']}") ;
        }
        smtp_mail("\"{$config['company_name']}\"<{$config['receipt_contact']}>", $_POST['email'], "Order #$ordernumber", $order['receipt']);

        m("Receipt was sent to {$_POST['email']}") ;
        hle("orders_.php?id={$_POST['id_orders']}") ;
        break ;

    case 'orders-send-tracking-info' :
        // get some data about carrier & order
        $carrier = sql_fetch_by_key($db, 'shipping_carriers', "id", $_POST['id_shipping_carriers']);
        $order = sql_fetch_by_key($db, 'orders', "id", $_POST['id_orders']);
        $date_shipped = date_compose('date_shipped', $_POST) ;

        // prepare email
        $tracking_link = str_replace('%s', $_POST['tracking_number'], $carrier['link_format']) ;
        $email_to      = $_POST['email'] ;
        $email_subject = "Order #{$order['ordernumber']} Shipped" ;

        $email_message = "<p>Good news: all or part of your order was shipped on ".date('F j', strtotime($date_shipped)).".  Use this link to track your order: <a href=\"$tracking_link\">$tracking_link</a>.</p>" ;
        if( $_POST['notes'] ){
            $email_message .= "<p><b>Notes</b></p><p>".nl2br(htmlspecialchars(stripslashes($_POST['notes'])))."</p>" ;
        }
        $email_message .= "<p>For your reference, your original receipt is copied below.</p><div>{$order['receipt']}</div>" ;
        $email_headers = $config['smtp_conf'] ;

        // send email
        smtp_mail("\"{$config['company_name']}\"<{$config['receipt_contact']}>", $_POST['email_to'], $email_subject, $email_message);

        // save db record
        $fields = array('id_orders', 'id_shipping_carriers', 'carrier_title', 'email_to', 'notes', 'tracking_number',
            'tracking_link', 'date_shipped', 'email_content', 'date_email_sent');
        $_POST['carrier_title'] = $carrier['title'];
        $_POST['tracking_link'] = $tracking_link;
        $_POST['date_shipped'] = $date_shipped;
        $_POST['email_content'] = $email_message;
        $_POST['date_email_sent'] = date('Y-m-d H:i:s');

        $values = sql_organize_values($fields, $_POST);

        $id = sql_upsert($db, 'order_shipments', $fields, $values, 'id', $_POST['id']);

        // wrap up
        m("Shipping info saved and sent at $g_date") ;
        hle("orders_.php?id={$_POST['id_orders']}") ;
        break ;
}
