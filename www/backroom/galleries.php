<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}
$table_name = 'galleries';

$query = $db->prepare("SELECT g.id, g.title, COUNT( p.id ) AS num_photos,
		(SELECT featured FROM photos AS featured_photos WHERE featured AND id_parent = g.id LIMIT 1) AS has_featured_photo
	FROM galleries g
		LEFT OUTER JOIN photos AS p ON p.id_parent = g.id
			AND p.type =  'galleries'
	GROUP BY g.id
	ORDER BY g.title ASC, p.featured DESC
	");
$query->execute();
$rr = $query->fetchAll();

include 'common/header.php';

?>

    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('add', 'galleries_.php', 'Add Gallery'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Galleries</h1>
            <?

            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <ol class="treeview list root" cmsTable="options" cmsHierarchyMode="flat">
                    <?
                    foreach ($rr as $r) {
                        $class = ++$i & 1 ? 'odd' : 'even';
                        ?>
                        <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                            <div class="row">
                                <div class="buttons">
                                    <div class="cell"><a class="btn btn-xs btn-default"
                                                         href="photos.php?id=<?= $r['id'] ?>">Manage <?= $r['num_photos'] ?: '' ?>
                                            photos</a></div>
                                    <div class="cell"><? duplicate_button('options', $r['id']); ?></div>
                                    <div class="cell"><? delete_button($table_name, $r['id']) ?></div>
                                </div>
                                <a class="cell edit-link" href="<?= $table_name . "_.php?id={$r['id']}" ?>">
                                    <?= htmlspecialchars($r['title']) ?>
                                </a>
                            </div>
                        </li>
                    <?
                    }
                    ?>
                </ol>
            <?
            }
            ?>
        </div>
    </div>
<?
include 'common/footer.php';
