<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$query = $db->prepare("SELECT * FROM pages WHERE id_parent = ? AND (template='product_use' || template='product_list' || template='include_php') ORDER BY priority ASC");
$query->execute(array($page['id']));
$pp = $query->fetchall();
$subpages = array();
foreach ($pp as $p) {
    $vars = get_page_vars($db, $p['id']);
    $p['page_vars'] = $vars;
    if($p['template']=='product_use') {
        if ($p['page_vars']['gallery']) {
            $query = $db->prepare("SELECT * FROM photos WHERE id_parent = ? ORDER BY priority LIMIT 1");
            $query->execute(array($p['page_vars']['gallery']));
            $image = $query->fetch();
            $p['image'] = $image['image'];
        }
    }elseif($p['template']=='product_list'){
        $catid = $p['page_vars']['id_categories'];
        $query = $db->prepare("SELECT ph.image FROM photos ph, galleries g, products p
            WHERE p.id_categories = ? AND p.id = g.projectid AND g.id = ph.id_parent ORDER BY p.priority, p.id ASC, ph.priority, ph.id ASC LIMIT 1");
        $query->execute(array($catid));
        $image = $query->fetch();
        $p['image'] = $image['image'];

        /*
         * $catid = products.id_categories
         * products.id = gallery.projectid
         * gallery.id = photos.id_parent
         *
         */

    }elseif($p['page_vars']['file_path']=='product_use.php'){
        $query = $db->prepare("SELECT * FROM pages WHERE id_parent = ? AND (template='product_use' || template='product_list' || template='include_php') ORDER BY priority ASC");
        $query->execute(array($p['id']));
        $subpp = $query->fetchall();
        foreach ($subpp as $sp) {
            $subvars = get_page_vars($db, $sp['id']);
            if($subvars['gallery']) {
                $query = $db->prepare("SELECT * FROM photos WHERE id_parent = ? ORDER BY priority LIMIT 1");
                $query->execute(array($subvars['gallery']));
                $image = $query->fetch();
                $p['image'] = $image['image'];
                break;
            }
        }
    }
    if($p['template']=='include_php'){
        if($p['page_vars']['file_path']=='product_use.php'){
            $subpages[] = $p;
        }
    }else {
        $subpages[] = $p;
    }
}

$content = $twigpanel->render('product_use.twig', array(
    'subpages' => $subpages
));


$page['page_vars']['content'] .= $content;

