<?
include 'common/config.php';
$settings['breadcrumb_separator'] = $config['breadcrumb_separator'];

if ($_GET['view'] and false === strpos($_GET['view'], '/')) {
	$page = [];
	$page['template'] = $_GET['view'];
	$page['meta_title'] = $page['title'] . ($settings['html_title_snippet'] ? " {$settings['html_title_snippet']}" : '') ;
	$page = cms_render_page($db, $page, $settings, $config, [
		'user'=>$_SESSION['user'],
		'form'=>form_load_clear($_GET['view']),
		'get'=>$_GET,
	]);
	if (!$page['breadcrumbs'] and is_array($page['breadcrumbs_array'])) {
		$page['breadcrumbs'] = get_breadcrumbs($page['breadcrumbs_array'], $settings['breadcrumb_separator'], true, false);
	}
}

$page['id_nav_root_page'] = 0;
$page['nav_depth'] = 3;
$page['meta_title'] = $page['title'] . ($settings['html_title_snippet'] ? " {$settings['html_title_snippet']}" : '') ;
$page['nav'] = cms_get_nav($db, $page['id_nav_root_page'], $page['nav_chain'], $page['nav_depth'], true, 'current');
$page['canonical_url'] = "http://".$config['ssl_address'].$_SERVER['REQUEST_URI'];
$page['requested_url'] = "http".($_SERVER['HTTPS'] ? 's' : '')."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

$basedir_photos = 'upload/photos';
$page['page_vars']['content'] = preg_replace_callback(
    '@\[gallery-(\d+)\]@',
    function ($matches) use ($db, $basedir_photos, $settings) {
        return '<div class="gallery">' . gallery_display(
            $db,
            $basedir_photos,
            $matches[1]
        ) . '</div>';
    },
    $page['page_vars']['content']
);
if(!$_SESSION['bkg_image'] || $page['id']==1){
    $query = $db->prepare("SELECT * FROM bkg_image ORDER BY RAND()");
    $query->execute();
    $bkg = $query->fetchall();
    $_SESSION['bkg_image'] = $bkg[0]['image'];
}
$page['bkg_image'] = $_SESSION['bkg_image'];


$query = $db->prepare("SELECT * FROM pages WHERE footershow ORDER BY priority ASC");
$query->execute();
$fnav = $query->fetchall();

// display the page
echo $twig->render($page['template'].'.html.twig', array(
	'nav_options' => $nav_options,
	'nav' => $page['nav'],
    'fnav' => $fnav,
	'page' => $page,
    'cart' => $_SESSION['cart'],
	'messages' => $_SESSION['m'],
	'errors' => $_SESSION['e'],
	'user' => $_SESSION['user'],
	'test_mode' => $_SESSION['test'],
    'config' => $config,
	'search_term' => $_GET['q']
));

// empty the errors and messages which should have been shown to the user
unset($_SESSION['e']);
unset($_SESSION['m']);
