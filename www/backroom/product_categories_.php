<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'product_categories';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if (!$r) {
    $r['enabled'] = 1;
    $r['id_parent'] = $_GET['id_parent'];
}

include 'common/header.php';
?>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Product Category</h1>

                <div class="formfield">
                    <b>Title</b><br/>
                    <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"/>
                </div>
                <div class="formfield">
                    <label><b>Allow size based svg images</b> <span class="note">(IE Panels/Wall Art)</span>
                    <input type="checkbox"
                                  name="svg_cat" <?= $r['svg_cat'] ? 'checked="checked"' : '' ?>/></label><br/>
                </div>

                <div class="formfield" style="display: none;">
                    <b>Description</b><br/>
                    <textarea name="description"
                              style="width: 600px ; height: 400px ;"><?= $r['description'] ?></textarea>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';
