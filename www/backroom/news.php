<?
include 'common/config.php' ;

$rr = $db->query("SELECT * FROM news ORDER BY date_posted DESC");

if( ! admin_perm_check($db, $_SESSION['admin'], 'news', $news['id']) ){
	exit('Permission denied') ;
}

include 'common/header.php' ;
?>
<div class="controls"><? button('add', 'news_.php', 'Add Entry') ; ?></div>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
	<table class="list">
		<tr>
			<th>Posted</th>
			<th>Title</th>
			<?
			if ($config['news_can_display_on_homepage']) {
				?>
				<th>Homepage?</th>
				<?
			}
			?>
		</tr>
		<?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			if( $r['hidden'] ){
				$class .= ' dead' ;
			}
			?><tr class="<?= $class ?>">
				<td><?= htmlspecialchars(date('F j, Y', strtotime($r['date_posted']))) ?></td>
				<td><?= $r['hidden']? '[Hidden] ': '' ?><?= htmlspecialchars($r['title']) ?></td>
				<?
				if ($config['news_can_display_on_homepage']) {
					?>
					<td style="text-align: center;"><?= $r['homepage'] ? '&#10003;' : '' ?></td>
					<?
				}
				?>
				<td><? edit_button("news_.php?id={$r['id']}") ; ?></td>
				<td><? delete_button('news',$r['id']) ; ?></td>
			</tr><?
		}
	?></table><?
}

include 'common/footer.php' ;

