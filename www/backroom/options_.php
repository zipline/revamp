<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'options';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[name="id_categories"]').change(function () {
                if($(this).val()==2 || $(this).val()==3 || $(this).val()==4){
                    $(".color-opt").show();
                }else{
                    $(".color-opt").hide();
                }
                console.log($('option:selected',this).attr('svg'));
                if($('option:selected',this).attr('svg')=='true'){
                    $(".pattern-opt").show();
                }else{
                    $(".pattern-opt").hide();
                }
            });
        });
    </script>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "option_categories.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Options</h1>
                <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Partnumber</b><br/>
                            <input type="text" name="partnumber" value="<?= htmlspecialchars($r['partnumber']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Category</b><br/><?
                            $rows = $db->query("SELECT * FROM option_categories ORDER BY priority ASC")
                            ?>
                            <select name="id_categories">
                                <option value=""></option>
                                <?
                                $show_svgs = false;
                                foreach( $rows as $row ){
                                    if($row['svg_cat'] && $row['id']==$r['id_categories']){
                                        $show_svgs = true;
                                    }
                                    ?>
                                    <option <?=$row['svg_cat']?'svg="true"':'' ?> value="<?= $row['id'] ?>" <?= $row['id']==$r['id_categories'] ? 'selected="selected"':'' ?>>
                                    <?= $row['title'] ?>
                                    </option>
                                <?
                                }
                                ?>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Price Multiplier</b><br><span class="note">Price calculated by "(base price x multiplier) x other option multipliers"</span><br/>
                            $<input type="text" name="price" value="<?= (float)htmlspecialchars($r['price']) ?>"
                            style="width: 50%;"/>
                        </div>
                        <div class="formfield">
                            <b>Fixed Additional Price</b><br><span class="note">This number will be added to the product after the multiplier is added.</span><br/>
                            $<input type="text" name="price_fixed" value="<?= (float)htmlspecialchars($r['price_fixed']) ?>"
                                    style="width: 50%;"/>
                        </div>

                        <div class="formfield">
                            <b>Weight(lbs) Multiplier</b><br><span class="note">Weight calculated by "(base weight x multiplier) x other option multipliers"</span><br/>
                            <input type="text" name="weight" value="<?= (float)htmlspecialchars($r['weight']) ?>"
                                   style="width: 50%;"/> lb
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="formfield color-opt" <? if($r['id_categories'] != 2 && $r['id_categories'] != 3 && $r['id_categories'] != 4){ echo 'style="display:none;"'; } ?> >
                            <b>Color Value</b><br/>
                            <input type="text" name="color_option" value="<?= htmlspecialchars($r['color_option']) ?>"/>
                        </div>
                        <? if($r['id']){ ?>
                        <div class="formfield pattern-opt" <? if(!$show_svgs){ echo 'style="display:none;"'; } ?> >
                            <b>Pattern SVG Images
                                <span class="note">
                                <?
                                if ($r['id']) {
                                    $basepath = '../upload/patterns/' . $r['id'] . '/';
                                    $folder = $basepath;
                                    $filetype = '*.*';
                                    $files = glob($folder . $filetype);
                                    $count = count($files);
                                    echo 'There are ' . $count . ' current images';
                                }
                                ?></span>
                            </b><br/>
                            <a href="options_gallery.php?oid=<?=$r['id'] ?>" class="btn btn-primary" id="option-gallery">Manage Pattern Images</a>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $('#option-gallery').magnificPopup({type:'iframe'});
                                });
                            </script>
                        </div>
                        <? } ?>
                    </div>
                </div>
                <div class="formfield pattern-opt" <? if($r['id_categories'] != 1 ){ echo 'style="display:none;"'; } ?> >
                    <b>Pattern Description</b><br/>
                    <textarea name="content" class="rich_editor" style="width: 100% ; height: 300px ;"><?= $r['content'] ?></textarea>
                </div>

                <div class="row">
                <div class="col-md-12">
                    <h2>Hide the Following when selected</h2>
                    <?
                    $cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
                    foreach ($cc as $c) {

                        $query = $db->prepare("SELECT * FROM options WHERE id_categories = ? ORDER BY priority DESC");
                        $query->execute(array($c['id']));
                        $oo = $query->fetchAll();
                        if (count($oo)) {
                            ?>
                            <h3><?= $c['title'] ?></h3>
                            <?
                            foreach ($oo as $o) {
                                $query = $db->prepare("SELECT * FROM  option_hide WHERE hide_oid = ? AND oid = ?");
                                $query->execute(array($o['id'], $r['id']));
                                $po = $query->fetch();
                                ?>
                                <div class="col-md-3">
                                    <label>
                                        <input type="checkbox"
                                               name="option[]"
                                               value="<?= $o['id'] ?>" <? if ($po['id']) echo 'checked="checked"'; ?> />
                                        <?= $o['title'] ?>
                                    </label>
                                </div>
                            <?
                            }
                            ?>
                            <div class="checkbox-menu">
                                <button type="button" class="checkallbelow btn btn-primary"><i
                                        class="fa fa-check-square-o"></i> All
                                </button>
                                <button type="button" class="checknonebelow btn btn-cancel"><i
                                        class="fa fa-check-square-o"></i> None
                                </button>
                            </div>
                            <hr>
                        <?
                        }
                    }
                    ?>
                </div>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

