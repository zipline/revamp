<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}

$basedir_thumbnail = '../upload/photos/';
?>
<div class="row">
    <div class="col-md-7">
        <div class="formfield">
            <b>Page Heading</b><br/>
            <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
        </div>

        <div class="formfield">
            <b>Appended Part Number <span class="note">This will get added to the end of the part number</span></b><br/>
            <input type="text" name="pn_append" value="<?= $page_vars['pn_append'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Hidden Price Multiplier Option <span class="note">These are edited via the options section</span></b><br/>
            <?
            write_select(array(
                'rows' => $db->query("SELECT * FROM options WHERE id_categories='18'"),
                'value' => 'id',
                'label' => 'title',
                'name' => 'hidden_option',
                'current' => $page_vars['hidden_option'],
                'display_length' => 35,
            ));
            ?>
        </div>
        <div class="formfield">
            <b>Thumbnail <span class="note">Used on parent Product Use Listing page. If empty, Gallery image will be used</span></b>
            <?= cms_photo_selector(
                '',
                'thumbnail',
                $page_vars['thumbnail'],
                $basedir_thumbnail.'/thumb/',
                $basedir_thumbnail.'/400/'
            )
            ?>
            <? if($page_vars['thumbnail']){ ?>
                <br /><label>Clear Image <input type="checkbox" name="thumbnail_clear" /></label>
            <? } ?>
        </div>
        <div class="formfield">
            <b>Gallery</b><br/>

            <div class="row">
                <div class="col-md-6">
                    <select name="gallery" id="gallery" onchange="setImage(this)">
                        <option value="0">no gallery</option>
                        <?
                        $galleries = $db->query("SELECT *, title AS formatted_title FROM galleries WHERE NOT projectid ORDER BY title ASC");
                        foreach ($galleries as $g) {
                            $query = $db->prepare("SELECT * FROM photos WHERE id_parent = :id_parent ORDER BY priority ASC LIMIT 1");
                            $query->execute(array(
                                ':id_parent' => $g['id']
                            ));
                            $p = $query->fetch();
                            $images[$g['id']] = $p['image'];
                            ?>
                            <option value="<?= $g['id'] ?>" <? if($g['id']==$page_vars['gallery']) echo 'selected="selected"'; ?>><?= $g['formatted_title'] ?></option>
                        <?
                        }
                        ?>
                    </select>

                    <div id="gallery-editlink" style="padding:10px 0px;"></div>
                </div>
                <div class="col-md-6">
                    <img src="" id="image" style="width:150px;"/>
                </div>
            </div>
            <script>
                var imagearray = {
                    <?
                        foreach($images as $key => $val){
                            echo '"'.$key.'":"'.$val.'",';
                        }
                    ?>
                };

                function setImage(imageSelect) {

                    galleryid = $('option:selected', "#gallery").attr('value');
                    gallerytitle = $('option:selected', "#gallery").html();
                    if (galleryid != 0) {
                        $('#gallery-editlink').html('<a target="_blank" class="btn btn-default" href="photos.php?id=' + galleryid + '">edit gallery: ' + gallerytitle + '</a>');
                    } else {
                        $('#gallery-editlink').html('');
                    }
                    theImageIndex = imageSelect.options[imageSelect.selectedIndex].value;
                    if (imagearray[theImageIndex]) {
                        if (document.images)
                            document.getElementById('image').src = "/upload/photos/400/" + imagearray[theImageIndex];
                        $('#image').show();
                    } else {
                        $('#image').hide();
                    }
                }
                window.onload = function () {
                    setImage(document.getElementById('gallery'));
                };
            </script>
        </div>
        <div class="formfield">
            <b>Page Content</b><br/>
    <textarea name="content" class="rich_editor"
              style="width: 100% ; height: 400px ;"><?= $page_vars['content'] ?></textarea>
        </div>
        <div class="formfield">

            <h3>Related Products</h3>
            <?
            $related_products = explode(',',$page_vars['related_products']);
            $query = $db->prepare("SELECT * FROM products WHERE id_categories != 1 ORDER BY priority DESC");
            $query->execute();
            $oo = $query->fetchAll();
            foreach ($oo as $o) {
                ?>
                <div class="col-md-6">
                    <label>
                        <input type="checkbox"
                               name="related_product[]"
                               value="<?= $o['id'] ?>" <? if (in_array($o['id'],$related_products)) echo 'checked="checked"'; ?> />
                        <?= $o['title'] ?>
                    </label>
                </div>
            <?
            }
            ?>
            <div class="checkbox-menu">
                <button type="button" class="checkallbelow btn btn-primary"><i
                        class="fa fa-check-square-o"></i> All
                </button>
                <button type="button" class="checknonebelow btn btn-cancel"><i
                        class="fa fa-check-square-o"></i> None
                </button>
            </div>
        </div>
        <div class="formfield">

            <h3>Hide Option Categories</h3>
            <?
            $hide_options = explode(',',$page_vars['hide_options']);
            $query = $db->prepare("SELECT * FROM option_categories ORDER BY priority DESC");
            $query->execute();
            $oo = $query->fetchAll();
            foreach ($oo as $o) {
                ?>
                <div class="col-md-6">
                    <label>
                        <input type="checkbox"
                               name="hide_option[]"
                               value="<?= $o['id'] ?>" <? if (in_array($o['id'],$hide_options)) echo 'checked="checked"'; ?> />
                        <?= $o['title'] ?>
                    </label>
                </div>
            <?
            }
            ?>
            <div class="checkbox-menu">
                <button type="button" class="checkallbelow btn btn-primary"><i
                        class="fa fa-check-square-o"></i> All
                </button>
                <button type="button" class="checknonebelow btn btn-cancel"><i
                        class="fa fa-check-square-o"></i> None
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="formfield">

            <h3>Associated Panel Sizes</h3>
            <?
                $products = explode(',',$page_vars['products']);

                $query = $db->prepare("SELECT * FROM products WHERE id_categories = 1 ORDER BY priority DESC");
                $query->execute();
                $oo = $query->fetchAll();
                if (count($oo)) {
                    echo '<div class="row">';
                    foreach ($oo as $o) {
                        ?>
                        <div class="col-md-4">
                            <label>
                                <input type="checkbox"
                                       name="product[]"
                                       value="<?= $o['id'] ?>" <? if (in_array($o['id'],$products)) echo 'checked="checked"'; ?> />
                                <?= $o['title'] ?>
                            </label>
                        </div>
                    <?
                    }
                    ?>
                    </div>
                    <div class="checkbox-menu">
                        <button type="button" class="checkallbelow btn btn-primary"><i
                                class="fa fa-check-square-o"></i> All
                        </button>
                        <button type="button" class="checknonebelow btn btn-cancel"><i
                                class="fa fa-check-square-o"></i> None
                        </button>
                    </div>
                    <hr>
                <?
                }

            ?>
            <script type="text/javascript">
                function check_option_box() {
                    console.log(parseFloat($(this).val()));
                    if (parseFloat($(this).val()) > 0) {
                        $(this).parent().parent().parent().find('input[type="checkbox"]').prop('checked', true);
                    }
                }
                $(document).ready(function () {
                    $('.op-price').keyup(check_option_box);
                    $('.op-price').change(check_option_box);

                    $('.checkallbelow').click(function () {
                        $(this).parent().prevUntil('h3').each(function () {
                            $(this).find('input[type="checkbox"]').prop('checked', true);
                        });
                    });
                    $('.checknonebelow').click(function () {
                        $(this).parent().prevUntil('h3').each(function () {
                            $(this).find('input[type="checkbox"]').prop('checked', false);
                        });
                    });
                });
            </script>

        </div>
    </div>
</div>