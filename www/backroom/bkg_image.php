<?
include '../common/config.php';

$parent_table = 'bkg_image';
$basedir = "../upload/background";

$query = $db->prepare("SELECT * FROM $parent_table");
$query->execute();
$rr = $query->fetchAll();


include 'common/header.php';

?>
    <div class="row">
        <div class="col-lg-12">
            <h1>Background Images</h1>

            <form action="a.php?a=bkg_image-save"
                  class="dropzone"
                  id="my-awesome-dropzone" enctype="multipart/form-data">
            </form>
            <div class="center"><a href="bkg_image.php">(Once upload is complete, click here to refresh the list below)</a></div>
            <hr>
            <h2>Uploaded Images:</h2>
            <?

            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <ol class="sortablegrid list cf"><?
                    foreach ($rr as $r) {
                        ?>
                    <li class="ui-state-default no-nest">
                        <div class="toolbar">
                            <? delete_button($parent_table, $r['id']); ?>
                        </div>
                        <?
                        if (file_exists("$basedir/thumb/{$r['image']}")) {

                        }
                        ?>
                        <img class="galleryimgs" src="<?= "$basedir/thumb/{$r['image']}" ?>"/>
                        </li><?
                    }
                    ?></ol>
            <?
            }
            ?>
        </div>
    </div>
<?

include 'common/footer.php';

