<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}
$table_name = 'blogs';

$rr = $db->query("SELECT * FROM $table_name ORDER BY title ASC") ;

include 'common/header.php' ;
?>
    <div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>Options</h4>
        <? button('add', 'blogs_.php', 'Add Blog') ; ?>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
    <h1>Blogs</h1>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
        <ol class="treeview list root" cmsTable="options" cmsHierarchyMode="flat">
            <?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			?>
            <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                <div class="row">
                    <div class="buttons">
                        <div class="cell">
                            <a href="blogs_.php?id=<?= $r['id'] ?>" title="Config" class="btn btn-xs"><span class="fa fa-gear"></span></a>
                        </div>
                        <div class="cell"><? delete_button($table_name,$r['id']) ; ?></div>
                    </div>
                    <a class="cell edit-link" href="<?= "blogs_entries.php?id={$r['id']}" ?>">
                        <?= htmlspecialchars($r['title']) ?>
                    </a>
                </div>
            </li><?
		}
	?></ol><?
}

?>
    </div>
    </div>
<?
include 'common/footer.php' ;

