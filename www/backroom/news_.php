<?
include 'common/config.php' ;

$table_name = 'news';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

if( ! admin_perm_check($db, $_SESSION['admin'], $table_name, $blog['id']) ){
	exit('Permission denied') ;
}
if (!$r) {
	$r['date_posted'] = date('Y-m-d');
}
include 'common/header.php' ;

?>
<script>
	$(function(){
		$('#datepicker').datepicker({
			dateFormat : 'yy-mm-dd'
		});
	});
</script>
<form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?= $r['id'] ?>"/>
	<? print_controls(array('back'=>"$table_name.php")) ; ?>
	<table class="editor">
		<tr>
			<td>Title<br/>
				<input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>" style="width:290px;"/></td>
			</td>
			<td>Date Posted<br/>
				<input type="text" id="datepicker" size="30" name="date_posted" value="<?= htmlspecialchars($r['date_posted']) ?>"/>
			</td>
		</tr>
		<?
		if ($config['news_can_display_on_homepage']) {
			?>
			<tr>
				<td colspan="2">
					<label><input type="checkbox" name="homepage" <?= $r['homepage'] ? 'checked="checked"':'' ?>/> Display summary on homepage</label><br/>
				</td>
			</tr>
			<?
		}
		?>
		<tr>
			<td colspan="2">Summary<br/>
				<textarea name="description" class="" style="width: 600px ; height: 50px ;"><?= $r['description'] ?></textarea>
			</tr>
		<tr>
			<td colspan="2">Content<br/>
				<textarea name="content" class="rich_editor" style="width: 600px ; height: 700px ;"><?= $r['content'] ?></textarea>
			</td>
		</tr>
	</table>
</form>
<?

include 'common/footer.php' ;

