<?

require_once(__DIR__."/../../vendor/autoload.php");
require_once(__DIR__."/mod_cms.php");
require_once(__DIR__."/mod_form.php");
require_once(__DIR__."/mod_format.php");
require_once(__DIR__."/mod_sql.php");
require_once(__DIR__."/mod_util.php");
require_once(__DIR__."/mod_galleries.php");
require_once(__DIR__."/mod_blog.php");
require_once(__DIR__."/mod_cart.php");
require_once(__DIR__."/mod_fedex.php");

mb_internal_encoding("utf-8");
session_start() ;
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

// config settings
date_default_timezone_set('America/Los_Angeles');

// import config file and merge with local config
$config = json_decode(file_get_contents(__DIR__.'/../../config/config.json'), true);
if (file_exists(__DIR__.'/../../config/config-local.json')) {
    $local_config = json_decode(file_get_contents(__DIR__.'/../../config/config-local.json'), true);
    $config = array_replace_recursive($config, $local_config);
}

$config['date'] = date('h:i:s a') ;

// database connection
try {
    $db = new PDO("mysql:dbname=" . $config['db']['name'] . ";host=" . $config['db']['host'], $config['db']['username'], $config['db']['password']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->query("SET sql_mode=''");
} catch (PDOException $e) {
    die("Problem connecting to database.");
}
$twig = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"].'/templates'), [
    'debug'=>true,
    'cache'=>__DIR__.'/../../twig_cache'
]);

$twig->addExtension(new Twig_Extension_Debug());
$twig->addExtension(new Aptoma\Twig\Extension\MarkdownExtension(new Aptoma\Twig\Extension\MarkdownEngine\MichelfMarkdownEngine()));

// mailer
$mailer_transport = Swift_SmtpTransport::newInstance(
    $config['smtp']['host'],
    $config['smtp']['port'],
    'ssl'
);
$mailer_transport->setUsername($config['smtp']['username']);
$mailer_transport->setPassword($config['smtp']['password']);
$mailer = Swift_Mailer::newInstance($mailer_transport);

if ($config['log_swiftmailer']) {
    $swiftmailer_logger = new Swift_Plugins_Loggers_ArrayLogger();
    $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($swiftmailer_logger));
}

// logging
$logger = new Monolog\Logger('applog');
$logger->pushHandler(new Monolog\Handler\StreamHandler(__DIR__.'/../../app.log', Monolog\Logger::DEBUG));

if ($_SESSION['admin']) {
    $config['admin_conf'] = $config['admin_types'][$_SESSION['admin']['type']];
}

// set up test mode
if (isset($_GET['test']) and $_SESSION['admin']) {
    $_SESSION['test'] = $_GET['test'];
}

$settings = settings_load($db);
if($settings['receipt_contact']) {
    $config['receipt_contact'] = $settings['receipt_contact'];
}

// log in for admin
if( false!==strpos($_SERVER['PHP_SELF'],'backroom/') ){
    if( 'admin_login' == $_POST['action'] ){
        $_SESSION['admin'] = admin_login($db, $_POST['username'], $_POST['password']) ;
        if( $_SESSION['admin'] ){
            hle($config['admin_types'][ $_SESSION['admin']['type'] ]['home_url']) ;
        }
    }
    if( ! $_SESSION['admin'] and 'index.php'!=basename($_SERVER['PHP_SELF']) ){
        hle('./') ;
    }
}
