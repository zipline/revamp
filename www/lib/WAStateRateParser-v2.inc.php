<?php
/********************************************************************************************************
 * Class WAXMLParser                                                                                    *
 * cURLs to dor.wa.gov, obtains XML response from server, and parses it into a usable array of
 * destination-based sales tax data
 ********************************************************************************************************
 * Written by Jerry Higbee
 * Property of JMH Online Retailers
 * May not be re-distributed without consent.
 * 2008 JMH Online Retailers
 *******************************************************************************************************/
 class WAXMLParser2{

		// Members
		var $type;
		var $request_url;
		var $sa1;
		var $city;
		var $zip;
		var $searchtag;
		var $rate_data;
		var $simple_xml;
		var $rate_info = array();
		var $reader;
		var $default_rate;

		// Constructor
		function __construct($address,$city,$zip){
			$this->request_url = "http://webgis.dor.wa.gov/webapi/AddressRates.aspx?output=xml&";
			$this->type = $type;
			$this->sa1 = $address;
            $this->city = $city;
			$this->zip = $zip;

	        $data ="addr=".urlencode($this->sa1)."&city=".urlencode($this->city)."&zip=".urlencode($this->zip);

			// Start a cURL session and send the XML data
			$data = trim($data);

	   	  	$this->reader = new XMLReader();

			$this->reader->open($this->request_url.$data);

		}

		// Reads all attributes in every element
		public function readAttributes(){
            while ($this->reader->read()){
                if ($this->reader->hasAttributes && $this->reader->nodeType == XMLReader::ELEMENT) {
                	while($this->reader->moveToNextAttribute()){
						$this->rate_info[$this->reader->name] = $this->reader->value;
					}
				}
			}
		}

		// Return the array of data we generated from our XML read
		public function getTaxData(){
			return $this->rate_info;
		}

		// Check for any error codes
		public function checkErrorCode(){
			switch($this->rate_info['code']){
				case '0':
					return FALSE; // No problems, address was found
				break;
				case '1':
					return 'The address was not found, but the ZIP+4 was located.';
				break;
				case '2':
					return 'Neither the address or ZIP+4 was found, but the 5-digit ZIP was located.';
				break;
				case '3':
						return 'The address, ZIP+4, and ZIP could not be  found.';
				break;
				case '4':
					return 'Invalid arguments.';
				break;
				case '5':
					return '500 Internal Server Error.';
				break;
			}
		}

		/******************************** Set/Get Individual Values **************************************/
        	// Set a default tax rate if we can't find the tax rates
		public function setDefaultRate($rate){
			$this->default_rate = $rate;
		}

		// Return the default tax rate
		public function getDefaultRate(){
			return $this->default_rate;
		}

		// Get the location code
		// Only called after $this->rate_info has been filled, else value is empty
		public function getLoCode(){
			return $this->rate_info['locode'];
		}

        // Get the local tax rate
		// Only called after $this->rate_info has been filled, else value is empty
		public function getLocalRate(){
			return $this->rate_info['localrate'];
		}

        // Get the combined tax rate
		// Only called after $this->rate_info has been filled, else value is empty
		public function getCombinedRate(){
			return $this->rate_info['rate'];
		}

         // Get the low street number
		// Only called after $this->rate_info has been filled, else value is empty
		public function getHouseLow(){
			return $this->rate_info['houselow'];
		}

         // Get the high street number
		// Only called after $this->rate_info has been filled, else value is empty
		public function getHouseHigh(){
			return $this->rate_info['househigh'];
		}

        // Get the odd/even indicator
		// Only called after $this->rate_info has been filled, else value is empty
		public function getOddEven(){
			return $this->rate_info['evenodd'];
		}

        // Get the (standardized) street address
		// Only called after $this->rate_info has been filled, else value is empty
		public function getStreet(){
			return $this->rate_info['street'];
		}

        // Get the state
		// Only called after $this->rate_info has been filled, else value is empty
		public function getState(){
			return $this->rate_info['state'];
		}

        // Get the 5-digit zip
		// Only called after $this->rate_info has been filled, else value is empty
		public function getZip5(){
			return $this->rate_info['zip'];
		}

        // Get the 4-digit zip
		// Only called after $this->rate_info has been filled, else value is empty
		public function getZip4(){
			return $this->rate_info['plus4'];
		}

        // Get the rate period
		// Only called after $this->rate_info has been filled, else value is empty
		public function getRatePeriod(){
			return $this->rate_info['period'];
		}

        // Get the state tax rate
		// Only called after $this->rate_info has been filled, else value is empty
		public function getStateRate(){
			return $this->rate_info['staterate'];
		}

        // Get the tax rate name
		// Only called after $this->rate_info has been filled, else value is empty
		public function getRateName(){
			return $this->rate_info['name'];
		}

        // Get the tax rate name
		// Only called after $this->rate_info has been filled, else value is empty
		public function getRTA(){
			return $this->rate_info['rta'];
		}

        // Get the tax rate name
		// Only called after $this->rate_info has been filled, else value is empty
		public function getPTBA(){
			return $this->rate_info['ptba'];
		}

        // Get the tax rate name
		// Only called after $this->rate_info has been filled, else value is empty
		public function getCEZ(){
			return $this->rate_info['cez'];
		}
 }
?>
