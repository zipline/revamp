<?
function fmt_urlify($str){
	return strtolower(trim(preg_replace('@[^a-z0-9]+@i','-',$str),'-')) ;
}
function fmt_ellipsis($str,$len){
	return strlen($str) > $len
		? (substr($str,0,$len).'...')
		: $str
		;
}
function fmt_quote($text){
	return str_replace('&mdash;','<br/>&mdash;',$text) ;
}
function fmt_date($datetime){
	return date('F j, Y',strtotime($datetime)) ;
}
function fmt_date_ago($datetime){
	return date('F j, Y',strtotime($datetime)) ;
}
function fmt_datetime($datetime){
	return date('F j, Y, g:i a',strtotime($datetime)) ;
}
function fmt_price($price){
	return '$'.number_format($price,2) ;
}
function fmt_name($person){
	return $person['firstname'].' '.$person['lastname'] ;
}
