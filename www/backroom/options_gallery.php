<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}
$basepath = '../upload/patterns/' . $_GET['oid'] . '/';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <link href="/lib/dropzone-master/downloads/css/dropzone.css" type="text/css" rel="stylesheet"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="global.css?5"/>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
    <script src="/lib/dropzone-master/downloads/dropzone.min.js"></script>
    <style type="text/css">
        body {
            background: #fff;
            padding-top: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(".ajax_delete").click(function (event) {
                event.preventDefault();
                if (window.confirm('Really delete?')) {
                    var d_item = $(this);
                    $.ajax({
                        type: 'POST',
                        url: "a_ajax.php",
                        data: {
                            'a': 'delete_option_image',
                            'oid': <?= $_GET['oid'] ?>,
                            'f': d_item.attr('rel')
                        }
                    }).done(function (content) {
                        d_item.closest(".element_contain").fadeOut("slow");
                    });
                }
            });
        });
    </script>
</head>
<body>
<p>Images must be
    named with the width x height of the final product. For example "120x48.svg" is the correct file name for a 120"
    wide by 48" tall panel.</p>

<form action="a.php?a=option-photos-save"
      class="dropzone"
      id="my-awesome-dropzone" enctype="multipart/form-data">
    <input type="hidden" name="oid" value="<?= $_GET['oid'] ?>"/>
</form>
<?php
$folder = $basepath;
$filetype = '*.*';
$files = glob($folder . $filetype);
$count = count($files);
echo '<h2>Existing Images</h2><ol class="sortablegrid list nosort">';
for ($i = 0; $i < $count; $i++) {
    $f = str_replace($folder, '', $files[$i]);
    ?>
    <li class="ui-state-default no-nest element_contain" id="priorityitems-<?= $r['id'] ?>">
        <div style="float: right;">
            <div style="display: inline-block;">
                <button class="delete_first btn btn-danger btn-xs ajax_delete" rel="<?= $f ?>" title="Delete"></button>
            </div>
        </div>
        <?= $f ?><br>
        <?= '<a name="' . $i . '" href="#' . $i . '"><img src="' . $files[$i] . '" /></a>'; ?>
    </li>
<?php
}
echo '</ul>';
?>

</body>