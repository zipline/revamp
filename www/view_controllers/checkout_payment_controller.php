<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

if( ! is_array($_SESSION['cart']) or ! $_SESSION['cart'] ){
    hle('cart') ;
}

$page['head_extra'] .= '';
$cc = form_load_clear('checkout_payment') ;
$r = $_SESSION['customer'] ;


ob_start();
cart_display($db, array(
    'cart'=>$_SESSION['cart'],
    'discounts'=>$_SESSION['discounts'],
    'customer'=>$_SESSION['customer'],
    'show_tax'=>true,
    'show_controls'=>false,
));
$cart = ob_get_clean();


ob_start();
write_months('cc_month',$cc['cc_month'],'','class="card-expiry-month required"') ;
write_years('cc_year',$cc['cc_year'],'',date('y'),date('y')+10,'class="card-expiry-year required"') ;
$ccdate = ob_get_clean();

$content = $twigpanel->render('cart_payment.twig', array(
    'r' => $r,
    'c' => $_SESSION['cart'],
    'cc' => $cc,
    'stripe_publishable_key' => $stripe_publishable_key,
    'carthtml' => $cart,
    'config' => $config,
    'ccdate' => $ccdate
));

$page['head'] = '
                <script type="text/javascript" src="/lib/jquery.validate.min.js"></script>';

$page['title'] = 'Shopping Cart: Payment';
$page['page_vars']['content'] = $content;
