<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}
?>
<div class="formfield">
    <b>Page Heading</b><br/>
    <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
</div>
<div class="formfield">
    <b>Page Content</b><br/>
    <textarea name="content" class="rich_editor"
              style="width: 100% ; height: 400px ;"><?= $page_vars['content'] ?></textarea>
</div>
<div class="formfield">
    <b>Gallery</b><br/>


    <select name="gallery" id="gallery" onchange="setImage(this)">
        <?
        $galleries = $db->query("SELECT *, title AS formatted_title FROM galleries WHERE NOT projectid ORDER BY title ASC");

        $images = array();
        foreach ($galleries as $g) {
            $query = $db->prepare("SELECT * FROM photos WHERE id_parent = :id_parent ORDER BY priority ASC LIMIT 1");
            $query->execute(array(
                ':id_parent' => $g['id']
            ));
            $p = $query->fetch();
            $images[$g['id']] = $p['image'];
            ?>
            <option value="<?= $g['id'] ?>" <? if($g['id'] == $page_vars['gallery']) echo 'selected="selected"'; ?>><?= $g['formatted_title'] ?></option>
            <?
        }
        ?>
    </select>

    <img src="" id="image" style="width:150px;"/>
    <div id="gallery-editlink" style="padding:10px 0px;"></div>
    <script>
        var imagearray = {
            <?
            foreach($images as $key => $val){
                echo '"'.$key.'":"'.$val.'",';
            }
            ?>
        };

        function setImage(imageSelect) {
            theImageIndex = imageSelect.options[imageSelect.selectedIndex].value;
            console.log(theImageIndex);
            gallerytitle = imageSelect.options[imageSelect.selectedIndex].innerHTML;
            if(theImageIndex!=0) {
                document.getElementById("gallery-editlink").innerHTML = '<a target="_blank" class="btn btn-default" href="/backroom/photos.php?id=' + theImageIndex + '">edit gallery: ' + gallerytitle + '</a>';
            }else{
                document.getElementById("gallery-editlink").innerHTML = "";
            }
            if (imagearray[theImageIndex]) {
                if (document.images)
                    document.getElementById('image').src = "/upload/photos/400/" + imagearray[theImageIndex];
                document.getElementById('image').style.display = 'block';
            } else {
                document.getElementById('image').style.display = 'none';
            }
        }
        window.onload = function () {
            setImage(document.getElementById('gallery'));
        };
    </script>
</div>

